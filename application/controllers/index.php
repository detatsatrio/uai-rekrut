<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {

	/**
	 * Kelas index untuk penanganan halaman awal depan aplikasi
	 *
	 * Created date : 12/04/2017
	 */
	var $skey = "UAI5ecretMsg2017";

	public function __construct(){
		parent::__construct();

		/* Variabel */
		$this->maks_teach = 30;

		$this->load->helper(array('url'));
		$this->load->library('parser');
		$this->load->library('encrypt');
		$this->load->library('session');
		$this->load->library('upload');

		$this->load->model('mUser');
		$this->load->model('mAll');

		$this->load->database();

		$this->menu = array(
			'role'		=> (empty($this->session->userdata['role'])) ? '' : $this->session->userdata['role'],
			'session'	=> $this->session->userdata('role')
		);

		$this->footer = array(
			'latest'	=> $this->mAll->getAllCustom('vacancy','ORDER BY id_vacancy DESC LIMIT 2')
		);
	}

	public function cekSession(){
		if ($this->session->userdata('role')=='1') {
			//redirect(base_url('login'));
			header('location:'.base_url().'login');
			exit(0);
		} elseif($this->session->userdata('role')=='3') {
			header('location:'.base_url().'login');
			exit(0);
		} elseif($this->session->all_userdata()==''){
			header('location:'.base_url().'login');
			exit(0);
		}
	}

	public function index()
	{
		$data = array(
			"title"		=> "UIA - Rekrutmen"
		);

		$this->parser->parse('frontend/common/header',$data);
		$this->parser->parse('frontend/common/menu',$this->menu);
		$this->parser->parse('frontend/index',$data);
		$this->parser->parse('frontend/common/footer',$this->footer);
		$this->parser->parse('frontend/common/foot_script',$data);
	}

	public function dasbor()
	{
		$this->cekSession();

		$app = $this->mAll->getAllWhere('applicant',array('id_user' => $this->session->userdata('id')));

		$applicant['status'] = (empty($app)) ? '1' : $app;

		$personal = $this->mAll->checkPersonalData($this->session->userdata('id'));
		$edukasi = $this->mAll->getAllWhere('education',array('id_user' => $this->session->userdata('id')));
		$penelitian = $this->mAll->getAllWhere('research',array('applicant' => $this->session->userdata('id')));
		$mengajar = $this->mAll->getAllWhere('teach',array('applicant' => $this->session->userdata('id')));
		$docs = ($applicant['status'] < 8) ? $this->mAll->checkUploadDocsP1($this->session->userdata('id')) : $this->mAll->checkUploadDocsP2($this->session->userdata('id'));

		if ((((($personal && $edukasi) && $penelitian) && $mengajar) && $docs) != null) {
			$status_cv = true;
		} else {
			$status_cv = false;
		}

		$data = array(
			"title"		=> "UIA - Rekrutmen",
			"applicant" => $this->mAll->getAllWhere("applicant",array('id_user'=>$this->session->userdata('id')),'array'),
			"status_user" => $this->mAll->getAllWhere('applicant',array('id_user'=>$this->session->userdata('id'))),
			"vacancy"	=> $this->mAll->getAll("vacancy"),
			"status_cv" => $status_cv
		);

		$this->parser->parse('frontend/common/header',$data);
		$this->parser->parse('frontend/common/menu',$this->menu);
		$this->parser->parse('frontend/dasbor',$data);
		$this->parser->parse('frontend/common/footer',$this->footer);
		$this->parser->parse('frontend/common/foot_script',$data);
	}

	public function register()
	{
		$data = array(
			"title"		=> "UIA - Rekrutmen"
		);

		$this->parser->parse('frontend/common/header',$data);
		$this->parser->parse('frontend/common/menu',$this->menu);
		$this->parser->parse('frontend/daftar',$data);
		$this->parser->parse('frontend/common/footer',$this->footer);
		$this->parser->parse('frontend/common/foot_script',$data);
	}

	public function template($action){
		if ($action == 'confirm') {
			$data = array(
				'title' => "Silahkan klik tombol dibawah untuk verifikasi akun Anda",
				'msg'	=> '<p><a href="" class="cws-button bt-color-4 border-radius">Disini</a></p>'
			);
		} else {
			$data = array(
				'title' => 'Password Anda',
				'msg'	=> 	''
			);
		}

		// $this->parser->parse('frontend/common/header',$data);
		$this->parser->parse('template', $data);
		//$this->parser->parse('frontend/common/foot_script',$data);
	}

	public function register_act()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$email	  = $this->input->post('email');

		if (($username && $password && $email) == null) {
			$respons['error'] = '01';
			$respons['mess'] = 'Form kosong';
		} else {
			$user = $this->mAll->getAll('user');
			foreach ($user as $u) {
				if ($u['username'] == $username) {
					$username = false;
				} elseif ($u['email'] == $email) {
					$email = false;
				}
			}
			if ($username === false) {
				$respons['error'] = '02';
				$respons['mess'] = 'Username sudah digunakan';
			} elseif ($email === false) {
				$respons['error'] = '02';
				$respons['mess'] = 'Email sudah digunakan';
			} else {
				// confirmation code
				$key = 'UAI5ecretMsg2017';
				$encrypted_username = $this->encode($username,$key);
				$url = base_url('index/confirmation').'/'.urlencode($encrypted_username);

				$html = '	<!DOCTYPE html>
							<html>
							<head>
								<title></title>
							</head>
							<body>
								<h1>Silahkan konfirmasi Akun anda.</h1>
								<small>Dengan terkonfirmasinya akun Anda, Anda dapat masuk ke sistem UAI Rekrutmen</small>
								<a href="'.$url.'">Konfirmasi Disini</a>
							</body>
							</html>';

				$send_email = $this->send_mail($email, $username, 'Konfirmasi Pendaftaran UAI Rekrutmen', $html);
				if ($send_email!= TRUE) {
					$respons['error'] = '02';
					$respons['mess'] = 'Gagal mengirim email';
				} else {
					$this->mAll->sisip_data('user',array('username'=>$username,'password'=>$this->encrypt->encode($password),'email'=>$email,'status'=>'2'));
					$respons['error'] = '00';
					$respons['mess'] = 'sukses';
					$respons['redirect'] = base_url('/login');
				}
			}
		}

		echo json_encode($respons);
	}

	/*Encrypted Class*/
	public function safe_b64encode($string) {

        $data = base64_encode($string);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }

    public function safe_b64decode($string) {
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public function encode($value) {

        if (!$value) {
            return false;
        }
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext));
    }

    public function decode($value) {

        if (!$value) {
            return false;
        }
        $crypttext = $this->safe_b64decode($value);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }
	/*Encryoted Class*/

	public function confirmation($confirm_code = null){
		if ($confirm_code) {
			$key = 'UAI5ecretMsg2017';
			$decode = $this->decode(urldecode($confirm_code),$key);
			$user = $this->mAll->getAll('user');
			// echo $confirm_code;
			// echo $decode;
			foreach ($user as $u) {
				if ($u['username'] == $decode) {
					$this->mAll->update_data('user', array('status'=>'1'),array('username'=>$u['username']));
					redirect(base_url('login'));
				}
			}
		} else {
			echo 'confirmation code is null';
		}
	}

	public function forgot_password(){
		$email = $this->input->post('email-forgot');
		$user = $this->mAll->getAllWhere('user',array('email'=>$email));

		if (!$user) {
			$result['respon']	= '01';
			$result['mess']		= 'Email tidak ditemukan';
		} else {
			$username = $user['username'];
			$password = $this->encrypt->decode($user['password']);
			$html = '	<!DOCTYPE html>
							<html>
							<head>
								<title></title>
							</head>
							<body>
								<h1>Username : '.$username.'</h1>
								<h1>Password : '.$password.'</h1>
							</body>
							</html>';

			$send_email = $this->send_mail($user['email'],($user['full_name']) ? $user['full_name'] : $user['username'], 'Lupa Password', $html);

			if ($send_email == FALSE) {
				$result['respon']	= '01';
				$result['mess']		= 'Email tidak dikirim';
			} elseif ($send_email == TRUE) {
				$result['respon']	= '00';
				$result['mess']		= 'Email telah dikirim';
				$result['redirect']		= base_url('login');
			} else {
				$result['respon']	= '01';
				$result['mess']		= 'Email tidak dikirim';
			}
		}

		echo json_encode($result);
	}

	public function send_mail($address, $name, $subject, $msg = '')
    {
        //require_once(APPPATH.'third_party/phpmailer/PHPMailerAutoload.php');
        require '/../third_party/phpmailer/PHPMailerAutoload.php';

       //Create a new PHPMailer instance
		$mail = new PHPMailer;

		//Tell PHPMailer to use SMTP
		$mail->isSMTP();

		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = 2;

		//Ask for HTML-friendly debug output
		$mail->Debugoutput = 'html';

		//Set the hostname of the mail server
		$mail->Host = 'ssl://smtp.gmail.com:465';

		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;

		$mail->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true
		    )
		);

		//Username to use for SMTP authentication - use full email address for gmail
		$mail->Username = "skyesprojects@gmail.com";

		//Password to use for SMTP authentication
		$mail->Password = "SkyesprojectS13082015";

		//Set who the message is to be sent from
		$mail->setFrom('uairekrut@gmail.com', 'UAI REKRUTMEN');

		//Set who the message is to be sent to
		$mail->addAddress($address, $name);

		//Set the subject line
		$mail->Subject = $subject;

		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		$mail->msgHTML($msg);

		//Replace the plain text body with one created manually
		$mail->AltBody = 'This is a plain-text message body';

		//Attach an image file
		//$mail->addAttachment('images/phpmailer_mini.png');

		//send the message, check for errors
		return (!$mail->send()) ? FALSE : TRUE;
    }

	public function lowongan()
	{
		$data = array(
			"title"			=> "UIA - Rekrutmen",
			"vacancy"		=> $this->mAll->getAllOrderBy('vacancy','id_vacancy','DESC'),
			"applicant"		=> $this->mAll->getAll('applicant')

		);

		$this->parser->parse('frontend/common/header',$data);
		$this->parser->parse('frontend/common/menu',$this->menu);
		$this->parser->parse('frontend/lowongan',$data);
		$this->parser->parse('frontend/common/footer',$this->footer);
		$this->parser->parse('frontend/common/foot_script',$data);
	}

	public function lowongandetail($id)
	{
		$data = array(
			"title"			=> "UIA - Rekrutmen",
			"vacancy"		=> $this->mAll->getAllWhere('vacancy',array('id_vacancy'=>$id)),
			"applicant"		=> $this->mAll->getAll('applicant')
		);

		$this->parser->parse('frontend/common/header',$data);
		$this->parser->parse('frontend/common/menu',$this->menu);
		$this->parser->parse('frontend/lowongan_detail',$data);
		$this->parser->parse('frontend/common/footer',$this->footer);
		$this->parser->parse('frontend/common/foot_script',$data);
	}

	public function akun()
	{
		if ($this->session->all_userdata() == null) {
			//redirect(base_url('login'));
			header('location:'.base_url().'login');
			exit(0);
		} else {
			$data = array(
				"title"		=> "UIA - Rekrutmen",
				"email"		=> $this->mAll->getValue('user', array('id_user'=>$this->session->userdata('id')), 'email')
			);

			$this->parser->parse('frontend/common/header',$data);
			$this->parser->parse('frontend/common/menu',$this->menu);
			$this->parser->parse('frontend/akun',$data);
			$this->parser->parse('frontend/common/footer',$this->footer);
			$this->parser->parse('frontend/common/foot_script',$data);
		}

	}

	public function form_aplikasi()
	{
		$this->cekSession();
		$user = $this->mAll->getAllWhere('user',array('id_user'=>$this->session->userdata('id')));
		$app = $this->mAll->getAllWhere('applicant',array('id_user'=>$this->session->userdata('id')));

		$applicant['status'] = (empty($app)) ? '1' : $app;

		$data = array(
			"title"		=> "UIA - Rekrutmen",
			"applicant" => $this->mAll->getAllWhere('applicant',array('id_user'=>$this->session->userdata('id'))),
			"education"	=> $this->mAll->getAllWhere('education',array('id_user'=>$this->session->userdata('id')),'array'),
			"research"	=> $this->mAll->getAllWhere('research',array('applicant'=>$this->session->userdata('id')),'array'),
			"teach"		=> $this->mAll->getAllWhere('teach',array('applicant'=>$this->session->userdata('id')),'array'),
			"docs"		=> $this->mAll->getAllWhere('docs',array('applicant'=>$this->session->userdata('id')),'array'),
			"jenjang"	=> $this->mAll->getAll('jenjang'),
			"action"	=> base_url('index/save_cv'),
			"email"		=> (isset($user['email'])) ? $user['email'] : '',
			"fullname"	=> (isset($user['full_name'])) ? $user['full_name'] : '',
			"birthplace"=> (isset($user['birth_place'])) ? $user['birth_place'] : '',
			"birthdate"	=> (isset($user['birth_date'])) ? $user['birth_date'] : '',
			"mother"	=> (isset($user['mother_name'])) ? $user['mother_name'] : '',
			"address"	=> (isset($user['address'])) ? $user['address'] : '',
			"id_card"	=> (isset($user['id_card_no'])) ? $user['id_card_no'] : '',
			"homephone"	=> (isset($user['homephone_no'])) ? $user['homephone_no'] : '',
			"handphone"	=> (isset($user['handphone_no'])) ? $user['handphone_no'] : '',
			"gender"	=> $this->mAll->getAll('gender'),
			"marital"	=> $this->mAll->getAll('marital'),
			"data_personal" => $this->mAll->checkPersonalData($this->session->userdata('id')),
			"data_edukasi"=> $this->mAll->getAllWhere('education',array('id_user' => $this->session->userdata('id'))),
			"data_penelitian"=> $this->mAll->getAllWhere('research',array('applicant' => $this->session->userdata('id'))),
			"data_mengajar"=> $this->mAll->getAllWhere('teach',array('applicant' => $this->session->userdata('id'))),
			"data_docs"=> ($applicant['status'] > 1 || $applicant['status'] < 8) ? $this->mAll->checkUploadDocsP1($this->session->userdata('id')) : $this->mAll->checkUploadDocsP2($this->session->userdata('id'))
		);

		$this->parser->parse('frontend/common/header',$data);
		$this->parser->parse('frontend/common/menu',$this->menu);
		$this->parser->parse('frontend/form_aplikasi',$data);
		$this->parser->parse('frontend/common/footer',$this->footer);
		$this->parser->parse('frontend/common/foot_script',$data);
	}

	public function add_education(){
		$id_user = $this->mAll->getAllWhere('education',array('id_user'	=> $this->session->userdata('id')),'array');

		if (count($id_user) == 5) {
			$respons['error'] 	= '01';
			$respons['mess']	= 'Batas pengisian maksimal 5';
		} else {
			$respons['error']	= '00';
			$respons['count']	= count($id_user);
			$respons['mess']	= 'sukses';
		}

		echo json_encode($respons);
	}

	public function add_research(){
		$maks = 5;
		$id_user = $this->mAll->getAllWhere('research',array('applicant'	=> $this->session->userdata('id')),'array');

		if (count($id_user) == $maks) {
			$respons['error'] 	= '01';
			$respons['mess']	= 'Batas pengisian maksimal '.$maks;
		} else {
			$respons['error']	= '00';
			$respons['count']	= count($id_user);
			$respons['mess']	= 'sukses';
		}

		echo json_encode($respons);
	}

	public function add_teach(){
		$maks = 5;
		$id_user = $this->mAll->getAllWhere('teach',array('applicant'	=> $this->session->userdata('id')),'array');

		if (count($id_user) == $maks) {
			$respons['error'] 	= '01';
			$respons['mess']	= 'Batas pengisian maksimal '.$maks;
		} else {
			$respons['error']	= '00';
			$respons['count']	= count($id_user);
			$respons['mess']	= 'sukses';
		}

		echo json_encode($respons);
	}

	public function save_cv(){
		$this->cekSession();

		if ($this->input->post()) {
			$user = array(
			"email"			=> $this->input->post('email_user'),
			"full_name"		=> $this->input->post('fullname'),
			"birth_place"	=> $this->input->post('birth_place'),
			"birth_date"	=> $this->input->post('birth_date'),
			"mother_name"	=> $this->input->post('mother'),
			"address"		=> $this->input->post('address'),
			"id_card_no"	=> $this->input->post('id_card_no'),
			"homephone_no"	=> $this->input->post('homephone_user'),
			"handphone_no"	=> $this->input->post('handphone_user'),
			"gender"		=> $this->input->post('gender'),
			"marital"		=> $this->input->post('marital')
		);

		$user_update = $this->mAll->update_data('user',$user,array('id_user'=>$this->session->userdata('id')));

		($user_update) ? $this->mAll->update_data('applicant',array('status'=>'2'),array('id_user'=>$this->session->userdata('id'))) : '';

		/* Save data pendidikan */
		$pendidikan1 = array(
			"id_education"	=> ($this->input->post('id-education-1') != null) ? $this->input->post('id-education-1') : null,
			'id_user'		=> $this->session->userdata('id'),
			"jenjang"		=> $this->input->post('jenjang-1'),
			"in_year"		=> $this->input->post('in-year-1'),
			"end_year"		=> $this->input->post('end-year-1'),
			"nim"			=> $this->input->post('nim-1'),
			"prodi"			=> $this->input->post('prodi-1'),
			"minat"			=> $this->input->post('minat-1'),
			"karya"			=> $this->input->post('karya-1')
		);

		if ($this->input->post('id-education-1')) {
			$this->mAll->update_data('education',$pendidikan1,array('id_education'=>$this->input->post('id-education-1')));
		} elseif ($this->input->post('action-1') == 'insert') {
			$this->mAll->sisip_data('education',$pendidikan1);
		} else {

		}

		$pendidikan2 = array(
			"id_education"	=> ($this->input->post('id-education-2') != null) ? $this->input->post('id-education-2') : null,
			'id_user'		=> $this->session->userdata('id'),
			"jenjang"		=> $this->input->post('jenjang-2'),
			"in_year"		=> $this->input->post('in-year-2'),
			"end_year"		=> $this->input->post('end-year-2'),
			"nim"			=> $this->input->post('nim-2'),
			"prodi"			=> $this->input->post('prodi-2'),
			"minat"			=> $this->input->post('minat-2'),
			"karya"			=> $this->input->post('karya-2')
		);

		if ($this->input->post('id-education-2')) {
			$this->mAll->update_data('education',$pendidikan2,array('id_education'=>$this->input->post('id-education-2')));
		} elseif ($this->input->post('action-2') == 'insert') {
			$this->mAll->sisip_data('education',$pendidikan2);
		} else {

		}

		$pendidikan3 = array(
			"id_education"	=> ($this->input->post('id-education-3') != null) ? $this->input->post('id-education-3') : null,
			'id_user'		=> $this->session->userdata('id'),
			"jenjang"		=> $this->input->post('jenjang-3'),
			"in_year"		=> $this->input->post('in-year-3'),
			"end_year"		=> $this->input->post('end-year-3'),
			"nim"			=> $this->input->post('nim-3'),
			"prodi"			=> $this->input->post('prodi-3'),
			"minat"			=> $this->input->post('minat-3'),
			"karya"			=> $this->input->post('karya-3')
		);

		if ($this->input->post('id-education-3')) {
			$this->mAll->update_data('education',$pendidikan3,array('id_education'=>$this->input->post('id-education-3')));
		} elseif ($this->input->post('action-3') == 'insert') {
			$this->mAll->sisip_data('education',$pendidikan3);
		} else {

		}

		$pendidikan4 = array(
			"id_education"	=> ($this->input->post('id-education-4') != null) ? $this->input->post('id-education-4') : null,
			'id_user'		=> $this->session->userdata('id'),
			"jenjang"		=> $this->input->post('jenjang-4'),
			"in_year"		=> $this->input->post('in-year-4'),
			"end_year"		=> $this->input->post('end-year-4'),
			"nim"			=> $this->input->post('nim-4'),
			"prodi"			=> $this->input->post('prodi-4'),
			"minat"			=> $this->input->post('minat-4'),
			"karya"			=> $this->input->post('karya-4')
		);

		if ($this->input->post('id-education-4')) {
			$this->mAll->update_data('education',$pendidikan4,array('id_education'=>$this->input->post('id-education-4')));
		} elseif ($this->input->post('action-4') == 'insert') {
			$this->mAll->sisip_data('education',$pendidikan4);
		} else {

		}

		$pendidikan5 = array(
			"id_education"	=> ($this->input->post('id-education-5') != null) ? $this->input->post('id-education-5') : null,
			'id_user'		=> $this->session->userdata('id'),
			"jenjang"		=> $this->input->post('jenjang-5'),
			"in_year"		=> $this->input->post('in-year-5'),
			"end_year"		=> $this->input->post('end-year-5'),
			"nim"			=> $this->input->post('nim-5'),
			"prodi"			=> $this->input->post('prodi-5'),
			"minat"			=> $this->input->post('minat-5'),
			"karya"			=> $this->input->post('karya-5')
		);

		if ($this->input->post('id-education-5')) {
			$this->mAll->update_data('education',$pendidikan5,array('id_education'=>$this->input->post('id-education-5')));
		} elseif ($this->input->post('action-5') == 'insert') {
			$this->mAll->sisip_data('education',$pendidikan5);
		} else {

		}

		/* End of Save Pendidikan */

		/* Start Save Penelitian */
		$penelitian1 = array(
			"id_research"	=> ($this->input->post('id-research-1') != null) ? $this->input->post('id-research-1') : null,
			"applicant"		=> $this->session->userdata('id'),
			"title"			=> $this->input->post('judul-penelitian-1'),
			"category"		=> $this->input->post('kategori-penelitian-1'),
			"name"			=> $this->input->post('nama-publikasi-1'),
			"publikasi"		=> $this->input->post('publikasi-1'),
			"tahun"			=> $this->input->post('tahun-1'),
		);

		if ($this->input->post('id-research-1')) {
			$this->mAll->update_data('research',$penelitian1,array('id_research'=>$this->input->post('id-research-1')));
		} elseif ($this->input->post('action-1') == 'insert-res') {
			$this->mAll->sisip_data('research',$penelitian1);
		}

		$penelitian2 = array(
			"id_research"	=> ($this->input->post('id-research-2') != null) ? $this->input->post('id-research-2') : null,
			"applicant"		=> $this->session->userdata('id'),
			"title"			=> $this->input->post('judul-penelitian-2'),
			"category"		=> $this->input->post('kategori-penelitian-2'),
			"name"			=> $this->input->post('nama-publikasi-2'),
			"publikasi"		=> $this->input->post('publikasi-2'),
			"tahun"			=> $this->input->post('tahun-2'),
		);

		if ($this->input->post('id-research-2')) {
			$this->mAll->update_data('research',$penelitian2,array('id_research'=>$this->input->post('id-research-2')));
		} elseif ($this->input->post('action-2') == 'insert-res') {
			$this->mAll->sisip_data('research',$penelitian2);
		}

		$penelitian3 = array(
			"id_research"	=> ($this->input->post('id-research-3') != null) ? $this->input->post('id-research-3') : null,
			"applicant"		=> $this->session->userdata('id'),
			"title"			=> $this->input->post('judul-penelitian-3'),
			"category"		=> $this->input->post('kategori-penelitian-3'),
			"name"			=> $this->input->post('nama-publikasi-3'),
			"publikasi"		=> $this->input->post('publikasi-3'),
			"tahun"			=> $this->input->post('tahun-3'),
		);

		if ($this->input->post('id-research-3')) {
			$this->mAll->update_data('research',$penelitian3,array('id_research'=>$this->input->post('id-research-3')));
		} elseif ($this->input->post('action-3') == 'insert-res') {
			$this->mAll->sisip_data('research',$penelitian3);
		}


		$penelitian4 = array(
			"id_research"	=> ($this->input->post('id-research-4') != null) ? $this->input->post('id-research-4') : null,
			"applicant"		=> $this->session->userdata('id'),
			"title"			=> $this->input->post('judul-penelitian-4'),
			"category"		=> $this->input->post('kategori-penelitian-4'),
			"name"			=> $this->input->post('nama-publikasi-4'),
			"publikasi"		=> $this->input->post('publikasi-4'),
			"tahun"			=> $this->input->post('tahun-4'),
		);

		if ($this->input->post('id-research-4')) {
			$this->mAll->update_data('research',$penelitian4,array('id_research'=>$this->input->post('id-research-4')));
		} elseif ($this->input->post('action-4') == 'insert-res') {
			$this->mAll->sisip_data('research',$penelitian4);
		}


		$penelitian5 = array(
			"id_research"	=> ($this->input->post('id-research-5') != null) ? $this->input->post('id-research-5') : null,
			"applicant"		=> $this->session->userdata('id'),
			"title"			=> $this->input->post('judul-penelitian-5'),
			"category"		=> $this->input->post('kategori-penelitian-5'),
			"name"			=> $this->input->post('nama-publikasi-5'),
			"publikasi"		=> $this->input->post('publikasi-5'),
			"tahun"			=> $this->input->post('tahun-5'),
		);

		if ($this->input->post('id-research-5')) {
			$this->mAll->update_data('research',$penelitian5,array('id_research'=>$this->input->post('id-research-5')));
		} elseif ($this->input->post('action-5') == 'insert-res') {
			$this->mAll->sisip_data('research',$penelitian5);
		}

		/* End Penelitian */

		/* Mengajar */
		$pengajar1 = array(
			"id_teach"			=> ($this->input->post('id-teach-1') != null) ? $this->input->post('id-teach-1') : null,
			"applicant"			=> $this->session->userdata('id'),
			"lecture"			=> $this->input->post('mata-kuliah-1'),
			"semester"			=> $this->input->post('semester-1'),
			"tahun"				=> $this->input->post('tahun-1'),
			"jenjang"			=> $this->input->post('jenjang-1'),
			"prodi"				=> $this->input->post('jurusan-1'),
			"fakultas"			=> $this->input->post('fakultas-1'),
			"perguruan_tinggi"	=> $this->input->post('perguruan-tinggi-1'),
		);

		if ($this->input->post('id-teach-1')) {
			$this->mAll->update_data('teach',$pengajar1,array('id_teach'=>$this->input->post('id-teach-1')));
		} elseif ($this->input->post('action-1') == 'insert-ajar') {
			$this->mAll->sisip_data('teach',$pengajar1);
		}

		$pengajar2 = array(
			"id_teach"			=> ($this->input->post('id-teach-2') != null) ? $this->input->post('id-teach-2') : null,
			"applicant"			=> $this->session->userdata('id'),
			"lecture"			=> $this->input->post('mata-kuliah-2'),
			"semester"			=> $this->input->post('semester-2'),
			"tahun"				=> $this->input->post('tahun-2'),
			"jenjang"			=> $this->input->post('jenjang-2'),
			"prodi"				=> $this->input->post('jurusan-2'),
			"fakultas"			=> $this->input->post('fakultas-2'),
			"perguruan_tinggi"	=> $this->input->post('perguruan-tinggi-2'),
		);

		if ($this->input->post('id-teach-2')) {
			$this->mAll->update_data('teach',$pengajar2,array('id_teach'=>$this->input->post('id-teach-2')));
		} elseif ($this->input->post('action-2') == 'insert-ajar') {
			$this->mAll->sisip_data('teach',$pengajar2);
		}

		$pengajar3 = array(
			"id_teach"			=> ($this->input->post('id-teach-3') != null) ? $this->input->post('id-teach-3') : null,
			"applicant"			=> $this->session->userdata('id'),
			"lecture"			=> $this->input->post('mata-kuliah-3'),
			"semester"			=> $this->input->post('semester-3'),
			"tahun"				=> $this->input->post('tahun-3'),
			"jenjang"			=> $this->input->post('jenjang-3'),
			"prodi"				=> $this->input->post('jurusan-3'),
			"fakultas"			=> $this->input->post('fakultas-3'),
			"perguruan_tinggi"	=> $this->input->post('perguruan-tinggi-3'),
		);

		if ($this->input->post('id-teach-3')) {
			$this->mAll->update_data('teach',$pengajar3,array('id_teach'=>$this->input->post('id-teach-3')));
		} elseif ($this->input->post('action-3') == 'insert-ajar') {
			$this->mAll->sisip_data('teach',$pengajar3);
		}


		$pengajar4 = array(
			"id_teach"			=> ($this->input->post('id-teach-4') != null) ? $this->input->post('id-teach-4') : null,
			"applicant"			=> $this->session->userdata('id'),
			"lecture"			=> $this->input->post('mata-kuliah-4'),
			"semester"			=> $this->input->post('semester-4'),
			"tahun"				=> $this->input->post('tahun-4'),
			"jenjang"			=> $this->input->post('jenjang-4'),
			"prodi"				=> $this->input->post('jurusan-4'),
			"fakultas"			=> $this->input->post('fakultas-4'),
			"perguruan_tinggi"	=> $this->input->post('perguruan-tinggi-4'),
		);

		if ($this->input->post('id-teach-4')) {
			$this->mAll->update_data('teach',$pengajar4,array('id_teach'=>$this->input->post('id-teach-4')));
		} elseif ($this->input->post('action-4') == 'insert-ajar') {
			$this->mAll->sisip_data('teach',$pengajar4);
		}


		$pengajar5 = array(
			"id_teach"			=> ($this->input->post('id-teach-5') != null) ? $this->input->post('id-teach-5') : null,
			"applicant"			=> $this->session->userdata('id'),
			"lecture"			=> $this->input->post('mata-kuliah-5'),
			"semester"			=> $this->input->post('semester-5'),
			"tahun"				=> $this->input->post('tahun-5'),
			"jenjang"			=> $this->input->post('jenjang-5'),
			"prodi"				=> $this->input->post('jurusan-5'),
			"fakultas"			=> $this->input->post('fakultas-5'),
			"perguruan_tinggi"	=> $this->input->post('perguruan-tinggi-5'),
		);

		if ($this->input->post('id-teach-5')) {
			$this->mAll->update_data('teach',$pengajar5,array('id_teach'=>$this->input->post('id-teach-5')));
		} elseif ($this->input->post('action-5') == 'insert-ajar') {
			$this->mAll->sisip_data('teach',$pengajar5);
		}
		/* End of Mengajar */
		}

		if ($_FILES['id-pict']) {
			$name = $this->input->post('id-pict');
			if (!empty($_FILES['id-pict'])) {

				@unlink("./uploads/id/".$name);

				$files  = $_FILES['id-pict']['name'];
				$ext = pathinfo($files, PATHINFO_EXTENSION);
				$nama = 'id-pict-'.$this->session->userdata['id'].'.'.$ext;

				$upload_conf = array(
					'file_name'		=> $nama,
					'upload_path'   => realpath('uploads/id/'),
					'allowed_types' => 'gif|jpg|png',
					'max_size'      => '0',
					"overwrite"		=> TRUE,
				);

				$this->upload->initialize( $upload_conf );

				if($this->upload->do_upload('id-pict')){

                    $data = array(
                    	'applicant' => $this->session->userdata['id'],
                     	'id_pict' 	=> $nama
                    );

                    $docs = $this->mAll->getAllWhere('docs',array('applicant'=>$this->session->userdata['id']));
                    if ($docs) {
                    	$this->mAll->update_data('docs',$data,array('applicant'=>$this->session->userdata['id']));
                    } else {
                    	$this->mAll->sisip_data('docs',$data);
                    }
        }
			}
		}

		// KTP
		if ($_FILES['id-card']) {
			$name = $this->input->post('id-card');
			if (!empty($_FILES['id-card'])) {

				@unlink("./uploads/id_card/".$name);

				$files  = $_FILES['id-card']['name'];
				$ext = pathinfo($files, PATHINFO_EXTENSION);
				$nama = 'id-card-'.$this->session->userdata['id'].'.'.$ext;

				$upload_conf = array(
					'file_name'		=> $nama,
					'upload_path'   => realpath('uploads/id_card/'),
					'allowed_types' => 'gif|jpg|png',
					'max_size'      => '0',
					"overwrite"		=> TRUE,
				);

				$this->upload->initialize( $upload_conf );

				if($this->upload->do_upload('id-card')){

                    $data = array(
                    	'applicant' => $this->session->userdata['id'],
                    	'id_card_pict' 	=> $nama
                    );

                    $docs = $this->mAll->getAllWhere('docs',array('applicant'=>$this->session->userdata['id']));
                    if ($docs) {
                    	$this->mAll->update_data('docs',$data,array('applicant'=>$this->session->userdata['id']));
                    } else {
                    	$this->mAll->sisip_data('docs',$data);
                    }
                }
			}
		}

		// ijazah s1
		if ($_FILES['ijazah-s1']) {
			$name = $this->input->post('ijazah-s1');
			if (!empty($_FILES['ijazah-s1'])) {

				@unlink("./uploads/bachelor/".$name);

				$files  = $_FILES['ijazah-s1']['name'];
				$ext = pathinfo($files, PATHINFO_EXTENSION);
				$nama = 'ijazah-s1-'.$this->session->userdata['id'].'.'.$ext;

				$upload_conf = array(
					'file_name'		=> $nama,
					'upload_path'   => realpath('uploads/bachelor/'),
					'allowed_types' => 'gif|jpg|png',
					'max_size'      => '0',
					"overwrite"		=> TRUE,
				);

				$this->upload->initialize( $upload_conf );

				if($this->upload->do_upload('ijazah-s1')){

                    $data = array(
                    	'applicant' => $this->session->userdata['id'],
                    	'cert_bachelor' 	=> $nama
                    );

                    $docs = $this->mAll->getAllWhere('docs',array('applicant'=>$this->session->userdata['id']));
                    if ($docs) {
                    	$this->mAll->update_data('docs',$data,array('applicant'=>$this->session->userdata['id']));
                    } else {
                    	$this->mAll->sisip_data('docs',$data);
                    }
                }
			}
		}

		// ijazah s2
		if ($_FILES['ijazah-s2']) {
			$name = $this->input->post('ijazah-s2');
			if (!empty($_FILES['ijazah-s2'])) {

				@unlink("./uploads/master/".$name);

				$files  = $_FILES['ijazah-s2']['name'];
				$ext = pathinfo($files, PATHINFO_EXTENSION);
				$nama = 'ijazah-s2-'.$this->session->userdata['id'].'.'.$ext;

				$upload_conf = array(
					'file_name'		=> $nama,
					'upload_path'   => realpath('uploads/master/'),
					'allowed_types' => 'gif|jpg|png',
					'max_size'      => '0',
					"overwrite"		=> TRUE,
				);

				$this->upload->initialize( $upload_conf );

				if($this->upload->do_upload('ijazah-s2')){

                    $data = array(
                    	'applicant' => $this->session->userdata['id'],
                    	'cert_master' 	=> $nama
                    );

                    $docs = $this->mAll->getAllWhere('docs',array('applicant'=>$this->session->userdata['id']));
                    if ($docs) {
                    	$this->mAll->update_data('docs',$data,array('applicant'=>$this->session->userdata['id']));
                    } else {
                    	$this->mAll->sisip_data('docs',$data);
                    }
                }
			}
		}

		// ijazah s3
		if ($_FILES['ijazah-s3']) {
			$name = $this->input->post('ijazah-s3');
			if (!empty($_FILES['ijazah-s3'])) {

				@unlink("./uploads/doctor/".$name);

				$files  = $_FILES['ijazah-s3']['name'];
				$ext = pathinfo($files, PATHINFO_EXTENSION);
				$nama = 'ijazah-s3-'.$this->session->userdata['id'].'.'.$ext;

				$upload_conf = array(
					'file_name'		=> $nama,
					'upload_path'   => realpath('uploads/doctor/'),
					'allowed_types' => 'gif|jpg|png',
					'max_size'      => '0',
					"overwrite"		=> TRUE,
				);

				$this->upload->initialize( $upload_conf );

				if($this->upload->do_upload('ijazah-s3')){

                    $data = array(
                    	'applicant' => $this->session->userdata['id'],
                    	'cert_doctor' 	=> $nama
                    );

                    $docs = $this->mAll->getAllWhere('docs',array('applicant'=>$this->session->userdata['id']));
                    if ($docs) {
                    	$this->mAll->update_data('docs',$data,array('applicant'=>$this->session->userdata['id']));
                    } else {
                    	$this->mAll->sisip_data('docs',$data);
                    }
                }
			}
		}

		// penyetaraan dikti
		if ($_FILES['cert-inter']) {
			$name = $this->input->post('cert-inter');
			if (!empty($_FILES['cert-inter'])) {

				@unlink("./uploads/inter/".$name);

				$files  = $_FILES['cert-inter']['name'];
				$ext = pathinfo($files, PATHINFO_EXTENSION);
				$nama = 'cert-inter-'.$this->session->userdata['id'].'.'.$ext;

				$upload_conf = array(
					'file_name'		=> $nama,
					'upload_path'   => realpath('uploads/inter/'),
					'allowed_types' => 'gif|jpg|png',
					'max_size'      => '0',
					"overwrite"		=> TRUE,
				);

				$this->upload->initialize( $upload_conf );

				if($this->upload->do_upload('cert-inter')){

                    $data = array(
                    	'applicant' => $this->session->userdata['id'],
                    	'cert_inter' 	=> $nama
                    );

                    $docs = $this->mAll->getAllWhere('docs',array('applicant'=>$this->session->userdata['id']));
                    if ($docs) {
                    	$this->mAll->update_data('docs',$data,array('applicant'=>$this->session->userdata['id']));
                    } else {
                    	$this->mAll->sisip_data('docs',$data);
                    }
                }
			}
		}


		// sk mendiknas
		if ($_FILES['sk-mendiknas']) {
			$name = $this->input->post('sk-mendiknas');
			if (!empty($_FILES['sk-mendiknas'])) {

				@unlink("./uploads/mendiknas/".$name);

				$files  = $_FILES['sk-mendiknas']['name'];
				$ext = pathinfo($files, PATHINFO_EXTENSION);
				$nama = 'sk-mendiknas-'.$this->session->userdata['id'].'.'.$ext;

				$upload_conf = array(
					'file_name'		=> $nama,
					'upload_path'   => realpath('uploads/mendiknas/'),
					'allowed_types' => 'gif|jpg|png',
					'max_size'      => '0',
					"overwrite"		=> TRUE,
				);

				$this->upload->initialize( $upload_conf );

				if($this->upload->do_upload('sk-mendiknas')){

                    $data = array(
                    	'applicant' => $this->session->userdata['id'],
                    	'sk_mendiknas' 	=> $nama
                    );

                    $docs = $this->mAll->getAllWhere('docs',array('applicant'=>$this->session->userdata['id']));
                    if ($docs) {
                    	$this->mAll->update_data('docs',$data,array('applicant'=>$this->session->userdata['id']));
                    } else {
                    	$this->mAll->sisip_data('docs',$data);
                    }
                }
			}
		}

		// sk mendiknas
		if ($_FILES['lolos-butuh']) {
			$name = $this->input->post('lolos-butuh');
			if (!empty($_FILES['lolos-butuh'])) {

				@unlink("./uploads/lolos_butuh/".$name);

				$files  = $_FILES['lolos-butuh']['name'];
				$ext = pathinfo($files, PATHINFO_EXTENSION);
				$nama = 'lolos-butuh-'.$this->session->userdata['id'].'.'.$ext;

				$upload_conf = array(
					'file_name'		=> $nama,
					'upload_path'   => realpath('uploads/lolos_butuh/'),
					'allowed_types' => 'gif|jpg|png',
					'max_size'      => '0',
					"overwrite"		=> TRUE,
				);

				$this->upload->initialize( $upload_conf );

				if($this->upload->do_upload('lolos-butuh')){

                    $data = array(
                    	'applicant' => $this->session->userdata['id'],
                    	'paper_lolos_butuh' 	=> $nama
                    );

                    $docs = $this->mAll->getAllWhere('docs',array('applicant'=>$this->session->userdata['id']));
                    if ($docs) {
                    	$this->mAll->update_data('docs',$data,array('applicant'=>$this->session->userdata['id']));
                    } else {
                    	$this->mAll->sisip_data('docs',$data);
                    }
                }
			}
		}

		// sk berhenti
		if ($_FILES['sk-berhenti']) {
			$name = $this->input->post('sk-berhenti');
			if (!empty($_FILES['sk-berhenti'])) {

				@unlink("./uploads/sk_berhenti/".$name);

				$files  = $_FILES['sk-berhenti']['name'];
				$ext = pathinfo($files, PATHINFO_EXTENSION);
				$nama = 'sk-berhenti-'.$this->session->userdata['id'].'.'.$ext;

				$upload_conf = array(
					'file_name'		=> $nama,
					'upload_path'   => realpath('uploads/sk_berhenti/'),
					'allowed_types' => 'gif|jpg|png',
					'max_size'      => '0',
					"overwrite"		=> TRUE,
				);

				$this->upload->initialize( $upload_conf );

				if($this->upload->do_upload('sk-berhenti')){

                    $data = array(
                    	'applicant' => $this->session->userdata['id'],
                    	'sk_berhenti' 	=> $nama
                    );

                    $docs = $this->mAll->getAllWhere('docs',array('applicant'=>$this->session->userdata['id']));
                    if ($docs) {
                    	$this->mAll->update_data('docs',$data,array('applicant'=>$this->session->userdata['id']));
                    } else {
                    	$this->mAll->sisip_data('docs',$data);
                    }
                }
			}
		}


		// sehat jasmani
		if ($_FILES['sehat-jasmani']) {
			$name = $this->input->post('sehat-jasmani');
			if (!empty($_FILES['sehat-jasmani'])) {

				@unlink("./uploads/sehat_jasmani/".$name);

				$files  = $_FILES['sehat-jasmani']['name'];
				$ext = pathinfo($files, PATHINFO_EXTENSION);
				$nama = 'sehat-jasmani-'.$this->session->userdata['id'].'.'.$ext;

				$upload_conf = array(
					'file_name'		=> $nama,
					'upload_path'   => realpath('uploads/sehat_jasmani/'),
					'allowed_types' => 'gif|jpg|png',
					'max_size'      => '0',
					"overwrite"		=> TRUE,
				);

				$this->upload->initialize( $upload_conf );

				if($this->upload->do_upload('sehat-jasmani')){

                    $data = array(
                    	'applicant' => $this->session->userdata['id'],
                    	'sehat_jasmani' 	=> $nama
                    );

                    $docs = $this->mAll->getAllWhere('docs',array('applicant'=>$this->session->userdata['id']));
                    if ($docs) {
                    	$this->mAll->update_data('docs',$data,array('applicant'=>$this->session->userdata['id']));
                    } else {
                    	$this->mAll->sisip_data('docs',$data);
                    }
                }
			}
		}

		// sehat rohani
		if ($_FILES['sehat-rohani']) {
			$name = $this->input->post('sehat-rohani');
			if (!empty($_FILES['sehat-rohani'])) {

				@unlink("./uploads/sehat_rohani/".$name);

				$files  = $_FILES['sehat-rohani']['name'];
				$ext = pathinfo($files, PATHINFO_EXTENSION);
				$nama = 'sehat-rohani-'.$this->session->userdata['id'].'.'.$ext;

				$upload_conf = array(
					'file_name'		=> $nama,
					'upload_path'   => realpath('uploads/sehat_rohani/'),
					'allowed_types' => 'gif|jpg|png',
					'max_size'      => '0',
					"overwrite"		=> TRUE,
				);

				$this->upload->initialize( $upload_conf );

				if($this->upload->do_upload('sehat-rohani')){

                    $data = array(
                    	'applicant' => $this->session->userdata['id'],
                    	'sehat_rohani' 	=> $nama
                    );

                    $docs = $this->mAll->getAllWhere('docs',array('applicant'=>$this->session->userdata['id']));
                    if ($docs) {
                    	$this->mAll->update_data('docs',$data,array('applicant'=>$this->session->userdata['id']));
                    } else {
                    	$this->mAll->sisip_data('docs',$data);
                    }
                }
			}
		}

		// bebas narkotika
		if ($_FILES['bebas-narkotika']) {
			$name = $this->input->post('bebas-narkotika');
			if (!empty($_FILES['bebas-narkotika'])) {

				@unlink("./uploads/bebas_narkotika/".$name);

				$files  = $_FILES['bebas-narkotika']['name'];
				$ext = pathinfo($files, PATHINFO_EXTENSION);
				$nama = 'bebas-narkotika-'.$this->session->userdata['id'].'.'.$ext;

				$upload_conf = array(
					'file_name'		=> $nama,
					'upload_path'   => realpath('uploads/bebas_narkotika/'),
					'allowed_types' => 'gif|jpg|png',
					'max_size'      => '0',
					"overwrite"		=> TRUE,
				);

				$this->upload->initialize( $upload_conf );

				if($this->upload->do_upload('bebas-narkotika')){

                    $data = array(
                    	'applicant' => $this->session->userdata['id'],
                    	'bebas_narkotika' 	=> $nama
                    );

                    $docs = $this->mAll->getAllWhere('docs',array('applicant'=>$this->session->userdata['id']));
                    if ($docs) {
                    	$this->mAll->update_data('docs',$data,array('applicant'=>$this->session->userdata['id']));
                    } else {
                    	$this->mAll->sisip_data('docs',$data);
                    }
                }
			}
		}

		header('Location:'.base_url('index/form_aplikasi'));
		//$this->load->view('index/form_aplikasi');
	}



	public function lamar()
	{
		$this->cekSession();

		$applicant = $this->mAll->getAllWhere('user',array('id_user'=>$this->session->userdata('id')));

		if ($this->session->userdata('id')) {
			$app = $this->mAll->getAllWhere('applicant',array('id_user' => $this->session->userdata('id')));

			$applicant['status'] = (empty($app)) ? '1' : $app;

			$personal = $this->mAll->checkPersonalData($this->session->userdata('id'));
			$edukasi = $this->mAll->getAllWhere('education',array('id_user' => $this->session->userdata('id')));
			$penelitian = $this->mAll->getAllWhere('research',array('applicant' => $this->session->userdata('id')));
			$mengajar = $this->mAll->getAllWhere('teach',array('applicant' => $this->session->userdata('id')));
			$docs = ($applicant['status'] > 1 || $applicant['status'] < 8) ? $this->mAll->checkUploadDocsP1($this->session->userdata('id')) : $this->mAll->checkUploadDocsP2($this->session->userdata('id'));

			$id_dosen = $this->mAll->getAllCustom('applicant','where id_user='.$this->session->userdata('id').' and id_dosen is not null');

			if (((($edukasi && $penelitian) && $mengajar) && $docs) == null) {
				$respons['respon'] 	= '01';
				$respons['mess'] 	= 'Data CV harus diisi';
				$this->alert('Data CV harus diisi');
			} elseif ($id_dosen) {
				$respons['respon'] 	= '01';
				$respons['mess'] 	= 'Anda telah menjadi dosen UAI';
				$this->alert('Anda telah menjadi dosen UAI');
			} else {
				$vacancy = $this->mAll->getAllWhere('applicant', array('id_user'=>$this->session->userdata('id'), 'vacancy'=>$this->input->post('vacancy_id')));
				$max_regis = $this->mAll->getAllCustom('applicant', 'where id_user = '.$this->session->userdata('id'));
				if ($vacancy) {
					$respons['respon'] 	= '01';
					$respons['vacancy']	= $vacancy;
					$respons['mess'] 	= 'Anda telah mendaftar';
					$this->alert('Anda telah mendaftar');
				} elseif (count($max_regis) >= '1') {
					$respons['respon'] 	= '01';
					$respons['vacancy']	= $vacancy;
					$respons['mess'] 	= 'Batas melamar Anda adalah 1 kali';
					$this->alert('Batas melamar Anda adalah 1 kali');
				} else {
					$this->mAll->sisip_data('applicant',array('id_user'=>$this->session->userdata('id'), 'vacancy'=>$this->input->post('vacancy_id'), 'status'=>'2'));
					$respons['respon'] 	= '00';
					$respons['mess'] 	= 'Sukses mendaftar lamaran';
					$this->alert('Sukses mendaftar lamaran');
				}
			}
		} else {
			$respons['respon']	= '02';
		}

		($respons['respon'] == '02') ? redirect(base_url('login')) : $this->lowongan();
	}

	public function alert($msg){
		echo "<script type='text/javascript'>";
		echo "alert('$msg');";
		echo "</script>";
	}

	public function checkUpload(){

	}
}
