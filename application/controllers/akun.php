<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Akun extends CI_Controller {

	/**
	 * Kelas index untuk penanganan halaman awal depan aplikasi
	 *
	 * Created date : 12/04/2017
	 */

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->library('parser');
		$this->load->library('encrypt');
		$this->load->library('session');
		$this->load->model('mUser');
		$this->load->model('mAll');
		
		$this->load->database();
	}

	public function cekSession(){
		if ($this->session->userdata('logged_in')) {
			redirect('/akun');
		} else {
			redirect('/login');
		};
	}

	public function index()
	{	
		$this->cekSession();
		$data = array(
			"title"		=> "UIA - Rekrutmen"
		);

		$this->parser->parse('frontend/common/header',$data);
		$this->parser->parse('frontend/common/menu',$data);
		$this->parser->parse('frontend/index',$data);
		$this->parser->parse('frontend/common/footer',$data);
		$this->parser->parse('frontend/common/foot_script',$data);
	}

	public function update_akun()
	{
		$password = $this->input->post('password');

		$confirm_password = $this->input->post('confirm_password');
		
		if ($password != $confirm_password) {
			$respons['error'] = '01';
			$respons['mess'] = 'Konfirmasi password tidak sesuai';
		} else {
			$result = $this->mAll->update_data('user',array('password'=>$this->encrypt->encode($password)),array('id_user'=>$this->input->post('user_id')));
			if ($result) {
				$respons['error'] = '00';
				$respons['mess'] = 'sukses';
				$respons['redirect'] = base_url('/login/logout');
			} else {
				$respons['error'] = '01';
				$respons['mess'] = 'gagal sisip';
			}
		}

		echo json_encode($respons);

	}
}
	