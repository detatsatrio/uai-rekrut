<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(FCPATH."vendor/thetechnicalcircle/codeigniter_social_login/src/Social.php");

class Login extends CI_Controller {

	/**
	 * Kelas login untuk penanganan login dan proses login
	 *
	 * Created date : 12/04/2017
	 */

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->library('parser');
		$this->load->library('encrypt');
		$this->load->library('session');
		$this->load->model('mUser');
		$this->load->model('mAll');

		$this->load->database();

		$this->menu = array(
			'role'	=> (empty($this->session->userdata['role'])) ? '' : $this->session->userdata['role'],
			'session'	=> $this->session->all_userdata()
		);
	}

	public function cekSession(){
		if ($this->session->userdata('logged_in') || $this->session->userdata('role')=='2') {
			header('location:'.base_url().'index/form_aplikasi');
			exit(0);
		}
	}

	public function logout(){
		unset($_SESSION['oauth_status']);
		unset($_SESSION['userData']);
		$this->session->sess_destroy();
		session_destroy();
		redirect(base_url('/login'));
	}

	public function index()
	{
		$data = array(
			"title"			=> "UIA - Rekrutmen",
			"error"   	=> "00",
			"latest"		=> $this->db->query('select title_vacancy,deadline,desc_vacancy from vacancy where deadline >="'.date('Y-m-d H:i').'" order by id_vacancy')->result_array(),
			//"decode"	=> $this->encrypt->decode('s0RXojIiAS+SKQd0MjJ7Sk+3aJ9ai8OIl1dV7DVqHIlE9IGZYNawxmnNUET10gP9UBnQbJZAlImr7kdJ2sVjFA=='),
			"action"	=> base_url().'login/prosesLogin',
			"linkedin_auth"=> base_url('login/linkedin_oath?oauth_init=1')
		);

		$this->parser->parse('frontend/common/header',$data);
		$this->parser->parse('frontend/common/menu',$this->menu);
		$this->parser->parse('frontend/login',$data);
		$this->parser->parse('frontend/common/footer',$data);
		$this->parser->parse('frontend/common/foot_script',$data);
	}

	public function linkedin_oath(){
		require_once '/../libraries/linkedin/http.php';
		require_once '/../libraries/linkedin/oauth_client.php';

		$apiKey = '81x8a8n95t50y4';
		$apiSecret = 'ZrNaFvBCFea2gQkA';
		$redirectURL = base_url('login/linkedin_oath');
		$scope = 'r_basicprofile r_emailaddress'; //API permissions

		if (isset($_SESSION['oauth_status']) && $_SESSION['oauth_status'] == 'verified' && !empty($_SESSION['userData'])) {
			//Prepare output to show to the user
			$userInfo = $_SESSION['userData'];
			$output = '<div class="login-form">
		        <div class="head">
		            <img src="'.$userInfo['picture'].'" alt=""/>
		        </div>
		        <form>
		        <li>
		            <p>'.$userInfo['first_name'].' '.$userInfo['last_name'].'</p>
		        </li>
		        <li>
		            <p>'.$userInfo['email'].'</p>
		        </li>
				<li>
		            <p>'.$userInfo['locale'].'</p>
		        </li>
		        <div class="foot">
		            <a href="logout.php">Logout</a>
		            <a href="'.$userInfo['link'].'" target="_blank">View Profile</a>
		            <div class="clear"> </div>
		        </div>
		        </form>
			</div>';
		} elseif ((isset($_GET["oauth_init"]) && $_GET["oauth_init"] == 1) || (isset($_GET['oauth_token']) && isset($_GET['oauth_verifier']))) {
			$client = new oauth_client_class;

			$client->client_id = $apiKey;
			$client->client_secret = $apiSecret;
			$client->redirect_uri = $redirectURL;
			$client->scope = $scope;
			$client->debug = false;
			$client->debug_http = true;
			$application_line = __LINE__;

			if(strlen($client->client_id) == 0 || strlen($client->client_secret) == 0){
				die('Please go to LinkedIn Apps page https://www.linkedin.com/secure/developer?newapp= , '.
					'create an application, and in the line '.$application_line.
					' set the client_id to Consumer key and client_secret with Consumer secret. '.
					'The Callback URL must be '.$client->redirect_uri.'. Make sure you enable the '.
					'necessary permissions to execute the API calls your application needs.');
			}

			//If authentication returns success
			if($success = $client->Initialize()){
				if(($success = $client->Process())){
					if(strlen($client->authorization_error)){
						$client->error = $client->authorization_error;
						$success = false;
					}elseif(strlen($client->access_token)){
						$success = $client->CallAPI('http://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,location,picture-url,public-profile-url,formatted-name)',
						'GET',
						array('format'=>'json'),
						array('FailOnAccessError'=>true), $userInfo);
					}
				}
				$success = $client->Finalize($success);
			}

			if($client->exit) exit;

			if($success){
				//Initialize User class
				$user = $this->mAll->getAll('user');

				//Insert or update user data to the database
				$fname = $userInfo->firstName;
				$lname = $userInfo->lastName;
				$inUserData = array(
					'oauth_provider'=> 'linkedin',
					'oauth_uid'     => $userInfo->id,
					'full_name'    => $fname,
					'email'         => $userInfo->emailAddress,
					'username'		=> ''
				);

				$userData = $this->checkUser($inUserData);

				//into proseslogin to activated session
				$ch = curl_init();                    // initiate curl
				$url = base_url('login/prosesLogin');// where you want to post data
				curl_setopt($ch, CURLOPT_URL,$url);
				curl_setopt($ch, CURLOPT_POST, true);  // tell curl you want to post something
				curl_setopt($ch, CURLOPT_POSTFIELDS, "username=".$userData['username']."&password=".$this->encrypt->decode($userData['password']).""); // define what you want to post
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
				$output = curl_exec ($ch); // execute

				curl_close ($ch); // close curl handle

				$data = array(
					'error'		=> '0',
					'id'		=> $userData['id_user'],
                   	'username'  => $fname,
                   	'email'     => $userData['email'],
                   	'role'     	=> $userData['role'],
                   	'password'	=> $this->encrypt->decode($userData['password']),
               		'redirect'	=> (($user[0]['role'] == '1') || ($userData['role'] == '3')) ? base_url('/admin') : base_url('/index/dasbor'),
                   	'logged_in' => 'TRUE'
				);

				$session = $this->session->set_userdata($data);
				//var_dump($output); // show output
				//Redirect the user back to the same page
				//
				header('Location: '.base_url('index/dasbor'));
			}else{
				 $output = '<h3 style="color:red">Error connecting to LinkedIn! try again later!</h3>';
			}
		}elseif(isset($_GET["oauth_problem"]) && $_GET["oauth_problem"] <> ""){
			$output = '<h3 style="color:red">'.$_GET["oauth_problem"].'</h3>';
		}else{
			$authUrl = '?oauth_init=1';
		}
	}

	function checkUser($userData = array()){
        if(!empty($userData)){
            //Check whether user data already exists in database
            $prevQuery = "SELECT * FROM user WHERE oauth_provider = '".$userData['oauth_provider']."' OR oauth_uid = '".$userData['oauth_uid']."' OR email = '".$userData['email']."'";
            $prevResult = $this->db->query($prevQuery);

            if($prevResult->num_rows > 0){
	            //Update user data if already exists
	            $query = "UPDATE user SET username = '".$userData['oauth_uid']."',password= '".$this->encrypt->encode($userData['oauth_provider'])."',full_name = '".$userData['full_name']."', email = '".$userData['email']."', updated = '".date("Y-m-d H:i:s")."', oauth_provider = '".$userData['oauth_provider']."', oauth_uid = '".$userData['oauth_uid']."' WHERE oauth_provider = '".$userData['oauth_provider']."' OR oauth_uid = '".$userData['oauth_uid']."' OR email = '".$userData['email']."'";
	            $update = $this->db->query($query);
	        }else{
	            //Insert user data
	            $query = "INSERT INTO user SET username = '".$userData['oauth_uid']."',password= '".$this->encrypt->encode($userData['oauth_provider'])."',oauth_provider = '".$userData['oauth_provider']."', oauth_uid = '".$userData['oauth_uid']."', full_name = '".$userData['full_name']."', email = '".$userData['email']."',  created = '".date("Y-m-d H:i:s")."', updated = '".date("Y-m-d H:i:s")."', status='1'";
	            $insert = $this->db->query($query);
	        }

	        //Get user data from the database
	        $result = $this->db->query($prevQuery);
	        $userData = $result->row_array();
        }

        //Return user data
        return $userData;
    }

	public function prosesLogin(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if (empty($username && $password)) {
			$result = array(
				"error" 	=> "1",
				"message"	=> "Username dan Password tidak boleh kosong"
			);

		} elseif (empty($username || $password)) {
			$result = array(
				"error" 	=> "1",
				"message"	=> "Username atau Password tidak boleh kosong"
			);

		} elseif ($username && $password) {
			$user = $this->mAll->getAllWhere('user',array('username'=>$username),'array');

			if (isset($user)) {
				(($password == $this->encrypt->decode($user[0]['password'])) && ($user[0]['status'] == '1')) ? $logged = TRUE : $logged = FALSE;

				($user[0]['status'] == '2') ? $activation = false : $activation = TRUE;
			}

			if (($logged == TRUE) && $activation == TRUE) {
				$result = array(
					'error'		=> '0',
					'id'		=> $user[0]['id_user'],
                   	'username'  => $user[0]['username'],
                   	'email'     => $user[0]['email'],
                   	'role'     	=> $user[0]['role'],
                   	'password'	=> $this->encrypt->decode($user[0]['password']),
               		'redirect'	=> (($user[0]['role'] == '1') || ($user[0]['role'] == '3')) ? base_url('/admin') : base_url('/index/dasbor'),
                   	'logged_in' => 'TRUE'
               	);
               	$this->session->set_userdata($result);
            } elseif ($activation == FALSE) {
            	$result = array(
					"error" 	=> "1",
					"username"	=> $username,
					"user"		=> $user,
					"message"	=> "Butuh Aktivasi, Silahkan cek email Anda"
				);
            } else {
				$result = array(
					"error" 	=> "1",
					"username"	=> $username,
					"user"		=> $user,
					"message"	=> "Username atau Password tidak sesuai"
				);

			}
		} else {
			$result = array(
				"error" 	=> "1",
				"message"	=> "Akses ditolak"
			);

		}

		echo json_encode($result);
	}

	public function forgotPassword(){
		$data = array(
			"title"		=> "UIA - Rekrutmen",
			"error"   	=> "00",
			"action"	=> base_url().'login/prosesLogin'
		);

		$this->parser->parse('frontend/common/header',$data);
		$this->parser->parse('frontend/common/menu',$this->menu);
		$this->parser->parse('frontend/forgot_password',$data);
		$this->parser->parse('frontend/common/footer',$data);
		$this->parser->parse('frontend/common/foot_script',$data);
	}

	public function linkedin(){

	}

	public function linkedin_callback(){
		$callback_url   =   base_url('login/linkedin_callback'); //Your callback URL

		$client_id      =   '81x8a8n95t50y4'; // Your LinkedIn Application Client ID
		$client_secret  =   'ZrNaFvBCFea2gQkA';  // Your LinkedIn Application Client Secret

		if ($client_id === '' || $client_secret === '') {
			 echo 'You need a API Key and Secret Key to test the sample code. Get one from <a href="https://www.linkedin.com/developer/apps/">https://www.linkedin.com/developer/apps/</a>';
			 exit;
		}

		if(isset($_GET['code'])) // get code after authorization
		{
		    $url = 'https://www.linkedin.com/uas/oauth2/accessToken';
		    $param = 'grant_type=authorization_code&code='.$_GET['code'].'&redirect_uri='.$callback_url.'&client_id='.$client_id.'&client_secret='.$client_secret;
		    $return = (json_decode(post_curl($url,$param),true)); // Request for access token
		    if($return['error']) // if invalid output error
		    {
		       $content = 'Some error occured<br><br>'.$return['error_description'].'<br><br>Please Try again.';
		    }
		    else // token received successfully
		    {
		       $url = 'https://api.linkedin.com/v1/people/~:(id,firstName,lastName,pictureUrls::(original),headline,publicProfileUrl,location,industry,positions,email-address)?format=json&oauth2_access_token='.$return['access_token'];
		       $User = json_decode(post_curl($url)); // Request user information on received token

		      // Insert Data in Database
		       $query = "INSERT INTO `test`.`users`
		       (`userid`,
		       `firstName`,
		       `lastName`,
		       `emailAddress`,
		       `position`,
		       `location`,
		       `profileURL`,
		       `pictureUrls`,
		       `headline`)

		       VALUES

		       ('$id',
		       '$firstName',
		       '$lastName',
		       '$emailAddress',
		       '$position',
		       '$location',
		       '$profileURL',
		       '$pictureUrls',
		       '$headline')";
		       mysqli_query($connection,$query);
		    }
		}
	}

	public function encode($data){
		$encrypt = $this->encrypt->encode($data);

		echo $encrypt;
		#echo $this->encrypt->decode($encrypt, $encryption_key);
	}

	public function decode($data){
		$encryption_key = "#u4!r3krUtmEn2017#";
		echo $this->encrypt->decode(htmlentities($data), $encryption_key);
	}
}
