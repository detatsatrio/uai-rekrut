<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Kelas admin untuk penanganan belakang aplikasi
	 *
	 * Created date : 12/04/2017
	 */

	public function session(){
		if (is_logged_in() == FALSE) {

		}else{

		}
	}

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url','download'));
		$this->load->library('parser');
		$this->load->library('encrypt');
		$this->load->library('session');
		$this->load->model('mUser');
		$this->load->model('mApplicant');
		$this->load->model('mVacancy');
		$this->load->model('mRole');
		$this->load->model('mAll');

		$this->load->database('default', TRUE);
		$this->mData		= $this->mAll;

		$this->vacancy 		= $this->mData->getAllOrderBy('vacancy','id_vacancy','desc');
		$this->applicant 	= $this->mData->getAll('applicant');
		$this->user 		= $this->mUser->getUser();
		$this->add_user 	= base_url().'admin/user/add_user';
		$this->save_user = base_url().'admin/user/save_user';

		$this->gender 		= $this->mData->getAll('gender');
		$this->marital 		= $this->mData->getAll('marital');
		$this->docs 		= $this->mData->getAll('docs');
		$this->status 		= $this->mData->getAll('status');

		$this->role 		= $this->mRole->getRole();

		$this->add_vacancy  = base_url().'admin/vacancy/add_vacancy';
		$this->save_vacancy = base_url().'admin/vacancy/save_vacancy';

		$user = $this->mAll->getAllWhere('user',array('id_user'=>$this->session->userdata('id')));
		$role = $this->mAll->getAllWhere('role',array('id_role'=>$user['role']));
		$status = $this->mAll->getAllWhere('status_user',array('id_status_user'=>$user['status']));

		$this->head = array(
			'list_vacancy' 	=> base_url().'admin/vacancy/list_vacancy',
			'list_applicant'=> base_url().'admin/applicant/list_applicant',
			'list_user' 	=> base_url().'admin/user/list_user',
			'username'		=> $user['username'],
			'email'				=> $user['email'],
			'role'				=> $role['role'],
			'status'			=> $status['status_user'],
			'id'					=> $this->session->userdata('id'),
			'logout'			=> base_url().'admin/logout'
		);

		$this->title = "Administrator - Rekrutmen Universitas Al-Azhar Indonesia";
	}

	public function cekSession(){
		if ($this->session->userdata('role')==='2') {
			header('location:'.base_url().'login');
			exit(0);
		}
	}

	public function logout(){
		$this->session->sess_destroy();

		redirect(base_url('/login'));
	}

	public function index(){
		$this->cekSession();
		foreach ($this->applicant as $app) {
			foreach ($this->vacancy as $vac) {
				if ($app['vacancy'] == $vac['id_vacancy']) {
					$vacancy_name = $vac['title_vacancy'];
				}
			}
		}
		$data = array(
			"title" 			=> $this->title,
			'applicant'			=> $this->applicant,
			'user'				=> $this->user,
			'vacancy'			=> (isset($vacancy_name)) ? $vacancy_name : "",
			'username'			=> $this->head['username'],
			'add_vacancy'		=> $this->add_vacancy,
			'add_user'			=> $this->add_user,
			'sum_applicant'		=> $this->mData->sumTable('applicant')->jumlah,
			'sum_registered' 	=> $this->mData->sumTable('applicant', "where status='1'")->jumlah,
			'sum_approve_I' 	=> $this->mData->sumTable('applicant', "where status='2'")->jumlah,
			'sum_interview_I' 	=> $this->mData->sumTable('applicant', "where status='3'")->jumlah,
			'sum_psikotest' 	=> $this->mData->sumTable('applicant', "where status='4'")->jumlah,
			'sum_toefl' 		=> $this->mData->sumTable('applicant', "where status='5'")->jumlah,
			'sum_interview_II' 	=> $this->mData->sumTable('applicant', "where status='6'")->jumlah,
			'sum_announcement' 	=> $this->mData->sumTable('applicant', "where status='7'")->jumlah,
			'sum_approve_II' 	=> $this->mData->sumTable('applicant', "where status='8'")->jumlah,
			'sum_created_id' 	=> $this->mData->sumTable('applicant', "where status='9'")->jumlah,
			'sum_notquali' 		=> $this->mData->sumTable('applicant', "where status='10'")->jumlah
		);
		$this->parser->parse('backend/common/header',$data);
		$this->parser->parse('backend/common/head',$this->head);
		$this->parser->parse('backend/index',$data);
		$this->parser->parse('backend/common/footer',$data);
	}

	public function user($action){
		$this->cekSession();
		switch ($action) {
			case 'list_user':
				$data = array(
					"title" 			=> $this->title,
					"user"				=> ($this->session->userdata('role') == '3') ? $this->mData->getAll('user') : $this->mData->getAllWhere('user',"role != '3'", 'array'),
					"add_user"			=> $this->add_user,
					"peran"				=> $this->mData->getAll('role'),
					"status"			=> $this->mData->getAll('status_user'),
					"job"				=> "",
					"description"		=> ""
				);

				$this->parser->parse('backend/common/header',$data);
				$this->parser->parse('backend/common/head',$this->head);
				$this->parser->parse('backend/list_user',$data);
				$this->parser->parse('backend/common/footer',$data);
				break;

			case 'save_user':
				$user = array(
					"username"			=> $this->input->post('username'),
					"password"			=> $this->encrypt->encode($this->input->post('password')),
					"email"				=> $this->input->post('email'),
					"role"				=> $this->input->post('role'),
					"status"			=> $this->input->post('status')
				);

				if (($user['username'] == "") || ($user['password'] == ""))	 {
					$respons['error'] = '02';
					$respons['post_data'] = $this->input->post();
					$respons['mess'] = 'Username / Password must been filled';
				} else {
					if ($this->input->post('action') == 'update') {
						$id_user = $this->input->post('id_user_update');
						$result = $this->mData->update_data('user',$user,array('id_user' => $id_user));

						if ($result) {
							$respons['error'] 	= '00';
							$respons['mess']	= 'Success to update user';
							$respons['post']	= $this->input->post();
							$respons['redirect']= $this->head['list_user'];
						} else {
							$respons['error'] = '01';
							$respons['mess'] = 'Failed to update user';
						}
					} else {
						$users = $this->mData->getAllWhere('user', array('username'=>$this->input->post('username')));
						if ($users) {
							$respons['error'] = "02";
							$respons['mess']  = "username has been used";
						} else {
							$insert = $this->mData->sisip_data('user', $user);

							if ($insert) {
								$respons['error'] 	= '00';
								$respons['mess']	= 'Success to save new user';
								$respons['redirect']= $this->head['list_user'];
							} else {
								$respons['error'] = '01';
								$respons['mess'] = 'Failed to save new user';
							}

						}
					}
				}

				echo json_encode($respons);
				break;

			case 'add_user':
				$data = array(
					"title" 					=> $this->title,
					"username"					=> "",
					"password"					=> "",
					"email"						=> "",
					"role"						=> "",
					"status"					=> "",
					"action"					=> $this->save_user
				);

				$this->parser->parse('backend/common/header',$data);
				$this->parser->parse('backend/common/head',$this->head);
				$this->parser->parse('backend/detail_user',$data);
				$this->parser->parse('backend/common/footer',$data);
				break;

			case 'edit_user':
				$user = $this->mData->getAllWhere('user', "id_user = ".$this->input->post('id_user'));

				if ($user) {
					$result['error'] = '00';
					$result['result'] = $user;
				} else {
					$result['error'] = '01';
					$result['message'] = 'Failed to get data';
				}

				echo json_encode($result);
				break;

			case 'delete_user':

					break;

			default:
				# code...
				break;
		}
	}

	public function delete_user($id_user) {
		if (!$id_user) {
			$result['error'] = '01';
			$result['message'] = 'Data post kosong';
		} else {
			$user = $this->mData->hapus_data('user', "id_user = ".$id_user);

			if ($user) {
				$result['error'] = '00';
				$result['user'] = $this->input->post('id_user');
				$result['result'] = $user;
			} else {
				$result['error'] = '01';
				$result['message'] = 'Failed to get data';
			}
		}

		redirect(base_url('/admin/user/list_user'));
	}

	public function vacancy($action){
		$this->cekSession();
		switch ($action) {
			#case tambah lowongan
			case 'add_vacancy':
				$data = array(
					"title" 					=> $this->title,
					"action"					=> $this->save_vacancy,
					"fakultas"					=> $this->mAll->getAll('fakultas'),
					"prodi"						=> $this->mAll->getAll('prodi'),
					"job"						=> "",
					"description"				=> "",
					"interview_schedule_1"		=> "",
					"interview_description_1"	=> "",
					"psikotest_schedule"		=> "",
					"psikotest_description"		=> "",
					"toefl_schedule"			=> "",
					"toefl_description"			=> "",
					"interview_schedule_2"		=> "",
					"interview_description_2"	=> "",
					"deadline"					=> "",
					"action"					=> $this->save_vacancy
				);

				$this->parser->parse('backend/common/header',$data);
				$this->parser->parse('backend/common/head',$this->head);
				$this->parser->parse('backend/add_vacancy',$data);
				$this->parser->parse('backend/common/footer',$data);
				break;

			case 'list_vacancy':
				$data = array(
					"title" 			=> $this->title,
					'add_vacancy'		=> $this->add_vacancy,
					"fakultas"			=> $this->mAll->getAll('fakultas'),
					"prodi"				=> $this->mAll->getAll('prodi'),
					'vacancy'			=> $this->vacancy,
					'result_vacancy'	=> base_url().'admin/vacancy/result_vacancy',
					'edit'				=> base_url().'admin/vacancy/edit_vacancy',
					'delete'			=> base_url().'admin/vacancy/delete_vacancy',
					"job"				=> "",
					"description"		=> ""
				);

				$this->parser->parse('backend/common/header',$data);
				$this->parser->parse('backend/common/head',$this->head);
				$this->parser->parse('backend/list_vacancy',$data);
				$this->parser->parse('backend/common/footer',$data);
				break;

			#case simpan vacancy
			case 'save_vacancy':
				$vacancy = array(
					"title_vacancy"		=> $this->input->post('job'),
					"fakultas"			=> $this->input->post('fakultas'),
					"prodi"				=> $this->input->post('prodi'),
					"desc_vacancy"		=> $this->input->post('description'),
					"inter_schedule"	=> $this->input->post('interview_schedule_1'),
					"inter_desc"		=> $this->input->post('interview_description_1'),
					"psiko_schedule"	=> $this->input->post('psikotest_schedule'),
					"psiko_desc"		=> $this->input->post('psikotest_description'),
					"toefl_schedule"	=> $this->input->post('toefl_schedule'),
					"toefl_desc"		=> $this->input->post('toefl_description'),
					"inter2_schedule"	=> $this->input->post('interview_schedule_2'),
					"inter2_desc"		=> $this->input->post('interview_description_2'),
					"deadline"			=> $this->input->post('deadline')
				);

				if (($vacancy['title_vacancy'] == '') || ($vacancy['desc_vacancy'] == '') || ($vacancy['inter_schedule'] == '') || ($vacancy['inter_desc'] == '') || ($vacancy['psiko_schedule'] == '') || ($vacancy['psiko_desc'] == '') || ($vacancy['toefl_schedule'] == '') || ($vacancy['toefl_desc'] == '') || ($vacancy['inter2_schedule'] == '') || ($vacancy['inter2_desc'] == '') || ($vacancy['deadline'] == '')) {
					$respons['error'] = '02';
					$respons['post_data'] = $this->input->post();
					$respons['mess'] = 'All data must be filled';
				} else if(($vacancy['inter_schedule'] <= $vacancy['deadline']) || ($vacancy['psiko_schedule'] <= $vacancy['deadline']) || ($vacancy['toefl_schedule'] <= $vacancy['deadline']) || ($vacancy['inter2_schedule'] <= $vacancy['deadline'])) {
					$respons['error'] = '02';
					$respons['post_data'] = $this->input->post();
					$respons['mess'] = 'Schedule must be greater than deadline';
				} elseif (($vacancy['inter_schedule'] > $vacancy['psiko_schedule'])) {
					$respons['error'] = '02';
					$respons['post_data'] = $this->input->post();
					$respons['mess'] = 'Psikotest schedule must be greater than interview schedule';
				} elseif (($vacancy['psiko_schedule'] > $vacancy['toefl_schedule'])) {
					$respons['error'] = '02';
					$respons['post_data'] = $this->input->post();
					$respons['mess'] = 'Toefl schedule must be greater than psikotest schedule';
				} elseif (($vacancy['inter2_schedule'] < $vacancy['toefl_schedule'])) {
					$respons['error'] = '02';
					$respons['post_data'] = $this->input->post();
					$respons['mess'] = 'Interview II schedule must be greater than interview toefl schedule';
				} elseif (($vacancy['inter2_schedule'] < $vacancy['deadline'])) {
					$respons['error'] = '02';
					$respons['post_data'] = $this->input->post();
					$respons['mess'] = 'Deadline schedule must be greater than interview schedule 2';
				} else {
					$id_vacancy = $this->input->post('id_vacancy');
					$result = ($this->input->post('action') == 'update') ? $this->mData->update_data('vacancy',$vacancy,array('id_vacancy' => $id_vacancy)) : $this->mVacancy->sisip_data('vacancy', $vacancy);

					if (isset($result)) {
						$respons['error'] 	= '00';
						$respons['mess']	= 'sukses';
						$respons['post']	= $this->input->post();
						$respons['redirect']= $this->head['list_vacancy'];
					} else {
						$respons['error'] = '01';
						$respons['mess'] = 'gagal sisip';
					}
				}

				echo json_encode($respons);
				break;

			case 'edit_vacancy':
				$this->load->database('default', TRUE);
				$vacancy = $this->mData->getAllWhere('vacancy', "id_vacancy = ".$this->input->post('id_vacancy'));

				if ($vacancy) {
					$result['error'] = '00';
					$result['result'] = $vacancy;
				} else {
					$result['error'] = '01';
					$result['message'] = 'Failed to get data';
				}

				echo json_encode($result);
				break;

			case 'delete_vacancy':
				$this->load->database('default', TRUE);
				$result = $this->mData->hapus_data('vacancy', array('id_vacancy'=>$this->input->post('id_vacancy')));

				if ($result == true) {
					$respons['error'] 	= '00';
					$respons['mess']	= 'sukses';
					$respons['redirect']= $this->head['list_vacancy'];
				} else {
					$respons['error'] = '01';
					$respons['mess'] = 'gagal sisip';
				}

				echo json_encode($respons);

				break;

			case 'result_vacancy':
				$data = array(
					"title" 			=> $this->title,
					'add_vacancy'		=> $this->add_vacancy,
					'edit'				=> base_url().'admin/vacancy/edit_vacancy',
					'delete'			=> base_url().'admin/vacancy/delete_vacancy',
					"applicant" 		=> $this->applicant,
					"user"				=> $this->user,
					"vacancy"			=> $this->mData->getAllWhere("vacancy", "id_vacancy = ".$this->input->post('id_vacancy'), 'array'),
					"announcement"		=> $this->mData->getAllWhere("applicant", array("vacancy"=>$this->input->post('id_vacancy')), 'array'),
					// "sum_interview_1"	=> $this->mData->sumTable("announcement","where id_vacancy = ".$this->input->post('id_vacancy'),"interview_1")->jumlah,
					// "sum_psikotest"		=> $this->mData->sumTable("announcement","where id_vacancy = ".$this->input->post('id_vacancy'),"psikotest")->jumlah,
					// "sum_toefl"			=> $this->mData->sumTable("announcement","where id_vacancy = ".$this->input->post('id_vacancy'),"toefl")->jumlah,
					// "sum_interview_2"	=> $this->mData->sumTable("announcement","where id_vacancy = ".$this->input->post('id_vacancy'),"interview_2")->jumlah,
					"job"				=> "",
					"description"		=> ""
				);
				$this->parser->parse('backend/common/header',$data);
				$this->parser->parse('backend/common/head',$this->head);
				$this->parser->parse('backend/result_vacancy',$data);
				$this->parser->parse('backend/common/footer',$data);
				break;

			case 'result_test':
				if (!$this->input->post('action')) {
					$result = array(
						"error" => "01",
						"mess"	=> "Parameter action tidak ditemukan"
					);
				}  else {
					$value 				= $this->input->post('value');
					$id_applicant	= $this->input->post('id_applicant');
					$id_vacancy		= $this->input->post('id_vacancy');

					switch ($this->input->post('action')) {
						case 'interview_1':
							if ((!$value || !$id_applicant) || !$id_vacancy) {
								$result = array(
									"error" => '01',
									"mess"	=> "Parameter tidak lengkap : value, id_applicant, id_vacancy"
								);
							} else {
								$query = $this->mAll->update_data('applicant',array('interview_1'=>$value),array('id_applicant'=>$id_applicant,'vacancy'=>$id_vacancy));
								if (!$query) {
									$result = array(
										"error" => '01',
										"mess"	=> "Gagal update data"
									);
								} else {
									$result = array(
										"error" => '00',
										"mess"	=> "Sukses update nilai"
									);
								}
							}
							break;

							case 'psikotest':
								if ((!$value || !$id_applicant) || !$id_vacancy) {
									$result = array(
										"error" => '01',
										"mess"	=> "Parameter tidak lengkap : value, id_applicant, id_vacancy"
									);
								} else {
									$query = $this->mAll->update_data('applicant',array('psikotest'=>$value),array('id_applicant'=>$id_applicant,'vacancy'=>$id_vacancy));
									if (!$query) {
										$result = array(
											"error" => '01',
											"mess"	=> "Gagal update data"
										);
									} else {
										$result = array(
											"error" => '00',
											"mess"	=> "Sukses update nilai"
										);
									}
								}
								break;

								case 'toefl':
									if ((!$value || !$id_applicant) || !$id_vacancy) {
										$result = array(
											"error" => '01',
											"mess"	=> "Parameter tidak lengkap : value, id_applicant, id_vacancy"
										);
									} else {
										$query = $this->mAll->update_data('applicant',array('toefl'=>$value),array('id_applicant'=>$id_applicant,'vacancy'=>$id_vacancy));
										if (!$query) {
											$result = array(
												"error" => '01',
												"mess"	=> "Gagal update data"
											);
										} else {
											$result = array(
												"error" => '00',
												"mess"	=> "Sukses update nilai"
											);
										}
									}
									break;

									case 'interview_2':
										if ((!$value || !$id_applicant) || !$id_vacancy) {
											$result = array(
												"error" => '01',
												"mess"	=> "Parameter tidak lengkap : value, id_applicant, id_vacancy"
											);
										} else {
											$query = $this->mAll->update_data('applicant',array('interview_2'=>$value),array('id_applicant'=>$id_applicant,'vacancy'=>$id_vacancy));
											if (!$query) {
												$result = array(
													"error" => '01',
													"mess"	=> "Gagal update data"
												);
											} else {
												$result = array(
													"error" => '00',
													"mess"	=> "Sukses update nilai"
												);
											}
										}
										break;

										case 'interview_1_reason':
											if ((!$value || !$id_applicant) || !$id_vacancy) {
												$result = array(
													"error" => '01',
													"mess"	=> "Parameter tidak lengkap : value, id_applicant, id_vacancy"
												);
											} else {
												$query = $this->mAll->update_data('applicant',array('interview_1_reason'=>$value),array('id_applicant'=>$id_applicant,'vacancy'=>$id_vacancy));
												if (!$query) {
													$result = array(
														"error" => '01',
														"mess"	=> "Gagal update data"
													);
												} else {
													$result = array(
														"error" => '00',
														"mess"	=> "Sukses update reason"
													);
												}
											}
											break;

											case 'psikotest_reason':
												if ((!$value || !$id_applicant) || !$id_vacancy) {
													$result = array(
														"error" => '01',
														"mess"	=> "Parameter tidak lengkap : value, id_applicant, id_vacancy"
													);
												} else {
													$query = $this->mAll->update_data('applicant',array('psikotest_reason'=>$value),array('id_applicant'=>$id_applicant,'vacancy'=>$id_vacancy));
													if (!$query) {
														$result = array(
															"error" => '01',
															"mess"	=> "Gagal update data"
														);
													} else {
														$result = array(
															"error" => '00',
															"mess"	=> "Sukses update reason"
														);
													}
												}
												break;

												case 'toefl_reason':
													if ((!$value || !$id_applicant) || !$id_vacancy) {
														$result = array(
															"error" => '01',
															"mess"	=> "Parameter tidak lengkap : value, id_applicant, id_vacancy"
														);
													} else {
														$query = $this->mAll->update_data('applicant',array('toefl_reason'=>$value),array('id_applicant'=>$id_applicant,'vacancy'=>$id_vacancy));
														if (!$query) {
															$result = array(
																"error" => '01',
																"mess"	=> "Gagal update data"
															);
														} else {
															$result = array(
																"error" => '00',
																"mess"	=> "Sukses update reason"
															);
														}
													}
													break;

													case 'interview_2_reason':
														if ((!$value || !$id_applicant) || !$id_vacancy) {
															$result = array(
																"error" => '01',
																"mess"	=> "Parameter tidak lengkap : value, id_applicant, id_vacancy"
															);
														} else {
															$query = $this->mAll->update_data('applicant',array('interview_2_reason'=>$value),array('id_applicant'=>$id_applicant,'vacancy'=>$id_vacancy));
															if (!$query) {
																$result = array(
																	"error" => '01',
																	"mess"	=> "Gagal update data"
																);
															} else {
																$result = array(
																	"error" => '00',
																	"mess"	=> "Sukses update reason"
																);
															}
														}
														break;

						default:
							$result = array(
								"error" => '01',
								"mess"	=> "Jenis tes tidak ditemukan"
							);
							break;
					}
				}

				echo json_encode($result);
				break;

			case 'pass_test':
				switch ($this->input->post('action')) {
					case 'interview_1':
						$id_applicant = $this->input->post('id_applicant');
						$id_vacancy = $this->input->post('id_vacancy');
						date_default_timezone_set('Asia/Jakarta');
						$vacancy = $this->vacancy;

						foreach ($vacancy as $vaca) {
							if ($vaca['id_vacancy'] == $id_vacancy) {
								$deadline = $vaca['deadline'];
								$date = $vaca['inter_schedule'];
							}
						}

						if (($deadline <= date('Y-m-d H:i')) && ($date <= date('Y-m-d H:i'))) {
							$query = $this->mData->update_data('applicant',array('status'=>'4'),array('id_applicant' => $id_applicant));
							if ($query) {
								$result = array(
									"error" => "00",
									"mess"	=> "Success",
								);
							} else {
								$result = array(
									"error" => "01",
									"mess"	=> "Failed to save"
								);
							}
						} else {
							$result = array(
								"error" => "01",
								"mess"	=> "Deadline case"
							);
						}

						echo json_encode($result);
						break;

					case 'psikotest':
						$id_applicant = $this->input->post('id_applicant');
						$id_vacancy = $this->input->post('id_vacancy');
						date_default_timezone_set('Asia/Jakarta');
						$vacancy = $this->vacancy;

						foreach ($vacancy as $vaca) {
							if ($vaca['id_vacancy'] == $id_vacancy) {
								$deadline = $vaca['deadline'];
								$date = $vaca['psiko_schedule'];
							}
						}

						if (($deadline <= date('Y-m-d H:i')) && ($date <= date('Y-m-d H:i'))) {
							$query = $this->mData->update_data('applicant',array('status'=>'5'),array('id_applicant' => $id_applicant));
							if ($query) {
								$result = array(
									"error" => "00",
									"mess"	=> "Success"
								);
							} else {
								$result = array(
									"error" => "01",
									"mess"	=> "Failed to save"
								);
							}
						} else {
							$result = array(
								"error" => "01",
								"mess"	=> "Deadline case"
							);
						}

						echo json_encode($result);
						break;

					case 'toefl':
						$id_applicant = $this->input->post('id_applicant');
						$id_vacancy = $this->input->post('id_vacancy');

						date_default_timezone_set('Asia/Jakarta');
						$vacancy = $this->vacancy;

						foreach ($vacancy as $vaca) {
							if ($vaca['id_vacancy'] == $id_vacancy) {
								$deadline = $vaca['deadline'];
								$date = $vaca['toefl_schedule'];
							}
						}

						if (($deadline <= date('Y-m-d H:i')) && ($date <= date('Y-m-d H:i'))) {
							$query = $this->mData->update_data('applicant',array('status'=>'6'),array('id_applicant' => $id_applicant));
							if ($query) {
								$result = array(
									"error" => "00",
									"mess"	=> "Success"
								);
							} else {
								$result = array(
									"error" => "01",
									"mess"	=> "Failed to save"
								);
							}
						} else {
							$result = array(
								"error" => "01",
								"mess"	=> "Deadline case"
							);
						}

						echo json_encode($result);
						break;

					case 'interview_2':
						$id_applicant = $this->input->post('id_applicant');
						$id_user =$this->input->post('id_user');
						$id_vacancy = $this->input->post('id_vacancy');
						date_default_timezone_set('Asia/Jakarta');
						$vacancy = $this->vacancy;

						foreach ($vacancy as $vaca) {
							if ($vaca['id_vacancy'] == $id_vacancy) {
								$deadline = $vaca['deadline'];
								$date = $vaca['inter2_schedule'];
							}
						}

						if (($deadline <= date('Y-m-d H:i')) && ($date <= date('Y-m-d H:i'))) {
								// mengambil dokumen
								$docs = $this->db->query("select * from docs where applicant=".$id_user)->row_array();

								// cek apakah persyaratan terakhir sudah diupload jika sudah menuju ke validasi form status 8
								if ($docs == null) {
									$status_docs = '7';
								} else {
									$status_docs = (($docs['sk_berhenti'] && $docs['sehat_jasmani'] && $docs['sehat_rohani'] && $docs['bebas_narkotika']) == NULL) ? '7' : '8';
								}

								$query = $this->mAll->update_data('applicant',array('status'=>$status_docs),array('id_applicant' => $id_applicant, 'vacancy' => $id_vacancy));

								if ($query) {
									$result = array(
										"error" => "00",
										"mess"	=> "Success",
										"status_docs" => $status_docs
								);
							} else {
								$result = array(
									"error" => "01",
									"mess"	=> "Failed to save"
								);
							}
						} else {
							$result = array(
								"error" => "01",
								"mess"	=> "Deadline case"
							);
						}

						echo json_encode($result);
						break;

					case 'create_id':
						$id_user = $this->input->post('id_user');

						// Update lowongan dengan user yang sama ke status not qualified
						$update = $this->mAll->update_data('applicant',array('status'=>'10'),array('id_user'=>$id_user));

						// Generate ID Dosen

						/*
							xx => Fakultas
							xx => Jurusan/Prodi
							xxxx => Incremental Dosen
						*/

						foreach ($this->vacancy as $vac) {
							if ($vac['id_vacancy'] == $this->input->post('id_vacancy')) {
								$fakultas = str_pad($vac['fakultas'], 2, '0', STR_PAD_LEFT);
								$prodi = str_pad($vac['prodi'], 2, '0', STR_PAD_LEFT);
							}
						}

						$sum_dosen = $this->mAll->sumTableVacancy('applicant','where id_dosen is not null');

						$incremen_id = ($sum_dosen['jumlah']) ? $sum_dosen['jumlah'] + 1 : 1;

						$id_dosen = $fakultas.$prodi.str_pad($incremen_id, 4, '0', STR_PAD_LEFT);

						// Update ke database id dosen
						$update_data = $this->mAll->update_data('applicant',array('id_dosen'=>$id_dosen,'status'=>'11'),array('id_applicant'=>$this->input->post('id_applicant')));

						if ($update && $update_data) {
							$result['respon'] = '00';
							$result['mess']	  = 'sukses';
						} else {
							$result['respon'] = '01';
							$result['update_data'] = $update_data;
							$result['update']	= $update;
							$result['mess']	  = 'failed';
						}

						echo json_encode($result);
						break;

					default:
						$result['error'] = '01';
						$result['mess']	= 'action required';
						echo json_encode($result);
						break;
				}
				break;

			default:
				$data = array(
					"title" 			=> $this->title,
					"job"				=> "",
					"description"		=> ""
				);

				$this->parser->parse('backend/common/header',$data);
				$this->parser->parse('backend/common/head',$this->head);
				$this->parser->parse('backend/list_vacancy',$data);
				$this->parser->parse('backend/common/footer',$data);
				break;
		}
	}

	public function applicant($action){
		$this->cekSession();
		switch ($action) {
			case 'list_applicant':
				$data = array(
					"title" 			=> $this->title,
					"applicant"		=> $this->applicant,
					"user"				=> $this->user,
					"education"		=> $this->mAll->getAll('education'),
					"research"		=> $this->mAll->getAll('research'),
					"teach"				=> $this->mAll->getAll('teach'),
					"status"			=> $this->status,
					"docs"				=> $this->docs,
					"vacancy"			=> isset($this->vacancy) ? $this->vacancy : '',
					"jenkel"			=> $this->gender,
					"marital"			=> $this->marital,
					"role"				=> $this->role,
					"jenjang"			=> $this->mAll->getAll('jenjang'),
					"job"					=> "",
					"description"		=> ""
				);

				$this->parser->parse('backend/common/header',$data);
				$this->parser->parse('backend/common/head',$this->head);
				$this->parser->parse('backend/list_applicant',$data);
				$this->parser->parse('backend/common/footer',$data);

				break;

			case 'view_applicant_mdl':
				$id = $this->input->post('id_user');

				if (!$this->input->post()) {
					$result = array(
						'error' => '01',
						'message'=> 'Data post tidak ditemukan'
					);
				} else {
					// Get Data Personal
					$personal = $this->mAll->getAllWhere('user',array("id_user"=>$id));

					// Get data pendidikan
					$pendidikan = $this->mAll->getAllWhere('education',array("id_user"=>$id),'array');

					// Get data penelitian
					$penelitian = $this->mAll->getAllWhere('research',array("applicant"=>$id),'array');

					// Get data mengajar
					$mengajar = $this->mAll->getAllWhere('teach',array("applicant"=>$id),'array');

					$result = array(
						'error' => '00',
						'personal'=> $personal,
						'pendidikan'=> $pendidikan,
						'penelitian'=> $penelitian,
						'mengajar'=>$mengajar
					);
				}

				echo json_encode($result);

				break;

			case 'edit_applicant':
				$applicant = $this->mData->getAllWhere('applicant', "id_applicant = ".$this->input->post('id_applicant'));

				if ($applicant) {
					$result['error'] = '00';
					$result['result'] = $applicant;
				} else {
					$result['error'] = '01';
					$result['message'] = 'Failed to get data';
				}

				echo json_encode($result);
				break;
			case 'approve_applicant':
				$id = $this->input->post('id_applicant');
				$status_user = $this->mData->getValue('applicant', array('id_applicant'=>$id), 'status');
				if ($this->input->post()!='') {

					$result = $this->mData->update_data('applicant',array("status"=>($status_user['status'] == '2') ? '3' : '12'),array('id_applicant'=>$id));

					if ($result) {
						$respons['error'] 	= '00';
						$respons['mess']	= 'sukses';
						$respons['post']	= $this->input->post();
						$respons['redirect']= $this->head['list_applicant'];
					} else {
						$respons['error'] = '01';
						$respons['mess'] = 'gagal sisip';
					}
				} else {
					$respons['error'] = '01';
					$respons['post']	= $this->input->post();
					$respons['mess'] = 'Failed';
				}
				echo json_encode($respons);
				break;
			case 'reject_applicant':

				$id = $this->input->post('id_applicant');

				if ($this->input->post()!='') {
					$result = $this->mData->update_data('applicant',array("status"=>'10'),array('id_applicant'=>$id));

					if (isset($result)) {
						$respons['error'] 	= '00';
						$respons['mess']	= 'sukses';
						$respons['post']	= $this->input->post();
						$respons['redirect']= $this->head['list_applicant'];
					} else {
						$respons['error'] = '01';
						$respons['mess'] = 'gagal sisip';
					}
				} else {
					$respons['error'] = '01';
					$respons['post']	= $this->input->post();
					$respons['mess'] = 'Failed';
				}

				echo json_encode($respons);
				break;

			case 'download_form':
				$id_app = '4';

				$data = $this->mData->getAllWhere('applicant',array('id_applicant'=>$id_app),'row');

				// Load all views as normal
				$this->parser->parse('backend/common/header',$data);
				$this->parser->parse('backend/applicant_form_download',$data);
				$this->parser->parse('backend/common/footer',$data);

				// Get output html
				$html = $this->output->get_output();

				// Load library
				$this->load->library('dompdf_gen');

				// Convert to PDF
				$this->dompdf->load_html($html);
				$this->dompdf->render();
				$this->dompdf->stream('CALDOS-'.str_pad($id_app, 4, '0', STR_PAD_LEFT).' ('.$data['full_name'].').pdf');
				break;

			default:
				# code...
				break;
		}
	}

	public function report_file($type,$id_vacancy=null){
		/** Error reporting */
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Europe/London');

		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');

		/** Include PHPExcel */
		require_once dirname(__FILE__) . '/../libraries/phpexcel/Classes/PHPExcel.php';


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		switch ($type) {
			case 'vacancy':

				// Add some data
				$objPHPExcel->setActiveSheetIndex(0)
				            ->setCellValue('A1', 'ID Applicant')
				            ->setCellValue('B1', 'ID Dosen')
				            ->setCellValue('C1', 'Nama Pelamar')
				            ->setCellValue('D1', 'Interview I')
				            ->setCellValue('E1', 'Psikotest')
				            ->setCellValue('F1', 'TOEFL')
				            ->setCellValue('G1', 'Interview II')
				            ->setCellValue('H1', 'Status');

				$applicant  = $this->mAll->getAllWhere('applicant',array('vacancy'=>$id_vacancy),'array');
				$docs		= $this->mAll->getAll('docs');
				$status 	= $this->mAll->getAll('status');
				$user 		= $this->mAll->getAll('user');
				$vacancy 	= $this->mAll->getAll('vacancy');

				foreach ($vacancy as $va) {
					($va['id_vacancy'] == $id_vacancy) ? $nama_vacancy = $va['title_vacancy'] : $nama_vacancy = '-';
				}
				// Style
				$styleArray = array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => '2F4F4F')
			        ),
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			        )
			    );

			    $color_status = array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '03A678')
			        )
			    );

				// Header style
			    $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);

			    $center = array(
			    	'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			        )
			    );

			    $border = array(
			    	'borders' => array(
			    		'allborders'    => array(
			    			'style' => PHPExcel_Style_Border::BORDER_THIN,
			    			)
			    		)
			    );

				$i = 2;
				$work = $objPHPExcel->setActiveSheetIndex(0);
				foreach ($applicant as $app) {
					$work->setCellValue('A'.$i, 'CALDOS-'.$app['id_applicant']);

					($app['id_dosen']=='') ? $work->setCellValue('B'.$i, '-') : $work->setCellValue('B'.$i, $app['id_dosen']);

					foreach ($user as $u) {
						($u['id_user'] == $app['id_user']) ? $work->setCellValue('C'.$i, $u['full_name']) : '-';
					}

					($app['status'] >= '4') ? $work->setCellValue('D'.$i, '✔') : $work->setCellValue('D'.$i, '-');
					($app['status'] >= '5') ? $work->setCellValue('E'.$i, '✔') : $work->setCellValue('E'.$i, '-');
					($app['status'] >= '6') ? $work->setCellValue('F'.$i, '✔') : $work->setCellValue('F'.$i, '-');
					($app['status'] >= '7') ? $work->setCellValue('G'.$i, '✔') : $work->setCellValue('G'.$i, '-');
					foreach ($status as $s) {
						($s['id_status'] == $app['status']) ? $work->setCellValue('H'.$i, $s['status']) : '-';
					}
					// Header style
			    	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->applyFromArray($center);
					// $objPHPExcel->getActiveSheet()->getStyle('P'.$i)->applyFromArray($color_status);
					$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$i)->applyFromArray($border);
					$i++;
				}

				foreach(range('A','H') as $columnID) {
				    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
				        ->setAutoSize(true);
				}

				// Rename worksheet
				$objPHPExcel->getActiveSheet()->setTitle($nama_vacancy);


				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$objPHPExcel->setActiveSheetIndex(0);
				break;

			default:

				// Add some data
				$objPHPExcel->setActiveSheetIndex(0)
				            ->setCellValue('A1', 'ID Applicant')
				            ->setCellValue('B1', 'Nama Applicant')
				            ->setCellValue('C1', 'Nama Lowongan')
				            ->setCellValue('D1', 'ID')
				            ->setCellValue('E1', 'KTP')
				            ->setCellValue('F1', 'Ijazah S1')
				            ->setCellValue('G1', 'Ijazah S2')
				            ->setCellValue('H1', 'Ijazah S3')
				            ->setCellValue('I1', 'Penyetaraan Dikti')
				            ->setCellValue('J1', 'SK Mendiknas')
				            ->setCellValue('K1', 'Paper Lolos Butuh')
				            ->setCellValue('L1', 'SK Berhenti')
				            ->setCellValue('M1', 'Sehat Jasmani')
				            ->setCellValue('N1', 'Sehat Rohani')
				            ->setCellValue('O1', 'Bebas Narkotika')
				            ->setCellValue('P1', 'Status');

				$applicant  = $this->mAll->getAll('applicant');
				$docs		= $this->mAll->getAll('docs');
				$status 	= $this->mAll->getAll('status');
				$user 		= $this->mAll->getAll('user');

				// Style
				$styleArray = array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => '2F4F4F')
			        ),
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			        )
			    );

			    $color_status = array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '03A678')
			        )
			    );

				// Header style
			    $objPHPExcel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($styleArray);

			    $center = array(
			    	'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			        )
			    );

			    $border = array(
			    	'borders' => array(
			    		'allborders'    => array(
			    			'style' => PHPExcel_Style_Border::BORDER_THIN,
			    			)
			    		)
			    );

				$i = 2;
				$work = $objPHPExcel->setActiveSheetIndex(0);
				foreach ($applicant as $app) {
					$work->setCellValue('A'.$i, 'CALDOS-'.$app['id_applicant']);

					foreach ($user as $u) {
						if ($u['id_user'] == $app['id_user']) {
							$work->setCellValue('B'.$i, $u['full_name']);
						}
					}

					foreach ($this->vacancy as $vac) {
						if ($vac['id_vacancy'] == $app['vacancy']) {
							$work->setCellValue('C'.$i, $vac['title_vacancy']);
						} else {
							foreach ($this->vacancy as $v) {
								if ($v['id_vacancy'] == $app['vacancy']) {
									$work->setCellValue('C'.$i, $v['title_vacancy']);
								}
							}
						}
					}
					foreach ($docs as $d) {
						if (($d['applicant']==$app['id_user']) && ($d['id_pict'] != null)) {
							$work->setCellValue('D'.$i, '✔');
						} else {
							$work->setCellValue('D'.$i, '-');
						}
					}
					foreach ($docs as $d) {
						if (($d['applicant']==$app['id_user']) && ($d['id_card_pict'] != null)) {
							$work->setCellValue('E'.$i, '✔');
						} else {
							$work->setCellValue('E'.$i, '-');
						}
					}
					foreach ($docs as $d) {
						if (($d['applicant']==$app['id_user']) && ($d['cert_bachelor'] != null)) {
							$work->setCellValue('F'.$i, '✔');
						} else {
							$work->setCellValue('F'.$i, '-');
						}
					}

					foreach ($docs as $d) {
						if (($d['applicant']==$app['id_user']) && ($d['cert_master'] != null)) {
							$work->setCellValue('G'.$i, '✔');
						} else {
							$work->setCellValue('G'.$i, '-');
						}
					}
					foreach ($docs as $d) {
						if (($d['applicant']==$app['id_user']) && ($d['cert_doctor'] != null)) {
							$work->setCellValue('H'.$i, '✔');
						} else {
							$work->setCellValue('H'.$i, '-');
						}
					}
					foreach ($docs as $d) {
						if (($d['applicant']==$app['id_user']) && ($d['cert_inter'] != null)) {
							$work->setCellValue('I'.$i, '✔');
						} else {
							$work->setCellValue('I'.$i, '-');
						}
					}
					foreach ($docs as $d) {
						if (($d['applicant']==$app['id_user']) && ($d['sk_mendiknas'] != null)) {
							$work->setCellValue('J'.$i, '✔');
						} else {
							$work->setCellValue('J'.$i, '-');
						}
					}
					foreach ($docs as $d) {
						if (($d['applicant']==$app['id_user']) && ($d['paper_lolos_butuh'] != null)) {
							$work->setCellValue('K'.$i, '✔');
						} else {
							$work->setCellValue('K'.$i, '-');
						}
					}
					foreach ($docs as $d) {
						if (($d['applicant']==$app['id_user']) && ($d['sk_berhenti'] != null)) {
							$work->setCellValue('L'.$i, '✔');
						} else {
							$work->setCellValue('L'.$i, '-');
						}
					}
					foreach ($docs as $d) {
						if (($d['applicant']==$app['id_user']) && ($d['sehat_jasmani'] != null)) {
							$work->setCellValue('M'.$i, '✔');
						} else {
							$work->setCellValue('M'.$i, '-');
						}
					}

					foreach ($docs as $d) {
						if (($d['applicant']==$app['id_user']) && ($d['sehat_rohani'] != null)) {
							$work->setCellValue('N'.$i, '✔');
						} else {
							$work->setCellValue('N'.$i, '-');
						}
					}
					foreach ($docs as $d) {
						if (($d['applicant']==$app['id_user']) && ($d['bebas_narkotika'] != null)) {
							$work->setCellValue('O'.$i, '✔');
						} else {
							$work->setCellValue('O'.$i, '-');
						}
					}

					foreach ($status as $s) {
						if ($s['id_status'] == $app['status']) {
							$work->setCellValue('P'.$i, $s['status']);
						}
					}

					// Header style
			    	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($center);
					// $objPHPExcel->getActiveSheet()->getStyle('P'.$i)->applyFromArray($color_status);
					$objPHPExcel->getActiveSheet()->getStyle('A1:P'.$i)->applyFromArray($border);
					$i++;
				}

				foreach(range('A','P') as $columnID) {
				    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
				        ->setAutoSize(true);
				}



			    // Freeze panes
				//$objPHPExcel->getActiveSheet()->freezePane('A1:P1');



				// Rename worksheet
				$objPHPExcel->getActiveSheet()->setTitle('All Applicant');


				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$objPHPExcel->setActiveSheetIndex(0);

				break;
		}

		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="[Report] All Applicant.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		ob_end_clean();
		$objWriter->save('php://output');
		exit;

	}

	public function download_file($action, $name){
		$this->cekSession();
		//load library download
		$this->load->helper('download');

		if(!empty($name)){
			ob_clean();
			switch ($action) {
				case 'id':
					$data = file_get_contents(base_url('uploads/id/'.$name));
					break;
				case 'id_card':
					$data = file_get_contents(base_url('uploads/id_card/'.$name));
					break;

				case 'bachelor':
					$data = file_get_contents(base_url('uploads/bachelor/'.$name));
					break;

				case 'master':
					$data = file_get_contents(base_url('uploads/master/'.$name));
					break;

				case 'doctor':
					$data = file_get_contents(base_url('uploads/doctor/'.$name));
					break;

				case 'inter':
					$data = file_get_contents(base_url('uploads/inter/'.$name));
					break;

				case 'mendiknas':
					$data = file_get_contents(base_url('uploads/mendiknas/'.$name));
					break;

				case 'lolos_butuh':
					$data = file_get_contents(base_url('uploads/lolos_butuh/'.$name));
					break;

				case 'sk_berhenti':
					$data = file_get_contents(base_url('uploads/sk_berhenti/'.$name));
					break;

				case 'sehat_rohani':
					$data = file_get_contents(base_url('uploads/sehat_rohani/'.$name));
					break;

				case 'sehat_jasmani':
					$data = file_get_contents(base_url('uploads/sehat_jasmani/'.$name));
					break;

				case 'bebas_narkotika':
					$data = file_get_contents(base_url('uploads/bebas_narkotika/'.$name));
					break;


			}
			force_download($name, $data);
		} else {
			echo "alert('Name is empty')";
		}
	}

	public function getFakultas(){
		$fakultas = $this->mAll->getAllCustom("fakultas","where IDFakultas != '0'");

		if ($fakultas) {
			$result['respon'] 	= '00';
			$result['mess']		= $fakultas;
		} else {
			$result['respon']	= '01';
		}

		echo json_encode($result);
	}

	public function getProdi(){
		$idFakultas = $this->input->get('idfakultas');

		if ($idFakultas) {
			$prodi = $this->mAll->getAllWhere('prodi',array('IDFakultas'=>$idFakultas), 'array');
		} else {
			$prodi = $this->mAll->getAllCustom('prodi',"where IDProgdi != '0'");
		}


		if ($prodi) {
			$result['respon'] 	= '00';
			$result['mess']		= $prodi;
		} else {
			$result['respon'] 	= '01';
			$result['mess']		= "prodi not found";
		}


		echo json_encode($result);
	}

	public function Zip($id)
	{
		ob_clean();
		$docs = $this->mAll->getAllWhere('docs',array('applicant'=>$id));

		($docs['id_pict'] == NULL) ? '' : $this->zip->read_file('uploads/id/'.$docs['id_pict']);
		($docs['id_card_pict'] == NULL) ? '' : $this->zip->read_file('uploads/id_card/'.$docs['id_card_pict']);

		($docs['cert_bachelor'] == NULL) ? '' : $this->zip->read_file('uploads/bachelor/'.$docs['cert_bachelor']);
		($docs['cert_master'] == NULL) ? '' : $this->zip->read_file('uploads/master/'.$docs['cert_master']);
		($docs['cert_doctor'] == NULL) ? '' : $this->zip->read_file('uploads/doctor/'.$docs['cert_doctor']);
		($docs['cert_inter'] == NULL) ? '' : $this->zip->read_file('uploads/inter/'.$docs['cert_inter']);
		($docs['sk_mendiknas'] == NULL) ? '' : $this->zip->read_file('uploads/mendiknas/'.$docs['sk_mendiknas']);
		($docs['paper_lolos_butuh'] == NULL) ? '' : $this->zip->read_file('uploads/lolos_butuh/'.$docs['paper_lolos_butuh']);
		($docs['sk_berhenti'] == NULL) ? '' : $this->zip->read_file('uploads/sk_berhenti/'.$docs['sk_berhenti']);

		($docs['sehat_jasmani'] == NULL) ? '' : $this->zip->read_file('uploads/sehat_jasmani/'.$docs['sehat_jasmani']);
		($docs['sehat_rohani'] == NULL) ? '' : $this->zip->read_file('uploads/sehat_rohani/'.$docs['sehat_rohani']);
		($docs['bebas_narkotika'] == NULL) ? '' : $this->zip->read_file('uploads/bebas_narkotika/'.$docs['bebas_narkotika']);

		$this->zip->download('Docs-'.$id.'.zip');
	}

}
