<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function is_logged_in() {
    
    // Get current CodeIgniter instance
    $ci =& get_instance();
    
    // We need to use $CI->session instead of $this->session
    $user = $ci->session->userdata('is_login');
    $ci->load->helper('url');

    if (!isset($user)) {
     	redirect('login', 'refresh'); 
 	} else {
 	 	if ($ci->session->userdata('role')=='1') {
			redirect(base_url('/admin'));
		}else{
			redirect(base_url('/akun'));
		}
 	}
}