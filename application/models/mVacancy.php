<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mVacancy extends CI_Model {
	
	/*
	*	Nama File : mApplicant.php
	*	Deskripsi : kelas untuk memproses data di database
	*	Developer : Skyesprojects | 2017		
	*/

	public function __construct(){
		parent::__construct();
		$this->load->database('default', TRUE);
	}

	function getVacancy(){
		$data = $this->db->get('vacancy');
		return $data->result_array();
	}

	function getVacancyWhere($id){
		$this->db->where('id_vacancy', $id);
		
		return $this->db->get('vacancy')->row_array();
	}

	function sumVacancy($where = ''){
		return $this->db->query("select count(*) as jumlah from vacancy $where")->row();
	}

	public function sisip_data($tabel,$data){
		return $this->db->insert($tabel,$data);
	}

	public function update_data($tabel,$data,$where){
		return $this->db->update($tabel,$data,$where);
	}

	public function hapus_data($tabel,$where){
		return $this->db->delete($tabel,$where);
	}
		
}

?>

