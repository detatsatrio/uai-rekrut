<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mRole extends CI_Model {
	
	/*
	*	Nama File : mUser.php
	*	Deskripsi : kelas untuk memproses data di database
	*	Developer : Skyesprojects | 2017		
	*/

	public function __construct(){
		parent::__construct();
		$this->load->database('default', TRUE);
	}

	function getRole(){
		$data = $this->db->get('role');
		return $data->result_array();
	}

	function getRoleWhere($id){
		$this->db->where('id_role', $id);
		
		return $this->db->get('role')->row_array();
	}

	function sumrole($where = ''){
		return $this->db->query("select count(*) as jumlah from role $where")->row();
	}

	public function sisip_data($tabel,$data){
		return $this->db->insert($tabel,$data);
	}

	public function update_data($tabel,$data,$where){
		return $this->db->update($tabel,$data,$where);
	}

	public function hapus_data($tabel,$where){
		return $this->db->delete($tabel,$where);
	}
		
}

?>

