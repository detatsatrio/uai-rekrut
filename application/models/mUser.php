<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mUser extends CI_Model {
	
	/*
	*	Nama File : mUser.php
	*	Deskripsi : kelas untuk memproses data di database
	*	Developer : Skyesprojects | 2017		
	*/

	public function __construct(){
		parent::__construct();
		$this->load->database('default', TRUE);
	}

	function getUser(){
		$data = $this->db->get('user');
		return $data->result_array();
	}

	function getLogin($username, $password){
		return $this->db->get_where('user', array('username' => $username, 'password' => $password, 'status' => '1'));
	}

	function getUserWhere($id){
		$this->db->where('id_user', $id);
		
		return $this->db->get('user')->row_array();
	}

	function sumUser($where = ''){
		return $this->db->query("select count(*) as jumlah from user $where")->row();
	}

	public function sisip_data($tabel,$data){
		return $this->db->insert($tabel,$data);
	}

	public function update_data($tabel,$data,$where){
		return $this->db->update($tabel,$data,$where);
	}

	public function hapus_data($tabel,$where){
		return $this->db->delete($tabel,$where);
	}
		
}

?>

