<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mApplicant extends CI_Model {
	
	/*
	*	Nama File : mApplicant.php
	*	Deskripsi : kelas untuk memproses data di database
	*	Developer : Skyesprojects | 2017		
	*/

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function getApplicant(){
		$data = $this->db->get('applicant');
		return $data->result_array();
	}

	function sumApplicant($where = ''){
		return $this->db->query("select count(*) as jumlah from applicant $where")->row();
	}

	function getApplicantWhere($username, $password){
		$sql = "SELECT * FROM applicant WHERE username = ? and password = ?";
		
		return $this->db->query($sql, array($username, $password));
	}

	public function sisip_data($tabel,$data){
		return $this->db->insert($tabel,$data);
	}

	public function update_data($tabel,$data,$where){
		return $this->db->update($tabel,$data,$where);
	}

	public function hapus_data($tabel,$where){
		return $this->db->delete($tabel,$where);
	}
		
}

?>

