<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mAll extends CI_Model {

	/*
	*	Nama File : mApplicant.php
	*	Deskripsi : kelas untuk memproses data di database
	*	Developer : Skyesprojects | 2017
	*/

	public function __construct(){
		parent::__construct();
		$this->load->database('default', TRUE);
	}

	function getAll($table){
		$data = $this->db->get($table);
		return $data->result_array();
	}

	function getAllWhere($table, $where, $result='row'){
		$this->db->where($where);

		if ($result == 'row') {
			return $this->db->get($table)->row_array();
		} elseif($result == 'array') {
			return $this->db->get($table)->result_array();
		}

	}

	function getAllOrderBy($table, $col, $order){
		$this->db->order_by($col, $order);
		return $this->db->get($table)->result_array();
	}

	function getAllCustom($table, $where){
		return $this->db->query("select * from $table $where")->result_array();
	}

	function getValue($table, $where, $value){
		$this->db->where($where);
		$this->db->select($value);
		return $this->db->get($table)->row_array();
	}

	function sumTable($table, $where = '', $column = '*'){
		return $this->db->query("select count($column) as jumlah from $table $where")->row();
	}

	function sumTableVacancy($table, $where = '', $column = '*'){
		return $this->db->query("select count($column) as jumlah from $table $where")->row_array();
	}

	public function sisip_data($tabel,$data){
		return $this->db->insert($tabel,$data);
	}

	public function update_data($tabel,$data,$where){
		return $this->db->update($tabel,$data,$where);
	}

	public function hapus_data($tabel,$where){
		return $this->db->delete($tabel,$where);
	}

	public function checkPersonalData($id_user){
		return $this->db->query("SELECT
		  *
		FROM
		  USER
		WHERE id_user = '$id_user'
		  AND full_name IS NOT NULL
		  AND birth_place IS NOT NULL
		  AND birth_date IS NOT NULL
		  AND gender IS NOT NULL
		  AND marital IS NOT NULL
		  AND mother_name IS NOT NULL
		  AND address IS NOT NULL
		  AND id_card_no IS NOT NULL
		  AND homephone_no IS NOT NULL
		  AND handphone_no IS NOT NULL")->row_array();
	}

	public function checkUploadDocsP1($id_user){
		return $this->db->query("SELECT
		  *
		FROM
		  docs
		WHERE applicant = '$id_user'
		AND id_pict IS NOT NULL
		AND id_card_pict IS NOT NULL
		AND cert_bachelor IS NOT NULL
		AND cert_master IS NOT NULL
		AND cert_inter IS NOT NULL
		AND sk_mendiknas IS NOT NULL
		AND paper_lolos_butuh IS NOT NULL")->row_array();
	}

	public function checkUploadDocsP2($id_user){
		return $this->db->query("SELECT
		  *
		FROM
		  docs
		WHERE applicant = '$id_user'
		AND sk_berhenti IS NOT NULL
		AND sehat_jasmani IS NOT NULL
		AND sehat_rohani IS NOT NULL
		AND bebas_narkotika IS NOT NULL")->row_array();
	}
}

?>
