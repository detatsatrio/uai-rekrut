<style>
	.done{
		background-color: white !important;
		color: #24282E !important;
		border-color: #24282E !important;
	}
</style>
<header>
	<!-- page title -->
	<div class="page-title">
		<div class="grid-row">
			<!-- bread crumb -->
			<nav class="bread-crumb">
				<?php $status = (empty($status_user['status'])) ? '1' : $status_user['status'];?>
				<div class="cws-button bt-color-6 <?php echo ($status >= 1 ) ? 'done' : 'alt' ;?> smaller" style="border-color:white;color:white;pointer-events: none;">
					Register
				</div>
				<i class="fa fa-long-arrow-right"></i>
				<div class="cws-button bt-color-6 <?php echo ($status == '1') ? 'alt' : 'done' ;?> smaller" style="border-color:white;color:white;pointer-events: none;">Lamar Lowongan</div>
				<i class="fa fa-long-arrow-right"></i>
				<div class="cws-button bt-color-6 <?php echo ($status_cv == true) ? 'done' : 'alt' ;?> smaller" style="border-color:white;color:white;pointer-events: none;">Isi CV</div>
				<i class="fa fa-long-arrow-right"></i>
				<div class="cws-button bt-color-6 <?php echo ($status > 3 ) ? 'done' : 'alt' ;?> smaller" style="border-color:white;color:white;pointer-events: none;">Interview I</div>
				<i class="fa fa-long-arrow-right"></i>
				<div class="cws-button bt-color-6 <?php echo ($status > 4 ) ? 'done' : 'alt' ;?> smaller" style="border-color:white;color:white;pointer-events: none;">Psikotest</div>
				<i class="fa fa-long-arrow-right"></i>
				<div class="cws-button bt-color-6 <?php echo ($status > 5 ) ? 'done' : 'alt' ;?> smaller" style="border-color:white;color:white;pointer-events: none;">TOEFL</div>
				<i class="fa fa-long-arrow-right"></i>
				<span class="cws-button bt-color-6 <?php echo ($status > 6 ) ? 'done' : 'alt' ;?> smaller" style="border-color:white;color:white;pointer-events: none;">Interview II</span>
			</nav>
			<!-- / bread crumb -->
		</div>
	</div>
	<!-- / page title -->
</header>
	<div class="page-content">
		<div class="container clear-fix">
			<div class="grid-col-row" style="width:100%;">
				<div class="grid-col grid-col-12" style="width:100%;">
					<!-- main content -->
					<main>
						<section>
							<h2>Selamat Datang di Website Rekrutmen Universitas Al Azhar Indonesia</h2>
							<h4 style="text-transform:capitalize;"><?php echo $this->session->userdata('username').' | CALDOS - '.str_pad($this->session->userdata('id'), 4, '0', STR_PAD_LEFT)?> </h4>
							<p>Hallo, Calon Dosen, Anda berada di menu dasbor website rekrutmen UAI. Informasi di bawah ini otomatis ter update jika Anda dinyatakan lolos dalam tahap seleksi kami. Jika ingin menanyakan lebih lanjut silahkan hubungi ke kontak kami.</p>

							<?php if ($applicant == null) {
								$sum = '0';
							} else { ?>
								<?php foreach ($applicant as $app) {
									if ($app['id_user'] == $this->session->userdata('id')) {
										$lowongan[] = $app;
										$sum = count($lowongan);

									} else {
										$sum = '0';
									}
								} ?>
							<?php } ?>

							<?php if ($sum == '0') { ?>
								<blockquote>
									<center>
										<h3>Anda belum mendaftar ke lowongan mana pun<br /> silahkan mendaftar ke lowongan tersedia di halaman <a href="<?php echo base_url('index/lowongan')?>">lowongan</a></h3>
									</center>
								</blockquote>
							<?php } else { ?>
								<div class="tabs">
									<div class="block-tabs-btn clear-fix">

										<?php foreach ($applicant as $low) { ?>
											<?php foreach ($vacancy as $vac) { ?>
												<?php if ($low['vacancy'] == $vac['id_vacancy']) { ?>
													<div class="tabs-btn" data-tabs-id="<?php echo $vac['id_vacancy']?>"><?php echo $vac['title_vacancy']?></div>
												<?php } ?>
											<?php } ?>
										<?php } ?>

										<!-- <div class="tabs-btn active" data-tabs-id="tabs1">Tab 1</div>
										<div class="tabs-btn" data-tabs-id="tabs2">Tab 2</div> -->
									</div>
									<!-- tabs keeper -->
									<div class="tabs-keeper">
										<!-- tabs container -->
										<!-- <div class="container-tabs active" data-tabs-id="cont-tabs1">
										<center><h2> Papan Pengumuman </h2>
										<p>Pengumuman terbaru dapat dilihat di Papan masing-masing lowongan</p></center></div> -->
										<!--/tabs container -->
										<!-- tabs container -->
										<?php foreach ($applicant as $low) { ?>
											<?php foreach ($vacancy as $vac) { ?>
												<?php if ($low['vacancy'] == $vac['id_vacancy']) { ?>
													<div class="container-tabs active" data-tabs-id="<?php echo 'cont-'.$vac['id_vacancy']?>">
														<center>
														<?php if ($low['status'] == '1') { ?>
															<h4>Terima Kasih telah bergabung, Silahkan lengkapi Curriculum Vitae Anda</h4>
														<?php } elseif($low['status'] == '2') { ?>
															<h4>Curriculum Vitae Anda Sedang di Validasi</h4>
															<h6>Silahkan Lengkapi CV Anda</h6>
														<?php } elseif ($low['status'] == '3') { ?>
															<h6>Selamat Anda Berhasil Ke Tahap Selanjutnya<h6>
															<h6>Jadwal Interview I :</h6>
															<h2><?php echo $vac['inter_schedule'];?></h2>
															<h6>Silahkan datang ke :</h6>
															<h6><?php echo $vac['inter_desc'];?></h6>
														<?php } elseif ($low['status'] == '4') { ?>
															<h6>Selamat Anda Berhasil Ke Tahap Selanjutnya<h6>
															<h6>Jadwal Psikotest :</h6>
															<h2><?php echo $vac['psiko_schedule'];?></h2>
															<h6>Silahkan datang ke :</h6>
															<h6><?php echo $vac['psiko_desc'];?></h6>
														<?php } elseif ($low['status'] == '5') { ?>
															<h6>Selamat Anda Berhasil Ke Tahap Selanjutnya<h6>
															<h6>Jadwal Tes TOEFL :</h6>
															<h2><?php echo $vac['toefl_schedule'];?></h2>
															<h6>Silahkan datang ke :</h6>
															<h6><?php echo $vac['toefl_desc'];?></h6>
														<?php } elseif ($low['status'] == '6') { ?>
															<h6>Selamat Anda Berhasil Ke Tahap Selanjutnya<h6>
															<h6>Jadwal Interview II :</h6>
															<h2><?php echo $vac['toefl_schedule'];?></h2>
															<h6>Silahkan datang ke :</h6>
															<h6><?php echo $vac['inter2_desc'];?></h6>
														<?php } elseif ($low['status'] == '7') { ?>
															<h4>Selamat Anda dalam tahap akhir seleksi</h4>
															<p>Silahkan upload berkas persyaratan terakhir</p>
														<?php } elseif ($low['status'] == '8') { ?>
															<h4>Berkas anda sedang divalidasi</h4>
														<?php } elseif ($low['status'] == '9') { ?>
															<h4>Selamat Anda berhasil menjadi Dosen di UAI</h4>
															<h1>Proses Create ID Dosen sedang berlangsung</h1>
														<?php } elseif ($low['status'] == '10') { ?>
															<h4>Mohon maaf Anda belum sesuai dengan kriteria yang kami tentukan, Terima kasih atas partisipasinya</h4>
														<?php } elseif ($low['status'] == '11') { ?>
															<h6>ID Dosen Anda : </h6>
															<h1><b><?php echo $low['id_dosen']?></b></h1>
														<?php } elseif ($low['status'] == '12') { ?>
															<h4>Selamat Anda Telah Menjadi Dosen Universitas Al-Azhar Indonesia</h4>
														<?php } else { ?>
															<h4>Mohon maaf Anda belum sesuai dengan kriteria yang kami tentukan, Terima kasih atas partisipasinya</h4>
														<?php } ?>
														</center>
													</div>
												<?php } ?>
											<?php } ?>
										<?php } ?>
									</div>
									<!--/tabs keeper -->
								</div>
								<!-- /tabs -->
							<?php } ?>
							<br />
							<p>Semua keputusan panitia rekrutmen merupakan keputusan yang bulat dan tidak dapat di ganggu gugat. Segala bentuk protes atau kelalaian pelamar dianggap bukan tanggung jawab panitia.</p>

						</section>
						<hr class="divider-color" />
					</main>
					<!-- / main content -->
				</div>
			</div>
		</div>
	</div>
	<!-- / content -->
