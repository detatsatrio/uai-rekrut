<!-- content -->
	<div class="page-content">


		<div class="container clear-fix">
			<div class="grid-col-row" style="width:100%;">
				<div class="grid-col grid-col-12" style="width:100%;">
					<!-- main content -->
					<main>
						<!-- blog item -->
						<div class="blog-post">
							<article>
								<div class="post-info">
									<div class="date-post" style="display:none;"><div class="day"></div><div class="month"></div></div>
									<div class="post-info-main" style="padding:15px 0 15px;">
										<div class="">My Account</div>
									</div>
									<div class="comments-post" style="display:none;"><i class="fa fa-comment"></i> </div>
								</div>
								<br />
								<!-- info box -->
								<div class="info-boxes confirmation-message" style="display:none;">
									<div class="info-box-icon"><i class="fa fa-check"></i></div>
									<strong>Sukses update</strong><br />Mohon tunggu...
									<div class="close-button"></div>
								</div>
								<!--/info box -->
								<div class="info-boxes error-message" style="display:none;">
									<div class="info-box-icon"><i class="fa fa-times"></i></div>
									<strong>Update password gagal</strong><br />Pastikan konfirmasi password sdh sesuai
									<div class="close-button"></div>
								</div>
								<!-- info box -->
								<div class="info-boxes info-message" style="display:none;">
									<div class="info-box-icon"><i class="fa fa-info"></i></div>
									<strong>System Processing</strong><br />Pengecekan sistem harap menunggu...
									<div class="close-button"></div>
								</div>
								<!--/info box -->
								<p>Password : <input name="password" type="password" size="30" aria-required="true" placeholder="Password" class="update-akun-form"></p>
								<input type="hidden" value="<?php echo $this->session->userdata('id')?>" name="user_id" class="update-akun-form">
								<p>Konfirmasi Password : <input name="confirm_password" type="password" size="30" aria-required="true" class="update-akun-form" placeholder="Konfirmasi Password"></p>
								<button type="button" id="update-akun-btn" class="cws-button border-radius alt icon-right">Update <i class="fa fa-angle-right"></i></button>
							</article>
						</div>
						<!-- / blog item -->
						<hr class="divider-color" />
					</main>
					<!-- / main content -->
					<!-- pagination -->
					
					<!-- / pagination -->
				</div>
			</div>
		</div>
	</div>
	<!-- / content -->