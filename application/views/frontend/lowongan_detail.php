<!-- content -->
	<div class="page-content">
	<hr class="divider-color" />
	<br />
		<div class="container clear-fix">
			<div class="grid-col-row" style="width:100%;">
				<div class="grid-col grid-col-12" style="width:100%;">
					<!-- main content -->
					<main>

							<!-- blog item -->
							<div class="community color-2" style="text-align:left;padding:30px;">
						<div class="blog-post">
							<article>
								<!-- banner alt -->
								<div class="category-button" style="background-color:#13a069;color:#fff;border-color:#13a069;">
									<i class="fa fa-briefcase"></i>
									<p>Lowongan<br><?php echo $vacancy['title_vacancy']?></p>
								</div>
								<h3><b>Persyaratan :</b></h3>
								<p><?php echo $vacancy['desc_vacancy']?></p>
								<div class="tags-post">
									<?php
									$jumlah = array();
									foreach ($applicant as $app) {
										if ($app['vacancy'] == $vacancy['id_vacancy']) {
											$jumlah[] = $app['id_applicant'];
											// $jumlah = $app['id_applicant'];
										}
									}

									//count($jumlah);
									?>
									<span  style="color:#454545;"><i class="fa fa-users"></i>&nbsp;<?php echo count($jumlah). ' Pelamar'?></span> |
									<span  style="color:#454545;">Pendaftaran Terakhir : &nbsp;<?php echo $vacancy['deadline']?></span>

								</div>
								<form action="<?php echo base_url('index/lamar')?>" method="post">
									<input type="hidden" value="<?php echo $vacancy['id_vacancy']?>" name="vacancy_id" class="vacancy-lowongan" data-id="<?php echo $vacancy['id_vacancy']?>">
									<input type="hidden" value="<?php echo $this->session->userdata('id')?>" name="user_id" class="lamar-form">
									<button type="submit" class="cws-button bt-color-6 alt icon-right" >Lamar Sekarang <i class="fa fa-angle-right"></i></button>
								</form>
							</article>
						</div>
						</div>
						<!-- / blog item -->
						<hr class="divider-color" />
						<br />
						
					</main>
					<!-- / main content -->
					<!-- pagination -->

					<!-- / pagination -->
				</div>
			</div>
		</div>
	</div>
	<!-- / content -->
