<style>
	.avatar{
		width: 70px !important;
		height: 70px !important;
	}
</style>
<!-- content -->
	<div class="page-content">


		<div class="container clear-fix">
			<div class="grid-col-row" style="width:100%;">
				<div class="grid-col grid-col-12" style="width:100%;">
					<form action="{action}" method="post"  role="form" enctype="multipart/form-data">
					<!-- main content -->
					<main>
					<section>
					<h2>Curriculum Vitae</h2>
					<div class="clear-fix">
						<div class="grid-col-row">
							<div class="grid-col grid-col-4">
								<!-- banner alt -->
								<div class="category-button" id="data-pribadi-btn">
									<i class="fa fa-book"></i>
									<?php echo ($data_personal != null) ? '<div class="info-box-icon" style="float:right;"><i class="fa fa-check"></i></div>' : '';?>
									<p>Data<br>Pribadi
								</div>

								<!-- <div class="info-boxes confirmation-message">
									<div class="info-box-icon"><i class="fa fa-check"></i></div>
									<strong>Confirmation box</strong><br />Vestibulum sodales pellentesque nibh quis imperdiet
									<div class="close-button"></div>
								</div> -->
								<!-- / banner alt -->
							</div>
							<div class="grid-col grid-col-4">
								<!-- banner alt -->
								<div class="category-button" id="data-pendidikan-btn">
									<i class="fa fa-briefcase"></i>
									<?php echo ($data_edukasi != null) ? '<div class="info-box-icon" style="float:right;"><i class="fa fa-check"></i></div>' : '';?>
									<p>Data<br>Pendidikan</p>
								</div>
								<!-- / banner alt -->
							</div>
							<div class="grid-col grid-col-4">
								<!-- banner alt -->
								<div class="category-button" id="data-penelitian-btn">
									<i class="fa fa-institution"></i>
									<?php echo ($data_penelitian != null) ? '<div class="info-box-icon" style="float:right;"><i class="fa fa-check"></i></div>' : '';?>
									<p>Data<br>Penelitian</p>
								</div>
								<!-- / banner alt -->
							</div>
							<div class="grid-col grid-col-4" style="margin-top:20px;">
								<!-- banner alt -->
								<div class="category-button" id="pengalaman-mengajar-btn">
									<i class="fa fa-graduation-cap"></i>
									<?php echo ($data_mengajar != null) ? '<div class="info-box-icon" style="float:right;"><i class="fa fa-check"></i></div>' : '';?>
									<p>Pengalaman<br>Mengajar</p>
								</div>
								<!-- / banner alt -->
							</div>
							<div class="grid-col grid-col-4" style="margin-top:20px;">
								<!-- banner alt -->
								<div class="category-button" id="upload-berkas-btn">
									<i class="fa fa-cloud-upload"></i>
									<?php echo ($data_docs != null) ? '<div class="info-box-icon" style="float:right;"><i class="fa fa-check"></i></div>' : '';?>
									<p>Upload<br>Dokumen</p>
								</div>
								<!-- / banner alt -->
							</div>
						</div>
					</div>

				</section>
					<!-- blog item -->
					<div class="blog-post" id="data-pribadi">
						<article>
							<div class="post-info">
								<div class="date-post" style="display:none;"><div class="day">26</div><div class="month">Feb</div></div>
								<div class="post-info-main" style="padding:15px 0 15px;background-color:#d36c59;">
									<div>Data Pribadi</div>
								</div>
								<div class="comments-post" style="display:none"><i class="fa fa-comment"></i> 3</div>
							</div>
							<br />
							<div class="community color-1" style="text-align:left;padding:10px;">
								<p>Nama Lengkap : <input id="fullname" name="fullname" type="text" size="30" aria-required="true" placeholder="Nama Lengkap" value="{fullname}" required></p>
								<p>Tempat Lahir : <input id="birth_place" name="birth_place" type="text" value="{birthplace}" size="30" aria-required="true" placeholder="Tempat Lahir" required></p>
								<p>Tanggal Lahir :
								<input class="ambil-tanggal" name="birth_date" type="text" value="{birthdate}" placeholder="Tanggal Lahir" required>
								<small><i>* format : 22-08-1992</i></small>
								</p>
								Jenis Kelamin :
								<p class="form-row form-row-wide select-arrow">
								<select name="gender">
									<?php foreach ($gender as $gender) { ?>
										<option value="<?php echo $gender['id_gender']?>"><?php echo $gender['gender']?></option>
									<?php } ?>
								</select></p>
								Status :
								<p class="form-row form-row-wide select-arrow">
								<select name="marital">
									<?php foreach ($marital as $marital) { ?>
										<option value="<?php echo $marital['id_marital']?>"><?php echo $marital['marital']?></option>
									<?php } ?>
								</select></p>
								<p>Ibu Kandung : <input id="mother" name="mother" type="text" value="{mother}" size="30" aria-required="true" placeholder="Ibu Kandung" required></p>
								<p>Nomor KTP : <input class="form-control" id="id_card_no" name="id_card_no" type="text" value="{id_card}" size="30" aria-required="true" placeholder="Nomor KTP" required></p>
								<p>Telp Rumah : <input id="homephone_no" name="homephone_user" type="text" value="{homephone}" size="30" aria-required="true" placeholder="Telp Rumah" ></p>
								<p>Handphone : <input id="handphone" name="handphone_user" type="text" value="{handphone}" size="30" aria-required="true" placeholder="Handphone" required></p>
								<p >Email : <input id="email" name="email_user" type="text" value="{email}" size="30" aria-required="true" placeholder="Email" required></p>
								<p class="form-row form-row-wide message-form-message" style="margin-left:-1px;">Alamat Lengkap : <textarea id="address" name="address" cols="147" rows="5" aria-required="true" placeholder="Isikan Alamat Sesuai KTP" required>{address}</textarea></p>
								</div>

							</article>
					</div>
					<!-- / blog item -->

					<div class="blog-post" style="display:none;" id="data-pendidikan">
						<article>
							<div class="post-info">
								<div class="date-post" style="display:none;"><div class="day">26</div><div class="month">Feb</div></div>
								<div class="post-info-main" style="padding:15px 0 15px;background-color:#d36c59;" >
									<div>Data Pendidikan</div>
								</div>
								<div class="comments-post" style="display:none"><i class="fa fa-comment"></i> 3</div>
							</div>
							<br />
							<?php if ($education == null) { ?>
								<div class="community color-1" style="text-align:left;padding:10px;" id="data-pendidikan-kosong">
									<center>
										<div class="community-logo">
											<i class="fa fa-file-o"></i>
										</div>
										<h2>Data Pendidikan Kosong</h2>
									</center>
								</div>
							<?php } ?>
							<div id="data-pendidikan-form">
							<?php $number = 1;?>
							<?php foreach($education as $edu) { ?>
									<div class="community color-1" style="text-align:left;padding:10px;">
										<h3>Data Pendidikan - <?php echo $number?></h3>
										<div width="50%">
										Jenjang Pendidikan :
										<input type="hidden" value="<?php echo $edu['id_education']?>" name="id-education-<?php echo $number?>">
										<p class="form-row form-row-wide select-arrow">
											<select name="jenjang-<?php echo $number?>" required>
												<?php foreach ($jenjang as $jen) {
													if ($edu['jenjang'] == $jen['id_jenjang']) { ?>
														<option value="<?php echo $jen['id_jenjang']?>" selected><?php echo $jen['jenjang']?></option>
													<?php } else { ?>
														<option value="<?php echo $jen['id_jenjang']?>"><?php echo $jen['jenjang']?></option>
													<?php }
												}?>
											</select>
										<p class="form-row form-row-wide">
											Masa studi :
											<table>
												<tr>
													<td style="padding-right:10px;">
														<input name="in-year-<?php echo $number?>" type="text" value="<?php echo $edu['in_year']?>" required><small><i>ex : 31 - 08 - 2011</i></small></td>
													<td> - </td>
													<td style="padding-left:10px;">
														<input name="end-year-<?php echo $number?>" type="text" value="<?php echo $edu['end_year']?>" required><small><i>ex : 31 - 08 - 2015</i></small></td>
												</tr>
											</table>
										</p>
										<p class="form-row form-row-wide">
											Nomor Induk Mahasiswa :
											<input type="text" name="nim-<?php echo $number?>" value="<?php echo $edu['nim']?>" required>
										</p>
										<p class="form-row form-row-wide">
											Jurusan / Program Studi :
											<input type="text" name="prodi-<?php echo $number?>" value="<?php echo $edu['prodi']?>" required>
										</p>
										<p class="form-row form-row-wide">
											Bidang Minat :
											<input type="text" name="minat-<?php echo $number?>" value="<?php echo $edu['minat']?>" required>
										</p>
										<p class="form-row form-row-wide">
											Judul Karya Ilmiah :
											<input type="text" name="karya-<?php echo $number?>" value="<?php echo $edu['karya']?>" required>
										</p>
										</div>
									</div>

									<br />
									<?php $number = $number+1;?>
								<?php } ?>
							</div>
							<br />
							<button type="button" class="cws-button alt icon-right bt-color-3" id="data-pendidikan-add" >Tambah <i class="fa fa-plus fa-lg"></i></button>
						</article>
					</div>

					<div class="blog-post" style="display:none;" id="data-penelitian">
						<article>
							<div class="post-info">
								<div class="date-post" style="display:none;"><div class="day">26</div><div class="month">Feb</div></div>
								<div class="post-info-main" style="padding:15px 0 15px;background-color:#d36c59;">
									<div>Data Penelitian</div>
								</div>
								<div class="comments-post" style="display:none"><i class="fa fa-comment"></i> 3</div>
							</div>
							<br />
							<?php if ($research == null) { ?>
								<div class="community color-1" style="text-align:left;padding:10px;" id="data-penelitian-kosong">

									<div class="community-logo">
										<i class="fa fa-file-o"></i>
									</div>
									<center>
										<h2>Data Penelitian Kosong</h2>
									</center>
								</div>
							<?php } ?>
								<div id="data-penelitian-form">
								<?php $number = 1;?>
								<?php foreach ($research as $re) { ?>
									<div class="community color-1" style="text-align:left;padding:10px;">
										<h3>Data Penelitian - <?php echo $number?></h3>
										<input type="hidden" value="<?php echo $re['id_research']?>" name="id-research-<?php echo $number?>" required></input>
										<p>
										Judul Penelitian :
											<input name="judul-penelitian-<?php echo $number?>" type="text" size="30" aria-required="true" placeholder="Judul Penelitian" value=" <?php echo $re['title']?> " required>
										</p>

										<p>
										Kategori Penelitian :
											<input name="kategori-penelitian-<?php echo $number?>" type="text" value="<?php echo $re['category']?>" size="30" aria-required="true" placeholder="<?php echo $re['category']?>" required>
										</p>

										<p>
										Nama :
											<input name="nama-publikasi-<?php echo $number?>" type="text" value="<?php echo $re['name']?>" size="30" aria-required="true" placeholder="Publikasi" required>
										</p>

										<p>
										Publikasi :
											<input name="publikasi-<?php echo $number?>" type="text" value="<?php echo $re['publikasi']?>" size="30" aria-required="true" placeholder="Publikasi" required>
										</p>

										<p>
										Tahun :
											<input name="tahun-<?php echo $number?>" type="text" value="<?php echo $re['tahun']?>" size="30" aria-required="true" placeholder="Publikasi" required>
										</p>
									</div>
									<br/>
								<?php $number = $number+1;?>
								<?php } ?>
								</div>
								<br />
								<button type="button" class="cws-button alt icon-right bt-color-3" id="data-penelitian-add">Tambah <i class="fa fa-plus fa-lg"></i></button>
						</article>
					</div>

					<div class="blog-post" style="display:none;" id="pengalaman-mengajar">
						<article>
							<div class="post-info">
								<div class="date-post" style="display:none;"><div class="day">26</div><div class="month">Feb</div></div>
								<div class="post-info-main" style="padding:15px 0 15px;background-color:#d36c59;">
									<div>Pengalaman Mengajar</div>
								</div>
								<div class="comments-post" style="display:none"><i class="fa fa-comment"></i> 3</div>
							</div>
							<br />
							<?php if ($teach == null) { ?>
								<div class="community color-1" style="text-align:left;padding:10px;" id="data-mengajar-kosong">
								<div class="community-logo">
									<i class="fa fa-file-o"></i>
								</div>
								<center>
									<h2>Pengalaman Mengajar Kosong</h2>
								</center>
							</div>
							<?php } ?>
							<br />
							<div id="data-mengajar-form">
							<?php $number = 1;?>
							<?php foreach ($teach as $te) { ?>
								<div class="community color-1" style="text-align:left;padding:10px;">
									<h3>Pengalaman Mengajar Ke - <?php echo $number;?></h3>
									<input type="hidden" value="<?php echo $te['id_teach']?>" name="id-teach-<?php echo $number?>">
									<p>
									Mata Kuliah :
										<input name="mata-kuliah-<?php echo $number?>" type="text" size="30" aria-required="true" value="<?php echo $te['lecture']?>" required>
									</p>

									<p>
									Semester :
										<input name="semester-<?php echo $number?>" type="text" value="<?php echo $te['semester']?>" required>
									</p>

									<p>
									Tahun Akademik :
										<input name="tahun-<?php echo $number?>" type="text" value="<?php echo $te['tahun']?>" required>
									</p>

									Jenjang :
									<p class="form-row form-row-wide select-arrow">
										<select name="jenjang-<?php echo $number?>" required>
											<?php foreach ($jenjang as $jen) {
												if ($te['jenjang'] == $jen['id_jenjang']) { ?>
													<option value="<?php echo $jen['id_jenjang']?>" selected><?php echo $jen['jenjang']?></option>
												<?php } else { ?>
													<option value="<?php echo $jen['id_jenjang']?>"><?php echo $jen['jenjang']?></option>
												<?php }
											}?>
										</select>
									</p>

									<p>
									Jurusan / Prodi :
										<input name="jurusan-<?php echo $number?>" type="text" value="<?php echo $te['prodi']?>" required>
									</p>

									<p>
									Fakultas :
										<input name="fakultas-<?php echo $number?>" type="text" value="<?php echo $te['fakultas']?>" required>
									</p>

									<p>
									Universitas / Perguruan Tinggi :
										<input name="perguruan-tinggi-<?php echo $number?>" type="text" value="<?php echo $te['perguruan_tinggi']?>" required>
									</p>
								</div>
								<?php $number++;?>
							<?php } ?>
							<br />
						</article>
						<button type="button" class="cws-button alt icon-right bt-color-3" id="data-mengajar-add">Tambah <i class="fa fa-plus fa-lg"></i></button>
					</div>

					<div class="blog-post" style="display:none;" id="upload-berkas" >
						<article>
							<div class="post-info">
								<div class="date-post" style="display:none;"><div class="day">26</div><div class="month">Feb</div></div>
								<div class="post-info-main" style="padding:15px 0 15px;background-color:#d36c59;">
									<div>Upload Dokumen</div>
								</div>
								<div class="comments-post" style="display:none"><i class="fa fa-comment"></i> 3</div>
							</div>
							<br />
							<div class="community color-1" style="text-align:left;padding:10px;">
							<i><h6>Note : Gunakan tipe file .jpg / .png</h6></i>

							<!-- Baris -->
							<div class="columns-row">
								<!-- Persyaratan I -->
								<div class="columns-col columns-col-6 margin-top">
									<h4>Persyaratan I</h4>

									<!-- Start Foto ID -->
									<div class="columns-col columns-col-6 margin-top">
										<h6>Foto Diri</h6>
										<div class="image-upload">
												<label for="file-input-id">
														<?php if ($docs) { ?>
															<?php foreach ($docs as $d) { ?>
														<?php if ($d['id_pict']) { ?>
														<img src="<?php echo base_url();?>uploads/id/<?php echo $d["id_pict"]?>" alt="" class="avatar" id="id-pict">
														<?php } else { ?>
														<img src="<?php echo base_url('assets/placeholder.png')?>" data-at2x="<?php echo base_url('assets/placeholder.png')?>" id="id-pict" alt="" class="avatar">
														<?php } ?>
													<?php } ?>
														<?php } else { ?>
															<img src="<?php echo base_url('assets/placeholder.png')?>" data-at2x="<?php echo base_url('assets/placeholder.png')?>" id="id-pict" alt="" class="avatar">
														<?php } ?>
												</label>

												<input id="file-input-id" type="file" name="id-pict" />
										</div>
									</div>
									<!-- End Foto ID -->

									<!-- Start KTP -->
									<div class="columns-col columns-col-6 margin-top">
										<h6>KTP</h6>
										<div class="image-upload">
												<label for="file-input-ktp">
													<?php 	if (($docs)) { ?>
														<?php foreach ($docs as $d) { ?>
														<?php if (isset($d['id_card_pict'])) { ?>
														<img src="<?php echo base_url();?>uploads/id_card/<?php echo $d["id_card_pict"]?>" alt="" class="avatar" id="id-card">
														<?php } else { ?>
														<img src="<?php echo base_url('assets/placeholder.png')?>" data-at2x="<?php echo base_url('assets/placeholder.png')?>" id="id-card" alt="" class="avatar">
														<?php } ?>
													<?php } ?>
												<?php } else { ?>
													<img src="<?php echo base_url('assets/placeholder.png')?>" data-at2x="<?php echo base_url('assets/placeholder.png')?>" id="id-card" alt="" class="avatar">

												<?php } ?>

												</label>

												<input id="file-input-ktp" type="file" name="id-card" />
										</div>
									</div>
									<!-- End KTP -->

									<!-- Ijazah s1 -->
									<div class="columns-col columns-col-6 margin-top">
										<h6>Ijazah S1</h6>
										<div class="image-upload">
										    <label for="file-input-ijazah-s1">
										      <?php if (($docs)) { ?>
										        <?php foreach ($docs as $d) { ?>
										        <?php if ($d['cert_bachelor']) { ?>
										        <img src="<?php echo base_url();?>uploads/bachelor/<?php echo $d["cert_bachelor"]?>" alt="" class="avatar" id="ijazah-s1">
										        <?php } else { ?>
										        <img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="ijazah-s1" alt="" class="avatar">
										        <?php } ?>
										      <?php } ?>
										    <?php } else { ?>
										      <img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="ijazah-s1" alt="" class="avatar">

										    <?php } ?>
										    </label>

										    <input id="file-input-ijazah-s1" type="file" name="ijazah-s1" />
											</div>
									</div>
									<!-- End Ijazah s1 -->

									<!-- Ijazah s2 -->
									<div class="columns-col columns-col-6 margin-top">
										<h6>Ijazah S2</h6>
										<div class="image-upload">
										    <label for="file-input-ijazah-s2">
										      <?php if (($docs)) { ?>
										        <?php foreach ($docs as $d) { ?>
										        <?php if ($d['cert_master']) { ?>
										        <img src="<?php echo base_url();?>uploads/master/<?php echo $d["cert_master"]?>" alt="" class="avatar" id="ijazah-s2">
										        <?php } else { ?>
										        <img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="ijazah-s2" alt="" class="avatar">
										        <?php } ?>
										      <?php } ?>
										    <?php } else { ?>
										      <img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="ijazah-s2" alt="" class="avatar">

										    <?php } ?>

										    </label>

										    <input id="file-input-ijazah-s2" type="file" name="ijazah-s2" />
									</div>
								</div>
								<!-- End Ijazah s2 -->

								<!-- Ijazah s3 -->
								<div class="columns-col columns-col-6 margin-top">
									<h6>Ijazah S3</h6>
									<div class="image-upload">
									    <label for="file-input-ijazah-s3">
									      <?php if (($docs)) { ?>
									         <?php foreach ($docs as $d) { ?>
									        <?php if ($d['cert_doctor']) { ?>
									        <img src="<?php echo base_url();?>uploads/doctor/<?php echo $d["cert_doctor"]?>" alt="" class="avatar" id="ijazah-s3">
									        <?php } else { ?>
									        <img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="ijazah-s3" alt="" class="avatar">
									        <?php } ?>
									      <?php } ?>
									    <?php } else { ?>
									      <img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="ijazah-s3" alt="" class="avatar">

									    <?php } ?>

									    </label>

									    <input id="file-input-ijazah-s3" type="file" name="ijazah-s3" />
									</div>
							</div>
							<!-- End Ijazah s3 -->

						<!-- Start Penyetaraan Dikti -->
						<div class="columns-col columns-col-6 margin-top">
							<h6>Penyetaraan Dikti</h6>
							<div class="image-upload">
									<label for="file-input-cert-inter">
										<?php if (($docs)) { ?>
											<?php foreach ($docs as $d) { ?>
											<?php if ($d['cert_inter']) { ?>
											<img src="<?php echo base_url();?>uploads/inter/<?php echo $d["cert_inter"]?>" alt="" class="avatar" id="cert-inter">
											<?php } else { ?>
											<img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="cert-inter" alt="" class="avatar">
											<?php } ?>
										<?php } ?>
									<?php } else { ?>
										<img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="cert-inter" alt="" class="avatar">
									<?php } ?>

									</label>

									<input id="file-input-cert-inter" type="file" name="cert-inter" />
							</div>
					</div>
						<!-- End Penyetaraan Dikti -->

						<!-- Start SK Mendiknas -->
						<div class="columns-col columns-col-6 margin-top">
							<h6>SK Mendiknas</h6>
							<div class="image-upload">
									<label for="file-input-sk-mendiknas">
										<?php if (($docs)) { ?>
											<?php foreach ($docs as $d) { ?>
											<?php if ($d['sk_mendiknas']) { ?>
											<img src="<?php echo base_url();?>uploads/mendiknas/<?php echo $d["sk_mendiknas"]?>" alt="" class="avatar" id="sk-mendiknas">
											<?php } else { ?>
											<img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="sk-mendiknas" alt="" class="avatar">
											<?php } ?>
										<?php } ?>
									<?php } else { ?>
										<img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="sk-mendiknas" alt="" class="avatar">

									<?php } ?>

									</label>

									<input id="file-input-sk-mendiknas" type="file" name="sk-mendiknas" />
							</div>
						</div>
						<!-- End SK Mendiknas -->

						<!-- Start Paper Lolos Butuh -->
						<div class="columns-col columns-col-6 margin-top">
							<h6>Paper Lolos Butuh</h6>
							<div class="image-upload">
									<label for="file-input-lolos-butuh">
										<?php if (($docs)) { ?>
											<?php foreach ($docs as $d) { ?>
											<?php if ($d['paper_lolos_butuh']) { ?>
											<img src="<?php echo base_url();?>uploads/lolos_butuh/<?php echo $d["paper_lolos_butuh"]?>" alt="" class="avatar" id="lolos-butuh">
											<?php } else { ?>
											<img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="lolos-butuh" alt="" class="avatar">
											<?php } ?>
										<?php } ?>
									<?php } else { ?>
										<img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="lolos-butuh" alt="" class="avatar">
									<?php } ?>

									</label>

									<input id="file-input-lolos-butuh" type="file" name="lolos-butuh" />
							</div>
						</div>
						<!-- End Paper Lolos Butuh -->

				</div>
				<!-- End Kolom Persyaratan 1  -->

								<!-- Persyaratan 2 -->
								<div class="columns-col columns-col-6 margin-top">
									<h4>Persyaratan II</h4>

									<!-- SK Berhenti -->
									<div class="columns-col columns-col-6 margin-top">
									  <h6 >SK Berhenti</h6>
									  <div class="image-upload">
									      <label for="file-input-sk-berhenti">
									        <?php if (($docs)) { ?>
									          <?php foreach ($docs as $d) { ?>
									          <?php if ($d['sk_berhenti']) { ?>
									          <img src="<?php echo base_url();?>uploads/sk_berhenti/<?php echo $d["sk_berhenti"]?>" alt="" class="avatar" id="sk-berhenti">
									          <?php } else { ?>
									          <img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="sk-berhenti" alt="" class="avatar">
									          <?php } ?>
									        <?php } ?>
									      <?php } else { ?>
									        <img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="sk-berhenti" alt="" class="avatar">

									      <?php } ?>

									      </label>

									      <input id="file-input-sk-berhenti" type="file" name="sk-berhenti" />
									  </div>
									</div>
									<!-- End SK Berhenti -->

									<!-- Start Sehat Jasmani -->
									<div class="columns-col columns-col-6 margin-top">
										<h6>Sehat Jasmani</h6>
										<div class="image-upload">
										    <label for="file-input-sehat-jasmani">
										      <?php if (($docs)) { ?>
										        <?php foreach ($docs as $d) { ?>
										        <?php if ($d['sehat_jasmani']) { ?>
										        <img src="<?php echo base_url();?>uploads/sehat_jasmani/<?php echo $d["sehat_jasmani"]?>" alt="" class="avatar" id="sehat-jasmani">
										        <?php } else { ?>
										        <img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="sehat-jasmani" alt="" class="avatar">
										        <?php } ?>
										      <?php } ?>
										    <?php } else { ?>
										      <img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="sehat-jasmani" alt="" class="avatar">

										    <?php } ?>

										    </label>

										    <input id="file-input-sehat-jasmani" type="file" name="sehat-jasmani" />
										</div>
										<!-- End Sehat Jasmani -->
									</div>

									<!-- Sehat Rohani -->
									<div class="columns-col columns-col-6 margin-top">
										<h6>Sehat Rohani</h6>
										<div class="image-upload">
										    <label for="file-input-sehat-rohani">
										    	<?php if (($docs)) { ?>
										    		<?php foreach ($docs as $d) { ?>
														<?php if ($d['sehat_rohani']) { ?>
														<img src="<?php echo base_url();?>uploads/sehat_rohani/<?php echo $d["sehat_rohani"]?>" alt="" class="avatar" id="sehat-rohani">
														<?php } else { ?>
														<img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="sehat-rohani" alt="" class="avatar">
														<?php } ?>
													<?php } ?>
												<?php } else { ?>
													<img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="sehat-rohani" alt="" class="avatar">

												<?php } ?>
										    </label>

										    <input id="file-input-sehat-rohani" type="file" name="sehat-rohani" />
											</div>
									</div>
									<!-- End Sehat Rohani -->

									<!-- Bebas Narkotika -->
									<div class="columns-col columns-col-6 margin-top">
										<h6>Bebas Narkotika</h6>
										<div class="image-upload">
										    <label for="file-input-bebas-narkotika">
										    	<?php if (($docs)) { ?>
										    		<?php foreach ($docs as $d) { ?>
														<?php if ($d['bebas_narkotika']) { ?>
														<img src="<?php echo base_url();?>uploads/bebas_narkotika/<?php echo $d["bebas_narkotika"]?>" alt="" class="avatar" id="bebas-narkotika">
														<?php } else { ?>
														<img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="bebas-narkotika" alt="" class="avatar">
														<?php } ?>
													<?php } ?>
												<?php } else { ?>
													<img src="<?php echo base_url('assets/placeholder-file.png')?>" data-at2x="<?php echo base_url('assets/placeholder-file.png')?>" id="bebas-narkotika" alt="" class="avatar">

												<?php } ?>

										    </label>

										    <input id="file-input-bebas-narkotika" type="file" name="bebas-narkotika" />
									</div>
									<!-- End Bebas Narkotika -->
								</div>

								</div>
								<!-- End Kolom Persyaratan 2  -->

							</div>
							<!-- End Baris -->
							 </div>
						</article>
					</div>
					<button type="submit" class="cws-button bt-color-6" style="width:100%;">Simpan</button>
					</form>
					</main>

					<!-- / main content -->
				</div>
			</div>
		</div>
	</div>
	<!-- / content -->
