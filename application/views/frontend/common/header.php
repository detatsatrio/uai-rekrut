<!DOCTYPE HTML>
<html>
<head>
	<title>{title}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<!-- style -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/frontend/img/favicon.png">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/select2.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/frontend/css/jquery.fancybox.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/fi/flaticon.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/frontend/css/jquery.fancybox.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/frontend/rs-plugin/css/settings.css" media="screen">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/animate.css">
	<!--styles -->

	<!-- datetimepicker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/res/datetimepicker/css/bootstrap-datetimepicker.min.css">

    <style type="text/css">
    	input[type='file']{
    		color:transparent;
    	}
    	.image-upload > input
		{
		    display: none;
		}
    </style>
</head>