<body class="">
	<header>
		<div class="page-header-top">
			<div class="grid-row clear-fix">
				<address>
					<a href="tel:021-72792753" class="phone-number"><i class="fa fa-phone"></i>(021) 727 92753</a>
					<a href="mailto:info@uai.ac.id" class="email"><i class="fa fa-envelope-o"></i>info@uai.ac.id</a>
				</address>
				<div class="header-top-panel">
					<?php echo ($role != '2') ? '<a href="'.base_url('login').'" class="fa fa-user login-icon"></a>' : ''; ?>
					<!-- <a href="#" class="search-open"><i class="fa fa-search"></i></a> 
					<form action="#" class="clear-fix">
						<input type="text" placeholder="Cari info apa ?" class="clear-fix">
					</form> -->
				</div>
			</div>
		</div>
		<!-- sticky -->
		<div class="sticky-wrapper">
			<div class="sticky-menu">
				<div class="grid-row clear-fix">
					<a href="<?php echo base_url()?>" class="logo">
						<img src="<?php echo base_url()?>assets/logo_uai2.png"  data-at2x="<?php echo base_url()?>assets/logo_uai2.png" alt>
					</a>
					<nav class="main-nav">
						<ul class="clear-fix">
							<li>
								<a href="<?php echo base_url()?>index/lowongan">Lowongan</a>
							</li>
							<?php if (($role == '2')) { ?>
							<li>
								<a href="<?php echo base_url()?>index/dasbor">Dasbor</a>
							</li>
							<li>
								<a href="<?php echo base_url()?>index/form_aplikasi">CV</a>
							</li>
							<li>
								<a href="<?php echo base_url()?>index/akun">Akun</a>
							</li>
							<li>
								<a href="<?php echo base_url()?>login/logout">Logout</a>
							</li>
							<?php } else { ?>

							<?php } ?>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<!-- sticky -->
	</header>