
	<footer>
		<div class="grid-row">
			<div class="grid-col-row clear-fix">
				<section class="grid-col grid-col-4 footer-about">
					<h2 class="corner-radius">Tentang Kami</h2>
					<div>
						<h3>E-Rekrutmen Universitas Al Azhar</h3>
						<p>Membuka Lowongan Dosen Universitas Al Azhar. <br /> Silahkan mendaftar bagi kamu yang memenuhi kualifikasi</p>
					</div>
					<address>
						<p></p>
						<a href="tel:123-123456789" class="phone-number">(021) 727 92753</a>
						<br />
						<a href="mailto:uni@domain.com" class="email">info@uai.ac.id</a>
						<br />
						<a href="www.uai.ac.id" class="site">www.uai.ac.id</a>
						<br />
						<a href="www.uai.ac.id" class="address">Universitas Al Azhar Indonesia<br/>Komplek Masjid Agung Al Azhar, Jakarta 12110</a>
					</address>
				</section>
				<section class="grid-col grid-col-4 footer-latest">
					<h2 class="corner-radius">Lowongan Terbaru</h2>
					<?php foreach ($latest as $latest) { ?>
						<?php if ($latest['deadline'] >= date('Y-m-d H:i')) { ?>
							<article>
							<h3><?php echo $latest['title_vacancy']?></h3>
							<div class="course-date">
								<div>Pendaftaran terakhir : <?php echo $latest['deadline']?></div>
								<div><a href="<?php echo base_url('index/lowongandetail/'.$latest['id_vacancy']);?>">Detail </a></div>
							</div>
							<p><?php echo $latest['desc_vacancy']?></p>
						</article>
						<?php } ?>
					<?php } ?>

					<!-- <article>
						<img src="http://placehold.it/83x83" data-at2x="http://placehold.it/83x83" alt>
						<h3>Sed aliquet dui at auctor blandit</h3>
						<div class="course-date">
							<div>10<sup>00</sup></div>
							<div>23.02.15</div>
						</div>
						<p>Sed pharetra lorem ut dolor dignissim,
	sit amet pretium tortor mattis.</p>
					</article>
					<article>
						<img src="http://placehold.it/83x83" data-at2x="http://placehold.it/83x83" alt>
						<h3>Sed aliquet dui at auctor blandit</h3>
						<div class="course-date">
							<div>10<sup>00</sup></div>
							<div>23.02.15</div>
						</div>
						<p>Sed pharetra lorem ut dolor dignissim,
	sit amet pretium tortor mattis.</p>
					</article> -->
				</section>
				<section class="grid-col grid-col-4 footer-contact-form">
					<h2 class="corner-radius">Hubungi Kami</h2>
					<div class="email_server_responce"></div>
					<form action="php/contacts-process.php" class="contact-form" method="post" novalidate="novalidate">
						<p><span class="your-name"><input type="text" name="name" value="" size="40" placeholder="Name" aria-invalid="false" required></span>
						</p>
						<p><span class="your-email"><input type="text" name="phone" value="" size="40" placeholder="Phone" aria-invalid="false" required></span> </p>
						<p><span class="your-message"><textarea name="message" placeholder="Comments" cols="40" rows="5" aria-invalid="false" required></textarea></span> </p>
						<button type="submit" class="cws-button bt-color-3 border-radius alt icon-right">Kirim <i class="fa fa-angle-right"></i></button>
					</form>
				</section>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="grid-row clear-fix">
				<div class="copyright">Rekrutmen UAI<span></span> 2017</div>
			</div>
		</div>
	</footer>
	<!-- footer
