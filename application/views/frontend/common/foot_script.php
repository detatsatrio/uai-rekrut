<!-- scripts -->
<script src="<?php echo base_url();?>assets/frontend/js/jquery.min.js"></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/frontend/js/jquery.validate.min.js'></script>
<script src="<?php echo base_url();?>assets/frontend/js/jquery.form.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/TweenMax.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/main.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/select2.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/jquery.isotope.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/jflickrfeed.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/jquery.tweet.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/jquery.fancybox.pack.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/jquery.fancybox-media.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/retina.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox_packed.js"></script>
<!-- scripts -->

<!-- jQuery REVOLUTION Slider  -->
<script type="text/javascript" src="<?php echo base_url();?>assets/frontend/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontend/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontend/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontend/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontend/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontend/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontend/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontend/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontend/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontend/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/jquery.isotope.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/owl.carousel.min.js"></script>

<!-- datetimepicker -->
<script type="text/javascript" src="<?php echo base_url();?>assets/res/datetimepicker/js/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>


 <script type="text/javascript">
// var today = new Date();
// var dd = today.getDate();
// var mm = today.getMonth()+1; //January is 0!
// var yyyy = today.getFullYear();

// if(dd<10) {
//     dd='0'+dd
// }

// if(mm<10) {
//     mm='0'+mm
// }

// today = yyyy+'-'+mm+'-'+dd;

// $('.ambil-tanggal').datetimepicker({
//     format: 'dd-mm-yyyy',
// });
// </script>

<script>
  $(function(){

    //ketika submit button d click
    $("#login").click(function(){

         //do ajax proses
         $.ajax({

           url : "<?php echo base_url();?>login/prosesLogin",
           type: "post", //form method
           data: $(".login-form").serialize(),
           dataType:"json", //misal kita ingin format datanya brupa json
           beforeSend:function(){
             //lakukan apasaja sambil menunggu proses selesai disini
             //misal tampilkan loading

             $(".info-message").show();
             console.log('beforeSend');
           },
           success:function(result){

             if(result.error == '0'){
               	$(".info-message").hide();
               	$(".error-message").hide();
               	$(".confirmation-message").show();
                //console.log(result);
                console.log('sukses');
                window.location.href = result.redirect;
             } else if (result.error == '1') {
               	$(".info-message").hide();
               	$(".confirmation-message").hide();
                $("#error-mess-login").html(result.message);
               	$(".error-message").show();
                console.log('elseif');
             } else {
             	$(".info-message").hide();
             	$(".confirmation-message").hide();
              $(".error-mess").html(result.message);
            	$(".error-message").show();
              console.log('else');
             }

           },
           error: function(xhr, Status, err) {
            	$(".info-message").hide();
            	$(".error-message").show();
              console.log('error');
           }

         });

       return false;
     })

//ketika submit button d click
    $("#forgot-password-btn").click(function(){

         //do ajax proses
         $.ajax({

           url : "<?php echo base_url();?>index/forgot_password",
           type: "post", //form method
           data: $(".forgot-password-form").serialize(),
           dataType:"json", //misal kita ingin format datanya brupa json
           beforeSend:function(){
             //lakukan apasaja sambil menunggu proses selesai disini
             //misal tampilkan loading

             $(".info-message").show();
             console.log('beforeSend');
           },
           success:function(result){

             if(result.respon == '00'){
                $(".info-message").hide();
                $(".error-message").hide();
                $(".confirmation-message").show();
                //console.log(result);
                console.log('sukses');
                setTimeout(function(){
                  window.location.href = result.redirect;
                }, 2000);
             } else if (result.respon == '01') {
                $(".info-message").hide();
                $(".confirmation-message").hide();
                $(".error-mess").html(result.mess);
                $(".error-message").show();
                console.log('elseif');
             } else {
              $(".info-message").hide();
              $(".confirmation-message").hide();
              $(".error-mess").html(result.mess);
              $(".error-message").show();
              console.log('else');
              console.log(result);
             }

           },
           error: function(xhr, Status, err) {
              $(".info-message").hide();
              $(".error-message").show();
              console.log('error');
           }

         });

       return false;
     })


    //ketika submit button d click
    $("#update-akun-btn").click(function(){

         //do ajax proses
         $.ajax({

           url : "<?php echo base_url();?>akun/update_akun",
           type: "post", //form method
           data: $(".update-akun-form").serialize(),
           dataType:"json", //misal kita ingin format datanya brupa json
           beforeSend:function(){
             //lakukan apasaja sambil menunggu proses selesai disini
             //misal tampilkan loading
             $(".info-message").show();
             //console.log('beforeSend');

           },
           success:function(result){
             if(result.error == '00'){
                $(".info-message").hide();
                $(".error-message").hide();
                $(".confirmation-message").show();
                //console.log(result);
                window.location.href = result.redirect;
             } else if (result.error == '01') {
                $(".info-message").hide();
                $(".confirmation-message").hide();
                $(".error-message").show();
                //console.log('elseif');
             } else {
              $(".info-message").hide();
              $(".confirmation-message").hide();
              $(".error-message").show();
              //console.log('else');
             }

           },
           error: function(xhr, Status, err) {
              $(".info-message").hide();
              $(".error-message").show();
              //console.log('error');
           }

         });

       return false;
     })

    //ketika submit button d click
    $("#daftar-form-btn").click(function(){

         //do ajax proses
         $.ajax({

           url : "<?php echo base_url();?>index/register_act",
           type: "post", //form method
           data: $(".daftar-form").serialize(),
           dataType:"json", //misal kita ingin format datanya brupa json
           beforeSend:function(){
             //lakukan apasaja sambil menunggu proses selesai disini
             //misal tampilkan loading
             $(".info-message").show();
             $(".error-message").hide();
             $(".confirmation-message").hide();
             $(".warning-message").hide();
             console.log('beforeSend');

           },
           success:function(result){
             if(result.error == '00'){
                $(".info-message").hide();
                $(".error-message").hide();
                $(".confirmation-message").show();
                $(".warning-message").hide();
                console.log('success');
                window.location.href = result.redirect;
             } else if (result.error == '01') {
                $(".info-message").hide();
                $(".confirmation-message").hide();
                $(".error-message").show();
                $(".warning-message").hide();
                console.log('elseif');
             } else if (result.error == '02') {
                $(".info-message").hide();
                $(".confirmation-message").hide();
                $(".error-message").hide();
                $("#warning").html(result.mess+'\n');
                $(".warning-message").show();
                console.log('02');
             } else {
              $(".info-message").hide();
              $(".confirmation-message").hide();
              $(".error-message").show();
              $(".warning-message").hide();
              console.log('else');
             }

           },
           error: function(xhr, Status, err) {
              $(".info-message").hide();
              $(".confirmation-message").hide();
              $(".error-message").show();
              $(".warning-message").hide();
              console.log('error');
           }

         });

       return false;
     })

    // //ketika submit button d click
    // $(".lamar-btn").click(function(){
    //     var session = "<?php echo $this->session->userdata('role')?>";
    //     var form = document.getElementsByClassName("lamar-form");
    //     (session == '2') ? '' : confirm('Sebelum melamar anda diharuskan mempunyai akun');
    //     $(".lamar-form").submit();
    //  })

    $("#data-pribadi-btn").click(function(){
      $("#data-pribadi").show();
      $("#data-pendidikan").hide();
      $("#data-penelitian").hide();
      $("#pengalaman-mengajar").hide();
      $("#upload-berkas").hide();
    })

    $("#data-pendidikan-btn").click(function(){
      $("#data-pribadi").hide();
      $("#data-pendidikan").show();
      $("#data-penelitian").hide();
      $("#pengalaman-mengajar").hide();
      $("#upload-berkas").hide();
    })

    $("#data-penelitian-btn").click(function(){
      $("#data-pribadi").hide();
      $("#data-pendidikan").hide();
      $("#data-penelitian").show();
      $("#pengalaman-mengajar").hide();
      $("#upload-berkas").hide();
    })

    $("#pengalaman-mengajar-btn").click(function(){
      $("#data-pribadi").hide();
      $("#data-pendidikan").hide();
      $("#data-penelitian").hide();
      $("#pengalaman-mengajar").show();
      $("#upload-berkas").hide();
    })

    $("#upload-berkas-btn").click(function(){
      $("#data-pribadi").hide();
      $("#data-pendidikan").hide();
      $("#data-penelitian").hide();
      $("#pengalaman-mengajar").hide();
      $("#upload-berkas").show();
    })


    // Start pendidikan add
    $("#data-pendidikan-add").click(function(){

      //do ajax proses
         $.ajax({
           url : "<?php echo base_url();?>index/add_education",
           type: "post", //form method
           //data: $(".daftar-form").serialize(),
           dataType:"json",
           beforeSend:function(){
             console.log('beforeSend');
           },
           success:function(result){
             if (result.error == '00'){
                    var contents = "";
                    var number = result.count + 1;

                    contents +='<div class="community color-1" style="text-align:left;padding:10px;" id="div-pendidikan-'+number+'">';
                    contents +='<input type="hidden" value="" name="id-education-'+number+'">';
                    contents +='<input type="hidden" value="insert" name="action-'+number+'">';
                    contents +='<h3>Data Pendidikan Ke - '+number+'</h3>';
                    contents +='<div width="50%">';
                    contents +='Jenjang Pendidikan :';
                    contents +='<p class="form-row form-row-wide select-arrow">';
                    contents +='<select name="jenjang-'+number+'" required>';
                    contents +='<option value="1">DI</option>';
                    contents +='<option value="2">DII</option>';
                    contents +='<option value="3">DIII</option>';
                    contents +='<option value="4">DIV</option>';
                    contents +='<option value="5">S1</option>';
                    contents +='<option value="6">S2</option>';
                    contents +='<option value="7">S3</option>';
                    contents +='</select>';
                    contents +='<p class="form-row form-row-wide">';
                    contents +='Masa studi :';
                    contents +='<table>';
                    contents +='<tr>';
                    contents +='<td style="padding-right:10px;"><input name="in-year-'+number+'" type="text" value="" required><small><i>ex : 31 - 08 - 2011</i></small></td>';
                    contents +='<td> - </td>';
                    contents +='<td style="padding-left:10px;"><input name="end-year-'+number+'" type="text" value="" required><small><i>ex : 31 - 08 - 2015</i></small></td>';
                    contents +='</tr>';
                    contents +='</table>';
                    contents +='</p>';
                    contents +='<p class="form-row form-row-wide">';
                    contents +='Nomor Induk Mahasiswa :';
                    contents +='<input type="text" name="nim-'+number+'" required>';
                    contents +='</p>';
                    contents +='<p class="form-row form-row-wide">';
                    contents +='Jurusan / Program Studi :';
                    contents +='<input type="text" name="prodi-'+number+'" required>';
                    contents +='</p>';
                    contents +='<p class="form-row form-row-wide">';
                    contents +='Bidang Minat :';
                    contents +='<input type="text" name="minat-'+number+'" >';
                    contents +='</p>';
                    contents +='<p class="form-row form-row-wide">';
                    contents +='Judul Karya Ilmiah :';
                    contents +='<input type="text" name="karya-'+number+'" required>';
                    contents +='</p>';
                    contents +='</div>';
                    contents +='</div>';
                    contents +='<br>';

                    $("#data-pendidikan-kosong").hide();
                    $('#data-pendidikan-form').hide().append(contents).fadeIn(1000);
                    //$('html, body').animate({scrollTop: $('#div-pendidikan'+number).offset().top}, 'slow');
                    $("#data-pendidikan-add").hide();
             } else if (result.error == '01') {
                alert(result.mess);
             }
           },
           error: function(xhr, Status, err) {
              console.log('error');
           }

         });

       return false;
    })


     // Start pendidikan add
    $("#data-penelitian-add").click(function(){

      //do ajax proses
         $.ajax({
           url : "<?php echo base_url();?>index/add_research",
           type: "post", //form method
           //data: $(".daftar-form").serialize(),
           dataType:"json",
           beforeSend:function(){
             console.log('beforeSend');
           },
           success:function(result){
             if (result.error == '00'){
                console.log(result);
                    var contents = "";
                    var number = result.count + 1;

                    contents +='<input type="hidden" value="" name="id-research-'+number+'">';
                    contents +='<input type="hidden" value="insert-res" name="action-'+number+'">';
                    contents +='<div class="community color-1" style="text-align:left;padding:10px;">';
                    contents +='<h3>Data Penelitian - '+number+'</h3>';
                    contents +='<p>';
                    contents +='Judul Penelitian :';
                    contents +='<input name="judul-penelitian-'+number+'" type="text" size="30" aria-required="true" placeholder="Judul Penelitian" value="" required>';
                    contents +='</p>';
                    contents +='<p>';
                    contents +='Kategori Penelitian :';
                    contents +='<input name="kategori-penelitian-'+number+'" type="text" value="" size="30" aria-required="true" placeholder="Kategori Penelitian" required>';
                    contents +='</p>';
                    contents +='<p>';
                    contents +='Nama :';
                    contents +='<input name="nama-publikasi-'+number+'" type="text" value="" size="30" aria-required="true" placeholder="Nama Publikasi" required>';
                    contents +='</p>';
                    contents +='<p>';
                    contents +='Publikasi :';
                    contents +='<input name="publikasi-'+number+'" type="text" value="" size="30" aria-required="true" placeholder="Publikasi" required>';
                    contents +='</p>';
                    contents +='<p>';
                    contents +='Tahun :';
                    contents +='<input name="tahun-'+number+'" type="text" value="" size="30" aria-required="true" placeholder="Tahun" required>';
                    contents +='</p>';
                    contents +='</div>';
                    contents +='<br>';

                    $("#data-penelitian-kosong").hide();
                    $('#data-penelitian-form').hide().append(contents).fadeIn(1000);
                    //$('html, body').animate({scrollTop: $('#div-pendidikan'+number).offset().top}, 'slow');
                    $("#data-penelitian-add").hide();
             } else if (result.error == '01') {
                alert(result.mess);
             }
           },
           error: function(xhr, Status, err) {
              console.log('error');
           }

         });

       return false;
    })

    // Start pendidikan add
    $("#data-mengajar-add").click(function(){

      //do ajax proses
         $.ajax({
           url : "<?php echo base_url();?>index/add_teach",
           type: "post", //form method
           //data: $(".daftar-form").serialize(),
           dataType:"json",
           beforeSend:function(){
             console.log('beforeSend');
           },
           success:function(result){
             if (result.error == '00'){
                console.log(result);

                    var contents = "";
                    var number = result.count + 1;
                    contents +='<input type="hidden" value="" name="id-teach-'+number+'">';
                    contents +='<input type="hidden" value="insert-ajar" name="action-'+number+'">';
                    contents +='<div class="community color-1" style="text-align:left;padding:10px;">';
                    contents +='<h3>Pengalaman Mengajar Ke - '+number+'</h3>';
                    contents +='<p>';
                    contents +='Mata Kuliah :';
                    contents +='<input name="mata-kuliah-'+number+'" type="text" value="" placeholder="Mata Kuliah" required>';
                    contents +='</p>';
                    contents +='<p>';
                    contents +='Semester :';
                    contents +='<input name="semester-'+number+'" type="text" value="" placeholder="Semester" required>';
                    contents +='</p>';
                    contents +='<p>';
                    contents +='Tahun Akademik :';
                    contents +='<input name="tahun-'+number+'" type="text" value="" placeholder="Tahun Akademik" required>';
                    contents +='</p>';
                    contents +='Jenjang :';
                    contents +='<p class="form-row form-row-wide select-arrow">';
                    contents +='<select name="jenjang-'+number+'" required>';
                    contents +='<option value="1">DI</option>';
                    contents +='<option value="2">DII</option>';
                    contents +='<option value="3">DIII</option>';
                    contents +='<option value="4">DIV</option>';
                    contents +='<option value="5">S1</option>';
                    contents +='<option value="6">S2</option>';
                    contents +='<option value="7">S3</option>';
                    contents +='</select>';
                    contents +='</p>';
                    contents +='<p>';
                    contents +='Jurusan / Prodi :';
                    contents +='<input name="jurusan-'+number+'" type="text" value="" placeholder="Jurusan / Prodi" required>';
                    contents +='</p>';
                    contents +='<p>';
                    contents +='Fakultas :';
                    contents +='<input name="fakultas-'+number+'" type="text" value="" placeholder="Fakultas" required>';
                    contents +='</p>';
                    contents +='<p>';
                    contents +='Universitas / Perguruan Tinggi :';
                    contents +='<input name="perguruan-tinggi-'+number+'" type="text" value="" placeholder="Universitas / Perguruan Tinggi" required>';
                    contents +='</p>';
                    contents +='</div>';
                    contents +='<br>';

                    $("#data-mengajar-kosong").hide();
                    $('#data-mengajar-form').hide().append(contents).fadeIn(1000);
                    //$('html, body').animate({scrollTop: $('#div-pendidikan'+number).offset().top}, 'slow');
                    $("#data-mengajar-add").hide();
             } else if (result.error == '01') {
                alert(result.mess);
             }
           },
           error: function(xhr, Status, err) {
              console.log('error');
           }

         });

       return false;
    })

     document.getElementById("file-input-id").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {
              // get loaded data and render thumbnail.
              document.getElementById("id-pict").src = e.target.result;
          };

          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };

      document.getElementById("file-input-ktp").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {
              // get loaded data and render thumbnail.
              document.getElementById("id-card").src = e.target.result;
          };

          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };


      document.getElementById("file-input-cert-inter").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {
              // get loaded data and render thumbnail.
              document.getElementById("cert-inter").src = e.target.result;
          };

          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };

      document.getElementById("file-input-sehat-rohani").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {
              // get loaded data and render thumbnail.
              document.getElementById("sehat-rohani").src = e.target.result;
          };

          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };


      document.getElementById("file-input-sehat-rohani").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {
              // get loaded data and render thumbnail.
              document.getElementById("sehat-rohani").src = e.target.result;
          };

          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };

      document.getElementById("file-input-sk-mendiknas").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {
              // get loaded data and render thumbnail.
              document.getElementById("sk-mendiknas").src = e.target.result;
          };

          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };

      document.getElementById("file-input-bebas-narkotika").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {
              // get loaded data and render thumbnail.
              document.getElementById("bebas-narkotika").src = e.target.result;
          };

          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };

      document.getElementById("file-input-ijazah-s1").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {
              // get loaded data and render thumbnail.
              document.getElementById("ijazah-s1").src = e.target.result;
          };

          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };

      document.getElementById("file-input-lolos-butuh").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {
              // get loaded data and render thumbnail.
              document.getElementById("lolos-butuh").src = e.target.result;
          };

          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };

      document.getElementById("file-input-ijazah-s2").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {
              // get loaded data and render thumbnail.
              document.getElementById("ijazah-s2").src = e.target.result;
          };

          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };

      document.getElementById("file-input-sk-berhenti").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {
              // get loaded data and render thumbnail.
              document.getElementById("sk-berhenti").src = e.target.result;
          };

          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };

      document.getElementById("file-input-ijazah-s3").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {
              // get loaded data and render thumbnail.
              document.getElementById("ijazah-s3").src = e.target.result;
          };

          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };

      document.getElementById("file-input-sehat-jasmani").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {
              // get loaded data and render thumbnail.
              document.getElementById("sehat-jasmani").src = e.target.result;
          };

          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };


  });
</script>
