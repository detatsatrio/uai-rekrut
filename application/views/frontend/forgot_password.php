<main>
	<section class="fullwidth-background bg-2">
		<div class="grid-row">
			<div class="login-block">
				<div class="logo">
					<img src="<?php echo base_url()?>assets/logo_uai2.png" data-at2x='<?php echo base_url()?>assets/logo_uai2.png' alt>
				</div>
				<!-- info box -->
				<div class="info-boxes confirmation-message" style="display:none;">
					<div class="info-box-icon"><i class="fa fa-check"></i></div>
					<strong>Sukses</strong><br />Email telah dikirim...
					<div class="close-button"></div>
				</div>
				<!--/info box -->
				<div class="info-boxes error-message" style="display:none;">
					<div class="info-box-icon"><i class="fa fa-times"></i></div>
					<strong>Otentikasi gagal</strong><br /><div class="error-mess"></div>
					<div class="close-button"></div>
				</div>
				<!-- info box -->
				<div class="info-boxes info-message" style="display:none;">
					<div class="info-box-icon"><i class="fa fa-info"></i></div>
					<strong>System Processing</strong><br />Pengecekan sistem harap menunggu...
					<div class="close-button"></div>
				</div>
				<!--/info box -->
				<div class="form-group">
						<input type="text" class="email forgot-password-form" placeholder="Masukan Email Anda" name="email-forgot" required>
						<span class="input-icon">
							<i class="fa fa-mail"></i>
						</span>
					</div>
				<button type="button" class="button-fullwidth cws-button bt-color-3 border-radius alt" id="forgot-password-btn"> Kirim Password</button>
			</div>
		</div>
	</section>
</main>

