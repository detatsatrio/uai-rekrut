<main>
	<section class="fullwidth-background bg-2">
		<div class="grid-row">
			<div class="login-block">
				<div class="logo">
					<img src="<?php echo base_url()?>assets/logo_uai2.png" data-at2x='img/logo@2x.png' alt>
				</div>

				<!-- info box -->
				<div class="info-boxes confirmation-message" style="display:none;">
					<div class="info-box-icon"><i class="fa fa-check"></i></div>
					<strong>Sukses daftar</strong><br />Mohon tunggu...
					<div class="close-button"></div>
				</div>
				<!--/info box -->
				<div class="info-boxes error-message" style="display:none;">
					<div class="info-box-icon"><i class="fa fa-times"></i></div>
					<strong>Otentikasi gagal</strong><br />Form tidak boleh kosong
					<div class="close-button"></div>
				</div>
				<!-- info box -->
				<div class="info-boxes info-message" style="display:none;">
					<div class="info-box-icon"><i class="fa fa-info"></i></div>
					<strong>System Processing</strong><br />Pendaftaran akun harap menunggu...
					<div class="close-button"></div>
				</div>
				<!--/info box -->
				<!-- info box -->
				<div class="info-boxes warning-message" style="display:none;">
					<div class="info-box-icon"><i class="fa fa-info"></i></div>
					<strong>Peringatan!</strong><br /><div id="warning"></div>
					<div class="close-button"></div>
				</div>
				<!--/info box -->
				
				<div class="form-group">
					<input type="text" class="daftar-form" placeholder="Username" name="username" required>
					<span class="input-icon">
						<i class="fa fa-user"></i>
					</span>
				</div>
				<div class="form-group">
					<input type="password" class="daftar-form" placeholder="Password" name="password" required>
					<span class="input-icon">
						<i class="fa fa-lock"></i>
					</span>
				</div>
				<div class="form-group">
					<input type="text" class="daftar-form" placeholder="Email" name="email" required>
					<span class="input-icon">
						<i class="fa fa-envelope-o"></i>
					</span>
				</div>
				<button type="button" class="button-fullwidth cws-button bt-color-3 border-radius" id="daftar-form-btn"> DAFTAR</button>
			</div>
		</div>
	</section>
</main>

