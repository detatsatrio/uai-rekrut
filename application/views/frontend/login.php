<main>
	<section class="fullwidth-background bg-2">
		<div class="grid-row">
			<div class="login-block">
				<div class="logo">
					<img src="<?php echo base_url()?>assets/logo_uai2.png" data-at2x='<?php echo base_url()?>assets/logo_uai2.png' alt>
				</div>
				<!-- info box -->
				<div class="info-boxes confirmation-message" style="display:none;">
					<div class="info-box-icon"><i class="fa fa-check"></i></div>
					<strong>Sukses login</strong><br />Mohon tunggu...
					<div class="close-button"></div>
				</div>
				<!--/info box -->
				<div class="info-boxes error-message" style="display:none;">
					<div class="info-box-icon"><i class="fa fa-times"></i></div>
					<strong>Otentikasi gagal</strong><br /><div id="error-mess-login"></div>
					<div class="close-button"></div>
				</div>
				<!-- info box -->
				<div class="info-boxes info-message" style="display:none;">
					<div class="info-box-icon"><i class="fa fa-info"></i></div>
					<strong>System Processing</strong><br />Pengecekan sistem harap menunggu...
					<div class="close-button"></div>
				</div>
				<!--/info box -->
				<form class="login-form">
					<div class="form-group">
						<input type="text" class="login-input" placeholder="Username" name="username" required>
						<span class="input-icon">
							<i class="fa fa-user"></i>
						</span>
					</div>
					<div class="form-group">
						<input type="password" class="login-input" placeholder="Pasword" name="password" required>
						<span class="input-icon">
							<i class="fa fa-lock"></i>
					</div>
					<p class="small">
					</span>
						<a href="<?php echo base_url()?>login/forgotPassword">Lupa Password?</a>
					</p>
					<button type="submit" class="button-fullwidth cws-button bt-color-3 alt" id="login"> MASUK</button>
					<button type="button" onclick="location.href='<?php echo base_url()?>index/register'" class="button-fullwidth cws-button bt-color-3"> DAFTAR</button>
				</form>
				<div class="login-or">
					<hr class="hr-or">
					<span class="span-or">or</span>
				</div>
				<a href="{linkedin_auth}"> <img src="<?php echo base_url()?>/assets/linkedin-login-btn-small.png" width="100%"/></a>
			</div>
		</div>
	</section>
</main>
