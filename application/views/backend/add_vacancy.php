<div class="main-content">
    <div class="panel-content">
        <div class="main-title-sec">
            <div class="row">
                <div class="col-md-3 column">
                    <div class="heading-profile">
                        <h2>Add Vacancy</h2>
                        <span>Please fill form for job vacancy</span>
                    </div>
                </div>
            </div>
        </div><!-- Heading Sec -->
        <ul class="breadcrumbs">
            <li><a href="<?php echo base_url();?>admin" title="">Dashboard</a></li>
            <li><a href="" title="">Add Vacancy</a></li>
        </ul>

        <div class="main-content-area">

          <!-- <div class="notification">
              <div role="alert" class="alert fade color green-bg" id="success-notif">
                   
              </div>
              <div role="alert" class="alert fade color blue-bg" style="display:none;" id="please-wait">
                   <strong>Please wait...</strong> Processing data..
              </div>
              <div role="alert" class="alert fade color red-bg" style="display:none;" id="failed-notif">
                   <strong>Sorry!</strong> save new vacancy is unsuccessful, please check your data and submit again.
              </div>
         </div>  -->
         <br/>
         <center>
          <span class="alert-sukses" style="display:none;"><strong>Done!</strong> You successfully save new vacancy.</span>
          <span class="alert-process" style="display:none;"><strong>Please wait...</strong> Processing data..</span>
          <span class="alert-error" style="display:none;"><strong>Sorry!</strong> save new vacancy is unsuccessful, please check your data and submit again.</span>
          <span class="alert-infor" style="display:none;"><strong>Sorry!</strong> <span id="mess"></span></span>
         </center>
          <form class="vacancy-form" >
            <div class="row">
              <div class="col-md-12">
                <div class="widget white no-padding">
                    <div class="widget-tabs">
                         <ul class="nav nav-tabs">
                              <li class="active"><a data-toggle="tab" data-target="#information">Vacancy Information</a></li>
                              <li><a data-toggle="tab" data-target="#interview_1">Interview I</a></li>
                              <li><a data-toggle="tab" data-target="#psikotest">Psikotest</a></li>
                              <li><a data-toggle="tab" data-target="#toefl">TOEFL</a></li>
                              <li><a data-toggle="tab" data-target="#interview_2">Interview II</a></li>
                         </ul>
                         <div class="tab-content">
                              <div id="information" class="tab-pane fade in active">
                                <div class="form-group">
                                     <label for="job">Fakultas</label>
                                     <select name="fakultas" class="form-control fakultas" required>
                                       <option value="-">Please Choose</option>
                                     </select>
                                  </div>
                                  <div class="form-group">
                                     <label for="job">Prodi</label>
                                     <select name="prodi" class="form-control prodi" required>
                                       <option value="-">Fakultas Must Be Selected</option>
                                     </select>
                                </div>
                                <div class="form-group">
                                   <label for="job">Job</label>
                                   <input type="text" placeholder="Job" id="job" name="job" class="form-control" value="{job}" >
                                </div>
                                <div class="form-group">
                                   <label for="Description">Description</label>
                                   <textarea name="description" id="description" class="form-control" placeholder="Description" rows="5" ></textarea>
                                </div>
                                <div class="form-group">
                                   <label for="deadline">Deadline</label>
                                   <input type="text" placeholder="Deadline" id="datetimepicker" name="deadline" class="form-control datetimepicker" value="{deadline}" >
                                </div>
                              </div>
                              <div id="interview_1" class="tab-pane fade">
                                  <div class="form-group">
                                     <label for="job">Interview I Schedule</label>
                                     <!-- <input type="text" placeholder="Schedule" id="datetimepicker" name="interview_schedule_1" class="form-control" value="2012-05-15 21:05" required>-->
                                      <input type="text" value="{interview_schedule_1}" class="form-control datetimepicker" name="interview_schedule_1" id="datetimepicker" readonly>
                                  </div>
                                  <div class="form-group">
                                     <label for="interview_description_1">Interview I Description</label>
                                     <textarea name="interview_description_1" id="description" class="form-control" placeholder="Interview I Description" rows="5" >{interview_description_1}</textarea>
                                  </div>
                              </div>
                              <div id="psikotest" class="tab-pane fade">
                                   <div class="form-group">
                                     <label for="psikotest">Psikotest Schedule</label>
                                     <!-- <input type="text" placeholder="Schedule" id="datetimepicker" name="interview_schedule_1" class="form-control" value="2012-05-15 21:05" >-->
                                      <input type="text" value="{psikotest_schedule}" name="psikotest_schedule" class="form-control datetimepicker"  id="datetimepicker" readonly>
                                  </div>
                                  <div class="form-group">
                                     <label for="psikotest">Psikotest Description</label>
                                     <textarea name="psikotest_description" id="description" class="form-control" placeholder="Psikotest Description" rows="5" >{psikotest_description}</textarea>
                                  </div>
                              </div>
                              <div id="toefl" class="tab-pane fade">
                                   <div class="form-group">
                                     <label for="toefl_schedule">TOEFL Schedule</label>
                                     <!-- <input type="text" placeholder="Schedule" id="datetimepicker" name="interview_schedule_1" class="form-control" value="2012-05-15 21:05" >-->
                                      <input type="text" value="{toefl_schedule}" name="toefl_schedule" class="form-control datetimepicker"  id="datetimepicker" readonly>
                                  </div>
                                  <div class="form-group">
                                     <label for="toefl_description">TOEFL Description</label>
                                     <textarea name="toefl_description" id="description" class="form-control" placeholder="Description" rows="5" >{description}</textarea>
                                  </div>
                              </div>
                              <div id="interview_2" class="tab-pane fade">
                                   <div class="form-group">
                                     <label for="job">Interview II Schedule</label>
                                     <!-- <input type="text" placeholder="Schedule" id="datetimepicker" name="interview_schedule_1" class="form-control" value="2012-05-15 21:05" >-->
                                      <input type="text" value="{interview_schedule_2}" name="interview_schedule_2" class="form-control datetimepicker"  id="datetimepicker" readonly>
                                  </div>
                                  <div class="form-group">
                                     <label for="Description">Description</label>
                                     <textarea name="interview_description_2" id="description" class="form-control" placeholder="Description" rows="5" >{description}</textarea>
                                  </div>
                              </div>
                         </div>

                    </div>

                </div>  

              </div>

            </div>
            <br />
            <button class="btn btn-primary btn-block" type="submit" id="vacancy">Save</button>
            
            </form>
        </div>
    </div>
</div><!-- Panel Content -->
