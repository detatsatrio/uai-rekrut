<div class="main-content">
    <div class="panel-content">
        <div class="main-title-sec">
            <div class="row">
                <div class="col-md-3 column">
                    <div class="heading-profile">
                        <h2>Add User</h2>
                        <span>Please fill form for user</span>
                    </div>
                </div>
            </div>
        </div><!-- Heading Sec -->
        <ul class="breadcrumbs">
            <li><a href="<?php echo base_url();?>admin" title="">Dashboard</a></li>
            <li><a href="" title="">Add User</a></li>
        </ul>

        <div class="main-content-area">
         <br/>
         <center>
          <span class="alert-sukses" style="display:none;"><strong>Done!</strong> You successfully save new vacancy.</span>
          <span class="alert-process" style="display:none;"><strong>Please wait...</strong> Processing data..</span>
          <span class="alert-error" style="display:none;"><strong>Sorry!</strong> please check your data and submit again.</span>
          <span class="alert-infor" style="display:none;"><strong>Sorry!</strong> <span id="mess"></span></span>
         </center>
          <form class="user-form">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                       <label for="username">Username</label>
                       <input type="text" placeholder="Username" id="username" name="username" class="form-control" value="{username}" required="required">
                    </div>
                    <div class="form-group">
                       <label for="password">Password</label>
                       <input type="password" placeholder="Password" id="password" name="password" class="form-control" value="{password}" required="required">
                    </div>
                    <div class="form-group">
                       <label for="email">Email</label>
                       <input type="email" placeholder="Email" id="email" name="email" class="form-control" value="{email}" >
                    </div>
                    <div class="form-group">
                       <label for="role">Role</label>
                       <select name="role" class="form-control">
                         <?php if($role == '1') { ?>
                          <option value="1" selected> Admin</option>
                          <option value="2"> Participant</option>
                          <option value="3"> Super Admin</option>
                         <?php } elseif ($role == '3') { ?>
                           <option value="1"> Admin</option>
                          <option value="2"> Participant</option>
                          <option value="3" selected> Super Admin</option>
                         <?php } else { ?>
                          <option value="1"> Admin</option>
                          <option value="2" selected> Participant</option>
                          <option value="3"> Super Admin</option>
                         <?php } ?>
                       </select>
                    </div>
                    <div class="form-group">
                       <label for="status">Status</label>
                       <select name="status" class="form-control">
                         <?php if($status == '1') { ?>
                          <option value="1" selected> Enable</option>
                          <option value="2"> Disable</option>
                         <?php } else { ?>
                          <option value="1" > Enable</option>
                          <option value="2" selected> Disable</option>
                         <?php } ?>
                       </select>
                    </div>
              </div>

            </div>
            <br />
            <button class="btn btn-primary btn-block" type="submit" id="user">Save</button>
            </form>
        </div>
    </div>
</div><!-- Panel Content -->
