<div class="main-content">
    <div class="panel-content">
        <div class="main-title-sec">
            <div class="row">
                <div class="col-md-3 column">
                    <div class="heading-profile">
                        <h2>List Vacancy</h2>
                        <span></span>
                    </div>
                </div>
                <div class="col-md-9 column">
                    <div class="quick-btn-title">
                        <a href="{add_vacancy}" title=""><i class="fa fa-plus"></i> New Vacancy</a>
                    </div>
                </div>
            </div>
        </div><!-- Heading Sec -->
        <ul class="breadcrumbs">
            <li><a href="<?php echo base_url();?>admin" title="">Dashboard</a></li>
            <li><a href="" title="">List Vacancy</a></li>
        </ul>
        <div class="main-content-area">
               <div class="row">
                    <div class="col-md-12">

                         <div class="streaming-table">

                                   <span id="found" class="label label-info"></span>
                                   <table id="stream_table" class='table table-striped table-bordered'>
                                     <thead>
                                        <tr>
                                          <th>ID Job</th>
                                          <th>Job</th>
                                          <th>Interview I</th>
                                          <th>Psikotest</th>
                                          <th>Toefl</th>
                                          <th>Interview II</th>
                                          <th>Deadline</th>
                                          <th width="15px">Action</th>
                                        </tr>
                                     </thead>
                                     <tbody>
                                        <?php if(isset($vacancy)=='') { ?>
                                          <h4>Vacancy is empty</h4>
                                        <?php } else { ?>
                                          <?php foreach ($vacancy as $vac) { ?>

                                            <tr>
                                                <td><?php echo 'Job - '.str_pad($vac['id_vacancy'], 4, '0', STR_PAD_LEFT)?></td>
                                                <td><?php echo $vac['title_vacancy'];?></td>
                                                <td><?php echo $vac['inter_schedule'];?></td>
                                                <td><?php echo $vac['psiko_schedule'];?></td>
                                                <td><?php echo $vac['toefl_schedule'];?></td>
                                                <td><?php echo $vac['inter2_schedule'];?></td>
                                                <td><?php echo $vac['deadline'];?></td>
                                                <td>
                                                  <?php
                                                    date_default_timezone_set('Asia/Jakarta');
                                                    if ($vac['deadline'] <= date('Y-m-d H:i')) { ?>
                                                      <a title="" id="edit-vacancy" class="c-btn small green-bg btn-block edit-vacancy" data-toggle="modal" data-id="<?php echo $vac['id_vacancy']?>" data-fakultas="<?php echo $vac['fakultas']?>" data-prodi="<?php echo $vac['prodi']?>" data-title="<?php echo $vac['title_vacancy']?>" data-desc-vacancy="<?php echo $vac['desc_vacancy']?>" data-inter-schedule="<?php echo $vac['inter_schedule']?>" data-inter-desc="<?php echo $vac['inter_desc']?>" data-psiko-schedule="<?php echo $vac['psiko_schedule']?>" data-psiko-desc="<?php echo $vac['psiko_desc']?>" data-toefl-schedule="<?php echo $vac['toefl_schedule']?>" data-toefl-desc="<?php echo $vac['toefl_desc']?>" data-inter-schedulep="<?php echo $vac['inter2_schedule']?>" data-inter-descp="<?php echo $vac['inter2_desc']?>" data-deadline="<?php echo $vac['deadline']?>" style="margin-bottom:5px;">Edit</a>
                                                      <form action="{result_vacancy}" method="post" style="margin-bottom:5px;">
                                                      <input type="hidden" value="<?php echo $vac['id_vacancy']?>" name="id_vacancy">
                                                      <button type="submit" class="c-btn small blue-bg btn-block">Result</button>
<!--                                                       <a class="" data-toggle="modal"></a> -->
                                                      </form>
                                                    <?php } else { ?>
                                                      <a title="" id="edit-vacancy" class="c-btn small green-bg btn-block edit-vacancy" data-toggle="modal" data-id="<?php echo $vac['id_vacancy']?>" data-fakultas="<?php echo $vac['fakultas']?>" data-prodi="<?php echo $vac['prodi']?>" data-title="<?php echo $vac['title_vacancy']?>" data-desc-vacancy="<?php echo $vac['desc_vacancy']?>" data-inter-schedule="<?php echo $vac['inter_schedule']?>" data-inter-desc="<?php echo $vac['inter_desc']?>" data-psiko-schedule="<?php echo $vac['psiko_schedule']?>" data-psiko-desc="<?php echo $vac['psiko_desc']?>" data-toefl-schedule="<?php echo $vac['toefl_schedule']?>" data-toefl-desc="<?php echo $vac['toefl_desc']?>" data-inter-schedulep="<?php echo $vac['inter2_schedule']?>" data-inter-descp="<?php echo $vac['inter2_desc']?>" data-deadline="<?php echo $vac['deadline']?>">Edit</a>
                                                      <a title="" class="c-btn small red-bg btn-block delete-vacancy" data-toggle="modal" data-id="<?php echo $vac['id_vacancy']?>" style="margin-bottom:5px;">Delete</a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php  } ?>
                                        <?php } ?>
                                     </tbody>
                                   </table>
                                   <div id="summary">
                                   <div>
                                </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
    </div>
</div><!-- Panel Content -->

<div class="modal fade small-modal" id="delete-vacancy-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Notification</h4>
      </div>
      <div class="modal-body">
        <center>
          <span class="alert-sukses" style="display:none;"><strong>Done!</strong> You successfully deleted vacancy.</span>
          <span class="alert-process" style="display:none;"><strong>Please wait...</strong> Processing data..</span>
          <span class="alert-error" style="display:none;"><strong>Sorry!</strong> delete vacancy is unsuccessful</span>
          <span class="alert-infor" style="display:none;"><strong>Sorry!</strong> <span id="mess"></span></span>
         </center>
         <input type="hidden" id="id_vacancy_delete" name="id_vacancy" class="id_vacancy_delete" value="">
        <center>Are you sure ?</center>
      </div>
      <div class="modal-footer">
        <button type="button" class="c-btn large red-bg btn-block" id="btn-delete-vacancy">Delete</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="edit-vacancy-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Data Vacancy</h4>
      </div>
      <div class="modal-body">
        <br />
        <center>
          <span class="alert-sukses" style="display:none;"><strong>Done!</strong> You successfully save vacancy.</span>
          <span class="alert-process" style="display:none;"><strong>Please wait...</strong> Processing data..</span>
          <span class="alert-error" style="display:none;"><strong>Sorry!</strong> save vacancy is unsuccessful, please check your data and submit again.</span>
          <span class="alert-infor" style="display:none;"><strong>Sorry!</strong> <span class="mess"></span></span>
         </center>
         <br/>
        <div class="widget-tabs">
           <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" data-target="#information">Vacancy Information</a></li>
                <li><a data-toggle="tab" data-target="#interview_1">Interview I</a></li>
                <li><a data-toggle="tab" data-target="#psikotest">Psikotest</a></li>
                <li><a data-toggle="tab" data-target="#toefl">TOEFL</a></li>
                <li><a data-toggle="tab" data-target="#interview_2">Interview II</a></li>
           </ul>
           <div class="tab-content">

                <div id="information" class="tab-pane fade in active">
                  <div class="form-group">
                       <label for="job">Fakultas</label>
                       <input type="hidden" value="" class="edit-form-vacancy idfakultas" id="idfakultas">
                       <input type="hidden" value="" class="edit-form-vacancy idprodi">
                       <select name="fakultas" class="form-control fakultas-edit edit-form-vacancy" required>
                         <option value="-">Please Choose</option>
                       </select>
                    </div>
                    <div class="form-group">
                       <label for="job">Prodi</label>
                       <select name="prodi" class="form-control prodi-edit edit-form-vacancy" required>
                         <option value="-">Fakultas Must Be Selected</option>
                       </select>
                  </div>
                  <div class="form-group">
                     <label for="job">Job</label>
                     <input type="hidden" id="id_vacancy" name="id_vacancy" class="id_vacancy_edit edit-form-vacancy" value="">
                     <input type="hidden" id="action" name="action" class="edit-form-vacancy" value="update">
                     <input type="text" placeholder="Job" id="job" name="job" class="form-control edit-form-vacancy job" value="">
                  </div>
                  <div class="form-group">
                     <label for="Description">Description</label>
                     <textarea name="description" id="description" class="form-control vacancy-description edit-form-vacancy" placeholder="Description" rows="5"></textarea>
                  </div>
                  <div class="form-group">
                     <label for="deadline">Deadline</label>
                     <input type="text" placeholder="Deadline" id="datetimepicker" name="deadline" class="form-control datetimepicker deadline edit-form-vacancy" value="" >
                  </div>
                </div>
                <div id="interview_1" class="tab-pane fade">
                    <div class="form-group">
                       <label for="job">Interview I Schedule</label>
                       <!-- <input type="text" placeholder="Schedule" id="datetimepicker" name="interview_schedule_1" class="form-control" value="2012-05-15 21:05" required>-->
                        <input type="text" value="" class="form-control datetimepicker interview_schedule_1 edit-form-vacancy" name="interview_schedule_1" id="datetimepicker" >
                    </div>
                    <div class="form-group">
                       <label for="interview_description_1">Interview I Description</label>
                       <textarea name="interview_description_1" id="interview_description_1" class="form-control interview_description_1 edit-form-vacancy" placeholder="Interview I Description" rows="5"></textarea>
                    </div>

                </div>
                <div id="psikotest" class="tab-pane fade">
                     <div class="form-group">
                       <label for="psikotest">Psikotest Schedule</label>
                       <!-- <input type="text" placeholder="Schedule" id="datetimepicker" name="interview_schedule_1" class="form-control" value="2012-05-15 21:05" >-->
                        <input type="text" value="" name="psikotest_schedule" class="form-control datetimepicker psikotes_schedule edit-form-vacancy"  id="datetimepicker" >
                    </div>
                    <div class="form-group">
                       <label for="psikotest">Psikotest Description</label>
                       <textarea name="psikotest_description" id="description" class="form-control psikotes_description edit-form-vacancy" placeholder="Psikotest Description" rows="5" ></textarea>
                    </div>

                </div>
                <div id="toefl" class="tab-pane fade">
                     <div class="form-group">
                       <label for="toefl_schedule">TOEFL Schedule</label>
                       <!-- <input type="text" placeholder="Schedule" id="datetimepicker" name="interview_schedule_1" class="form-control" value="2012-05-15 21:05" >-->
                        <input type="text" value="" name="toefl_schedule" class="form-control datetimepicker toefl_schedule edit-form-vacancy"  id="datetimepicker" >
                    </div>
                    <div class="form-group">
                       <label for="toefl_description">TOEFL Description</label>
                       <textarea name="toefl_description" id="description" class="form-control toefl_description edit-form-vacancy" placeholder="Description" rows="5" ></textarea>
                    </div>

                </div>
                <div id="interview_2" class="tab-pane fade">
                     <div class="form-group">
                       <label for="job">Interview II Schedule</label>
                       <!-- <input type="text" placeholder="Schedule" id="datetimepicker" name="interview_schedule_1" class="form-control" value="2012-05-15 21:05" >-->
                        <input type="text" value="" name="interview_schedule_2" class="form-control datetimepicker interview_schedule_2 edit-form-vacancy"  id="datetimepicker" >
                    </div>
                    <div class="form-group">
                       <label for="Description">Description</label>
                       <textarea name="interview_description_2" id="description" class="form-control interview_description_2 edit-form-vacancy" placeholder="Description" rows="5" ></textarea>
                    </div>

                </div>
           </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="c-btn large green-bg btn-block" id="update_vacancy">Update</button>
      </div>
      </div>
    </div>
  </div>
</div>
