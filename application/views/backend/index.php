<div class="main-content">
    <div class="panel-content">
        <div class="main-title-sec">
            <div class="row">
                <div class="col-md-3 column">
                    <div class="heading-profile">
                        <h2>Dashboard</h2>
                        <span>Welcome back, {username}</span>
                    </div>
                </div>
                <div class="col-md-9 column">
                    <div class="quick-btn-title">
                        <a href="{add_vacancy}" title=""><i class="fa fa-plus"></i> New Vacancy</a>
                    </div>
                     <div class="quick-btn-title">
                        <a href="{add_user}" title=""><i class="fa fa-plus"></i> New User</a>
                    </div>
                </div>
            </div>
        </div><!-- Heading Sec -->
        <ul class="breadcrumbs">
            <li><a href="javascript:void(0)" title="">Home</a></li>
            <li><a href="javascript:void(0)" title="">Dashboard</a></li>
        </ul>
        <div class="main-content-area">
            <div class="row">
            <div class="col-md-12">
                  <div class="widget">
                      <div class="widget-title">
                          <h3>Status Applicant</h3>
                          <span>Sum of each status</span>
                          <div class="widget-controls">
                              <span class="close-content"><i class="fa fa-trash-o"></i></span>
                              <span class="expand-content"><i class="fa fa-expand"></i></span>
                              <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                          </div><!-- Widget Controls -->
                      </div>
                      <div class="task-graph">
                          <div class="col-md-6" >
                            <div class="task-graph-chart">
                              <span class="pie-colours">4,7,6,5</span>
                              <i>All User Register</i>
                              <h3>{sum_applicant}</h3>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="task-graph-chart">
                              <span class="pie-colours">4,7,6,5</span>
                              <i>All User Not Qualified</i>
                              <h3>{sum_notquali}</h3>
                            </div>
                          </div>
                          <ul>
                             <li>Registered<span>{sum_registered}</span></li>
                             <li>Approve Form I<span>{sum_approve_I}</span></li>
                              <li>Interview I<span>{sum_interview_I}</span></li>
                              <li>Tes Psikotest<span>{sum_psikotest}</span></li>
                              <li>Tes TOEFL<span>{sum_toefl}</span></li>
                              <li>Interview II<span>{sum_interview_II}</span></li>
                              <li>Annoucement<span>{sum_announcement}</span></li>
                              <li>Approve Form II<span>{sum_approve_II}</span></li>
                              <li>Created ID<span>{sum_created_id}</span></li>
                          </ul>
                      </div>
                  </div>
              </div>
              <!-- <div class="col-md-6">
                         <div class="widget">
                              <div class="widget-title">
                                   <h3>Support Tickets</h3>
                                   <span> all tickets here...</span>
                                   <div class="widget-controls">
                                        <span class="close-content"><i class="fa fa-trash-o"></i></span>
                                        <span class="expand-content"><i class="fa fa-expand"></i></span>
                                        <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                   </div><!-- Widget Controls -->
                              </div>
                              <div class="support-ticket-sec">
                                   <ul class="support-ticket" id="ticket-scroll">
                                        <!-- <li>
                                             <div class="tckt-status"><span class="removed red-bg">REMOVED</span><i>23/02/2013</i></div>
                                             <div class="tckt-number"><span>#AA-235</span></div>
                                             <div class="ticket-info">
                                                  <a href="javascript:void(0)" title="">Buy on themeforest this great template...</a>
                                                  <h6>Added by Dmitry Ivaniuk</h6>
                                             </div>
                                        </li> -->
                                        <!-- <li>
                                             <div class="tckt-status"><span class="new green-bg">NEW</span><i>23/02/2013</i></div>
                                             <div class="tckt-number"><span>#AA-457</span></div>
                                             <div class="ticket-info">
                                                  <a href="javascript:void(0)" title="">Goto Shop and Buy Beer</a>
                                                  <h6>Added by John Doe</h6>
                                             </div>
                                        </li>
                                        <li>
                                             <div class="tckt-status"><span class="new green-bg">NEW</span><i>23/02/2013</i></div>
                                             <div class="tckt-number"><span>#AJ-569</span></div>
                                             <div class="ticket-info">
                                                  <a href="javascript:void(0)" title="">There is no error in this Template</a>
                                                  <h6>Added by Dina John</h6>
                                             </div>
                                        </li>
                                        <li>
                                             <div class="tckt-status"><span class="done blue-bg">DONE</span><i>23/02/2013</i></div>
                                             <div class="tckt-number"><span>#JH-784</span></div>
                                             <div class="ticket-info">
                                                  <a href="javascript:void(0)" title="">How can i Buy this Admin?</a>
                                                  <h6>Added by Bolliona</h6>
                                             </div>
                                        </li>
                                        <li>
                                             <div class="tckt-status"><span class="removed red-bg">REMOVED</span><i>23/02/2013</i></div>
                                             <div class="tckt-number"><span>#LD-567</span></div>
                                             <div class="ticket-info">
                                                  <a href="javascript:void(0)" title="">Finest Admin ever of themeforest</a>
                                                  <h6>Added by Nikonilina</h6>
                                             </div>
                                        </li>
                                        <li>
                                             <div class="tckt-status"><span class="removed red-bg">REMOVED</span><i>23/02/2013</i></div>
                                             <div class="tckt-number"><span>#AA-235sadasda</span></div>
                                             <div class="ticket-info">
                                                  <a href="javascript:void(0)" title="">Buy on themeforest this great template...</a>
                                                  <h6>Added by Dmitry Ivaniuk</h6>
                                             </div>
                                        </li>
                                        <li>
                                             <div class="tckt-status"><span class="new green-bg">NEW</span><i>23/02/2013</i></div>
                                             <div class="tckt-number"><span>#AA-457</span></div>
                                             <div class="ticket-info">
                                                  <a href="javascript:void(0)" title="">Goto Shop and Buy Beer</a>
                                                  <h6>Added by John Doe</h6>
                                             </div>
                                        </li>
                                        <li>
                                             <div class="tckt-status"><span class="new green-bg">NEW</span><i>23/02/2013</i></div>
                                             <div class="tckt-number"><span>#AJ-569</span></div>
                                             <div class="ticket-info">
                                                  <a href="javascript:void(0)" title="">There is no error in this Template</a>
                                                  <h6>Added by Dina John</h6>
                                             </div>
                                        </li>
                                        <li>
                                             <div class="tckt-status"><span class="done blue-bg">DONE</span><i>23/02/2013</i></div>
                                             <div class="tckt-number"><span>#JH-784</span></div>
                                             <div class="ticket-info">
                                                  <a href="javascript:void(0)" title="">How can i Buy this Admin?</a>
                                                  <h6>Added by Bolliona</h6>
                                             </div>
                                        </li>
                                        <li>
                                             <div class="tckt-status"><span class="removed red-bg">REMOVED</span><i>23/02/2013</i></div>
                                             <div class="tckt-number"><span>#LD-567</span></div>
                                             <div class="ticket-info">
                                                  <a href="javascript:void(0)" title="">Finest Admin ever of themeforest</a>
                                                  <h6>Added by Nikonilina</h6>
                                             </div>
                                        </li> -->
                                       <!-- <?php if ($applicant == '') { ?>
                                <h4>Applicant register still empty</h4>
                              <?php } else { ?>
                                <?php foreach ($applicant as $app) { ?>
                                   <?php if ($app['status'] == '1') { ?>
                                        <li>
                                             <div class="tckt-status" ><span class="new green-bg">REGISTER</span><i><?php echo $app['create']?></i></div>
                                             <div class="tckt-number"><strong><?php echo '#CALDOS-'.str_pad($app['id_applicant'], 4, '0', STR_PAD_LEFT); ?></strong></div>
                                             <div class="ticket-info" >
                                                  <a href="javascript:void(0)" title="">
                                                  <?php foreach ($user as $u) {
                                                    if ($app['id_user']==$u['id_user']) {
                                                      echo $u['full_name'];
                                                    }
                                                  } ?>
                                                  </a>
                                                  <h6>{vacancy} | <i><?php echo $app['update']?></i> </h6>
                                              </div>

                                        </li>
                                   <?php }elseif ($app['status'] == '2') { ?>
                                        <li>
                                             <div class="tckt-status"><span class="new green-bg">APPR FORM I</span><i><?php echo $app['create']?></i></div>
                                             <div class="tckt-number"><strong><?php echo '#CALDOS-'.str_pad($app['id_applicant'], 4, '0', STR_PAD_LEFT); ?></strong></div>
                                             <div class="ticket-info" >
                                                  <a href="javascript:void(0)" title="">
                                                  <?php foreach ($user as $u) {
                                                    if ($app['id_user']==$u['id_user']) {
                                                      echo $u['full_name'];
                                                    }
                                                  } ?>
                                                  </a>
                                                  <h6>{vacancy} | <i><?php echo $app['update']?></i> </h6>
                                              </div>

                                        </li>
                                   <?php } elseif ($app['status'] == '3') { ?>
                                        <li>
                                             <div class="tckt-status"><span class="new green-bg">INTERVIEW I</span><i><?php echo $app['create']?></i></div>
                                             <div class="tckt-number"><strong><?php echo '#CALDOS-'.str_pad($app['id_applicant'], 4, '0', STR_PAD_LEFT); ?></strong></div>
                                             <div class="ticket-info" >
                                                  <a href="javascript:void(0)" title="">
                                                    <?php foreach ($user as $u) {
                                                    if ($app['id_user']==$u['id_user']) {
                                                      echo $u['full_name'];
                                                    }
                                                  } ?>
                                                  </a>
                                                  <h6>{vacancy} | <i><?php echo $app['update']?></i> </h6>
                                              </div>
                                        </li>
                                   <?php } elseif ($app['status'] == '4') { ?>
                                        <li>
                                             <div class="tckt-status"><span class="new green-bg">TES PSIKOTEST</span><i><?php echo $app['create']?></i></div>
                                             <div class="tckt-number"><strong><?php echo '#CALDOS-'.str_pad($app['id_applicant'], 4, '0', STR_PAD_LEFT); ?></strong></div>
                                             <div class="ticket-info" >
                                                  <a href="javascript:void(0)" title="">
                                                    <?php foreach ($user as $u) {
                                                    if ($app['id_user']==$u['id_user']) {
                                                      echo $u['full_name'];
                                                    }
                                                  } ?>
                                                  </a>
                                                  <h6>{vacancy} | <i><?php echo $app['update']?></i> </h6>
                                              </div>
                                        </li>
                                   <?php } elseif ($app['status'] == '5') {?>
                                        <li>
                                             <div class="tckt-status"><span class="new green-bg">TES TOEFL</span></div>
                                             <div class="tckt-number"><strong><?php echo '#CALDOS-'.str_pad($app['id_applicant'], 4, '0', STR_PAD_LEFT); ?></strong></div>
                                             <div class="ticket-info" >
                                                  <a href="javascript:void(0)" title="">
                                                    <?php foreach ($user as $u) {
                                                    if ($app['id_user']==$u['id_user']) {
                                                      echo $u['full_name'];
                                                    }
                                                  } ?>
                                                  </a>
                                                  <h6>{vacancy} | <i><?php echo $app['update']?></i> </h6>
                                              </div>
                                        </li>
                                   <?php } elseif ($app['status'] == '6') {?>
                                        <li>
                                             <div class="tckt-status"><span class="new green-bg">INTERVIEW II</span></div>
                                             <div class="tckt-number"><strong><?php echo '#CALDOS-'.str_pad($app['id_applicant'], 4, '0', STR_PAD_LEFT); ?></strong></div>
                                             <div class="ticket-info" >
                                                  <a href="javascript:void(0)" title="">
                                                    <?php foreach ($user as $u) {
                                                    if ($app['id_user']==$u['id_user']) {
                                                      echo $u['full_name'];
                                                    }
                                                  } ?>
                                                  </a>
                                                  <h6>{vacancy} | <i><?php echo $app['update']?></i> </h6>
                                              </div>
                                        </li>
                                   <?php } elseif ($app['status'] == '7') {?>
                                        <li>
                                             <div class="tckt-status"><span class="new green-bg">ANNOUCEMENT</span></div>
                                             <div class="tckt-number"><strong><?php echo '#CALDOS-'.str_pad($app['id_applicant'], 4, '0', STR_PAD_LEFT); ?></strong></div>
                                             <div class="ticket-info" >
                                                  <a href="javascript:void(0)" title="">
                                                    <?php foreach ($user as $u) {
                                                    if ($app['id_user']==$u['id_user']) {
                                                      echo $u['full_name'];
                                                    }
                                                  } ?>
                                                  </a>
                                                  <h6>{vacancy} | <i><?php echo $app['update']?></i> </h6>
                                              </div>
                                        </li>
                                   <?php } elseif ($app['status'] == '8') {?>
                                        <li>
                                             <div class="tckt-status"><span class="new green-bg">APPR FORM II</span></div>
                                             <div class="tckt-number"><strong><?php echo '#CALDOS-'.str_pad($app['id_applicant'], 4, '0', STR_PAD_LEFT); ?></strong></div>
                                             <div class="ticket-info" >
                                                  <a href="javascript:void(0)" title="">
                                                    <?php foreach ($user as $u) {
                                                    if ($app['id_user']==$u['id_user']) {
                                                      echo $u['full_name'];
                                                    }
                                                  } ?>
                                                  </a>
                                                  <h6>{vacancy} | <i><?php echo $app['update']?></i> </h6>
                                              </div>

                                        </li>
                                   <?php } elseif ($app['status'] == '9') {?>
                                        <li>
                                              <div class="tckt-status" ><span class="done blue-bg">CREATED ID</span></div>
                                              <div class="tckt-number"><strong><?php echo '#CALDOS-'.str_pad($app['id_applicant'], 4, '0', STR_PAD_LEFT); ?></strong></div>
                                              <div class="ticket-info" >
                                                  <a href="javascript:void(0)" title="">
                                                    <?php foreach ($user as $u) {
                                                    if ($app['id_user']==$u['id_user']) {
                                                      echo $u['full_name'];
                                                    }
                                                  } ?>
                                                  </a>
                                                  <h6>{vacancy} | <i><?php echo $app['update']?></i> </h6>
                                              </div>
                                          </li>
                                   <?php } else { ?>
                                        <li>
                                         <div class="tckt-status"><span class="removed red-bg">NOT QUALIFIED</span><i><?php echo $app['update']?></i></div>
                                         <div class="tckt-number"><strong><?php echo '#CALDOS-'.str_pad($app['id_applicant'], 4, '0', STR_PAD_LEFT); ?></strong></div>
                                         <div class="ticket-info">
                                             <a href="javascript:void(0)" title="">
                                               <?php foreach ($user as $u) {
                                                    if ($app['id_user']==$u['id_user']) {
                                                      echo $u['full_name'];
                                                    }
                                                  } ?>
                                             </a>
                                             <h6>{vacancy}</h6>
                                         </div>
                                     </li>
                                   <?php } ?>
                              <?php } ?>
                              <?php } ?>
                                   </ul>
                              </div>
                         </div>
                    </div> --> 
                    <!-- End Support Ticket -->
            </div>
        </div>
    </div><!-- Panel Content -->
</div>
