<!-- Vendor: Javascripts -->
<script src="<?php echo base_url();?>assets/backend/res/js/jquery-2.1.3.js"></script>
<script src="<?php echo base_url();?>assets/backend/res/js/bootstrap.min.js"></script>

<!-- Our Website Javascripts -->
<script src="<?php echo base_url();?>assets/backend/res/js/app.js"></script>
<script src="<?php echo base_url();?>assets/backend/res/js/common.js"></script>
<script src="<?php echo base_url();?>assets/backend/res/js/widgets.js"></script>
<script src="<?php echo base_url();?>assets/backend/res/js/task-dynamic.js"></script>
<script src="<?php echo base_url();?>assets/backend/res/js/home1.js"></script>


<script src="<?php echo base_url();?>assets/backend/res/js/tabular-table/streaming-mustache.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/backend/res/js/tabular-table/stream_table.js" type="text/javascript"></script>


<script type="text/javascript" src="<?php echo base_url();?>assets/backend/res/js/fresh.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/res/js/text-editor.js"></script>

<!--<script src="<?php echo base_url();?>assets/backend/res/js/tabular-table/stream.js" type="text/javascript"></script>-->
<script src="<?php echo base_url();?>assets/backend/res/js/tabs.js"></script>

<!-- Data table -->
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" charset="utf-8"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js" charset="utf-8"></script>

<!-- datetimepicker -->
<script type="text/javascript" src="<?php echo base_url();?>assets/res/datetimepicker/js/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>

<script type="text/javascript">
$(document).ready(function() {
  $('#applicant-table').DataTable({
    "pageLength": 5 
  });
} );
</script>

<script type="text/javascript">
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
}


if(mm<10) {
    mm='0'+mm
}

today = yyyy+'-'+mm+'-'+dd;

$('.datetimepicker').datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    todayBtn: true,
    autoclose: true,
    startDate: today
});

</script>

<script>
  $(function(){

    // Start Reject
    $(".reject-applicant").click(function(){
      var contents = "";

      contents +='<div class="modal-body">';
      contents +='<center>';
      contents +='<span class="alert-sukses" style="display:none;"><strong>Done!</strong> You successfully reject applicant.</span>';
      contents +='<span class="alert-process" style="display:none;"><strong>Please wait...</strong> Processing data..</span>';
      contents +='<span class="alert-error" style="display:none;"><strong>Sorry!</strong> reject applicant unsuccessful</span>';
      contents +='<span class="alert-infor" style="display:none;"><strong>Sorry!</strong> <span id="mess"></span></span>';
      contents +='</center>';
      contents +='<center>Are you sure for reject <b><span class="name_applicant"></span></b>?</center>';
      contents +='<br /></div>';

      $('.modal-title').html('Notification');
      $('.text-button').html('Reject');
      $('.form-content').html(contents);
      $('#id_applicant').val($(this).data('id'));
      $('.name_applicant').html($(this).data('name'));
      $('#small').modal('show');
    })

    $("#btn-reject-applicant").click(function(){

      $.ajax({
           url : "<?php echo base_url();?>admin/applicant/reject_applicant",
           type: "post",
           data: $("#id_applicant").serialize(),
           dataType:"json",
           beforeSend:function(){
             $(".alert-process").show();
             $(".alert-error").hide();
             $(".alert-sukses").hide();
             console.log($("#id_applicant").serialize());
             console.log("beforeSend");
           },
           success:function(result){
             console.log(result);
             if(result.error == '00'){
                setTimeout(function(){
                  $(".alert-process").hide();
                  $(".alert-error").hide();
                  $(".alert-sukses").show();
                }, 1000);
                setTimeout(function(){
                  window.location = result.redirect;
                }, 2500);
                console.log('in');
                console.log(result.post_data);
             } else if (result.error == '01') {
                setTimeout(function(){
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                }, 2000);
                console.log(result.post_data);
             } else {
                setTimeout(function(){
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                }, 2000);
                console.log('else');
             }

           },
           error: function(xhr, Status, err) {
              setTimeout(function(){
              $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              }, 2000);
              console.log('error');
           }

         });

       return false;
    })

    // End reject


    // Start Approve
    $(".approve-applicant").click(function(){
      $('.modal-title').html('Notification');
      $('.id_applicant').val($(this).data('id'));
      $('#approve-applicant-modal').modal('show');
    })

    $("#btn-approve-applicant").click(function(){
         $.ajax({
           url : "<?php echo base_url();?>admin/applicant/approve_applicant",
           type: "post",
           data: $(".id_applicant").serialize(),
           dataType:"json",
           beforeSend:function(){
             $(".alert-process").show();
              $(".alert-error").hide();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
             console.log('beforeSend');
             console.log($("#id_applicant_form").serialize());
           },
           success:function(result){
             console.log('success');
             if(result.error == '00'){
                setTimeout(function(){
                  $(".alert-process").hide();
              $(".alert-error").hide();
              $(".alert-sukses").show();
              $(".alert-infor").hide();
                }, 1000);
                setTimeout(function(){
                  window.location = result.redirect;
                }, 2500);
                console.log(result.user.status);
                console.log('in');
                console.log(result.post_data);
             } else if (result.error == '01') {
                setTimeout(function(){
                $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
                }, 2000);
                console.log(result.post_data);
             } else {
                setTimeout(function(){
                $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
                }, 2000);
                console.log('else');
             }

           },
           error: function(xhr, Status, err) {
              setTimeout(function(){
              $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
              }, 2000);
              console.log('error');
           }

         });

       return false;
     })
    // End approve


    // Start View applicant
    $(".view-applicant").click(function(){
      // $('.id_applicant').val($(this).data('id'));
      // $('.name').val($(this).data('name'));
      // $('.vacancy_name').val($(this).data('vacancy'));
      // $('.birthdate').val($(this).data('birthdate'));
      // $('.gender').val($(this).data('gender'));
      // $('.marital').val($(this).data('marital'));
      // $('.mothername').val($(this).data('mothername'));
      // $('.address').val($(this).data('address'));
      // $('.idcard').val($(this).data('idcard'));
      // $('.homephone').val($(this).data('homephone'));
      // $('.handphone').val($(this).data('handphone'));
      // $('.email').val($(this).data('email'));
      // $('.role').val($(this).data('role'));
      // $('.status').val($(this).data('status'));
      $("#view-applicant-modal-"+$(this).data('idapplicant')).modal('show');
    })
    // End View applicant

    $("#btn-reject-applicant").click(function(){

      $.ajax({
           url : "<?php echo base_url();?>admin/applicant/reject_applicant",
           type: "post",
           data: $("#id_applicant").serialize(),
           dataType:"json",
           beforeSend:function(){
             $(".alert-process").show();
             $(".alert-error").hide();
             $(".alert-sukses").hide();
             console.log($("#id_applicant").serialize());
             console.log("beforeSend");
           },
           success:function(result){
             console.log(result);
             if(result.error == '00'){
                setTimeout(function(){
                  $(".alert-process").hide();
                  $(".alert-error").hide();
                  $(".alert-sukses").show();
                }, 1000);
                setTimeout(function(){
                  window.location = result.redirect;
                }, 2500);
                console.log('in');
                console.log(result.post_data);
             } else if (result.error == '01') {
                setTimeout(function(){
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                }, 2000);
                console.log(result.post_data);
             } else {
                setTimeout(function(){
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                }, 2000);
                console.log('else');
             }

           },
           error: function(xhr, Status, err) {
              setTimeout(function(){
              $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              }, 2000);
              console.log('error');
           }

         });

       return false;
    })

    // End reject


    // Start Approve
    $(".approve-applicant").click(function(){
      $('.modal-title').html('Notification');
      $('.id_applicant').val($(this).data('id'));
      $('#approve-applicant-modal').modal('show');
    })



    // $("#view-applicant").click(function(){
    //      $.ajax({
    //        url : "<?php echo base_url();?>admin/applicant/view_applicant_mdl",
    //        type: "post",
    //        data: $(".view-applicant").serialize(),
    //        dataType:"json",
    //        beforeSend:function(){
    //          console.log('beforeSend');
    //        },
    //        success:function(result){
    //          console.log('success');
    //
    //          //  Personal data
    //          console.log(result.personal.full_name);
    //
    //          $('.name').val(result.personal.full_name);
    //          $('.vacancy_name').val($(this).data('vacancy'));
    //          $('.birthdate').val(result.personal.birth_date);
    //          $('.gender').val($(this).data('gender'));
    //          $('.marital').val($(this).data('marital'));
    //          $('.mothername').val($(this).data('mothername'));
    //          $('.address').val($(this).data('address'));
    //          $('.idcard').val($(this).data('idcard'));
    //          $('.homephone').val($(this).data('homephone'));
    //          $('.handphone').val($(this).data('handphone'));
    //          $('.email').val($(this).data('email'));
    //          $('.role').val($(this).data('role'));
    //          $('.status').val($(this).data('status'));
    //
    //          $('#view-applicant-modal').modal('show');
    //        },
    //        error: function(xhr, Status, err) {
    //           console.log('error');
    //        }
    //
    //      });
    //
    //    return false;
    //  })

    // Start View user
    $(".view-user").click(function(){
      $('.username').val($(this).data('username'));
      $('.email').val($(this).data('email'));
      $('.roleuser').val($(this).data('rouser'));
      $('.status').val($(this).data('status'));
      $('#view-user-modal').modal('show');
    })
    // End View user

    // Start View user
    $(".download-form").click(function(){
      $('.id_applicant').val($(this).data('id_applicant'));
      $('#download-form-applicant').modal('show');
    })
    // End View user

    // Start edit user
    $(".edit-user").click(function(){
      var rouser = "";
      var role = $(this).data("rouser");
      var status = "";
      var stat = $(this).data('status');

      if (role == 'Admin') {
        rouser += '<option value="1" selected> Admin</option>';
        rouser += '<option value="2"> Applicant</option>';
      } else if (role == 'Super Admin') {
        rouser += '<option value="1"> Admin</option>';
        rouser += '<option value="2"> Applicant</option>';
        rouser += '<option value="3" selected> Super Admin</option>';
      } else if (role == 'Applicant') {
        rouser += '<option value="1"> Admin</option>';
        rouser += '<option value="2" selected> Applicant</option>';
      };


      if (stat == 'Enable') {
        status +='<option value="1" selected> Enable</option>';
        status +='<option value="2"> Disable</option>';
      } else {
        status +='<option value="1"> Enable</option>';
        status +='<option value="2" selected> Disable</option>';
      }

      $('.username').val($(this).data('username'));
      $('.email').val($(this).data('email'));
      $('.role').append(rouser);
      $('.status').append(status);
      $('#id_user_update').val($(this).data('iduser'));
      console.log($(this).data('rouser'));
      $('#edit-user-modal').modal('show');
    })
    // End edit user

    // Start edit user
    $(".edit-user2").click(function(){
      var rouser = "";
      var role = $(this).data("rouser2");
      var status = "";
      var stat = $(this).data('status2');

      if (role == 'Admin') {
        rouser += '<option value="1" selected> Admin</option>';
        rouser += '<option value="2"> Applicant</option>';
      } else if (role == 'Super Admin') {
        rouser += '<option value="1"> Admin</option>';
        rouser += '<option value="2"> Applicant</option>';
        rouser += '<option value="3" selected> Super Admin</option>';
      } else if (role == 'Applicant') {
        rouser += '<option value="1"> Admin</option>';
        rouser += '<option value="2" selected> Applicant</option>';
      };


      if (stat == 'Enable') {
        status +='<option value="1" selected> Enable</option>';
        status +='<option value="2"> Disable</option>';
      } else {
        status +='<option value="1"> Enable</option>';
        status +='<option value="2" selected> Disable</option>';
      }

      $('.username').val($(this).data('username'));
      $('.email').val($(this).data('email'));
      $('.role').append(rouser);
      $('.status').append(status);
      $('#id_user_update').val($(this).data('iduser'));
      console.log($(this).data('rouser'));
      $('#edit-user2-modal').modal('show');
    })
    // End edit user

    // Update user
    $(".btn-user-update").click(function(){
         $.ajax({
           url : "<?php echo base_url();?>admin/user/save_user",
           type: "post",
           data: $(".form-edit-user").serialize(),
           dataType:"json",
           beforeSend:function(){
             $(".alert-process").show();
              $(".alert-error").hide();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
             console.log('beforeSend');
             console.log($(".form-edit-user").serialize());
           },
           success:function(result){
             console.log(result.post);
             if(result.error == '00'){
                setTimeout(function(){
                  $(".alert-process").hide();
              $(".alert-error").hide();
              $(".alert-sukses").show();
              $(".alert-infor").hide();
                }, 1000);
                setTimeout(function(){
                  window.location = result.redirect;
                }, 2500);
                console.log(result.user.status);
                console.log('in');
                console.log(result.post_data);
             } else if (result.error == '01') {
                setTimeout(function(){
                $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
                }, 2000);
                console.log(result.post_data);
             } else {
                setTimeout(function(){
                $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
                }, 2000);
                console.log('else');
             }

           },
           error: function(xhr, Status, err) {
              setTimeout(function(){
              $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
              }, 2000);
              console.log('error');
           }

         });

       return false;
     })
    // End approve

    $("#vacancy").click(function(){

         $.ajax({
           url : "<?php echo base_url();?>admin/vacancy/save_vacancy",
           type: "post",
           data: $(".vacancy-form").serialize(),
           dataType:"json",
           beforeSend:function(){
             $(".alert-process").show();
             $(".alert-error").hide();
             $(".alert-sukses").hide();
             $(".alert-infor").hide();
             console.log($(".vacancy-form").serialize());
             console.log($("#description").val());
             console.log("beforeSend");
           },
           success:function(result){
             console.log(result);
             if(result.error == '00'){
               	setTimeout(function(){
                  $(".alert-process").hide();
                  $(".alert-error").hide();
                  $(".alert-sukses").show();
                  $(".alert-infor").hide();
                }, 1000);
                setTimeout(function(){
                  window.location = result.redirect;
                }, 4000);
               	console.log(result.post_data);
             } else if (result.error == '01') {
                setTimeout(function(){
               	$(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
                console.log(result.post_data);
             } else if (result.error == '02') {
                setTimeout(function(){
                  $(".alert-process").hide();
                  $(".alert-error").hide();
                  $(".alert-sukses").hide();
                  document.getElementById('mess').innerHTML = result.mess;
                  $(".alert-infor").show();
                }, 2000);
             } else {
                setTimeout(function(){
               	$(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
                console.log('else');
             }

           },
           error: function(xhr, Status, err) {
              setTimeout(function(){
            	$(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              }, 2000);
              console.log('error');
           }

         });

       return false;
     })

    // Start View user
    $(".delete-vacancy").click(function(){
      $('.id_vacancy_delete').val($(this).data('id'));
      $('#delete-vacancy-modal').modal('show');
    })
    // End View user

    $("#update_vacancy").click(function(){

         $.ajax({
           url : "<?php echo base_url();?>admin/vacancy/save_vacancy",
           type: "post",
           data: $(".edit-form-vacancy").serialize(),
           dataType:"json",
           beforeSend:function(){
             $(".alert-process").show();
             $(".alert-error").hide();
             $(".alert-sukses").hide();
             $(".alert-infor").hide();
             console.log($(".tab-content-vacancy").serialize());
             console.log($("#description").val());
             console.log("beforeSend");
             console.log($(".edit-form-vacancy").serialize());
           },
           success:function(result){
             console.log(result);
             if(result.error == '00'){
                setTimeout(function(){
                  $(".alert-process").hide();
                  $(".alert-error").hide();
                  $(".alert-sukses").show();
                  $(".alert-infor").hide();
                }, 1000);
                setTimeout(function(){
                  window.location = result.redirect;
                  //console.log(result);
                }, 2500);
                console.log(result);
                console.log(result.post);
             } else if (result.error == '01') {
                setTimeout(function(){
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
                console.log(result.post_data);
             } else if (result.error == '02') {
                setTimeout(function(){
                  $(".alert-process").hide();
                  $(".alert-error").hide();
                  $(".alert-sukses").hide();
                  document.getElementsByClassName('mess')[0].innerHTML = result.mess;
                  $(".alert-infor").show();
                  console.log('filled');
                }, 2000);
             }else {
                setTimeout(function(){
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
                console.log('else');
             }

           },
           error: function(xhr, Status, err) {
              setTimeout(function(){
              $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
              }, 2000);
              console.log('error');
           }

         });

       return false;
     })

    $("#btn-delete-vacancy").click(function(){

         $.ajax({
           url : "<?php echo base_url();?>admin/vacancy/delete_vacancy",
           type: "post",
           data: $(".id_vacancy_delete").serialize(),
           dataType:"json",
           beforeSend:function(){
             $(".alert-process").show();
             $(".alert-error").hide();
             $(".alert-sukses").hide();
             console.log($(".tab-content-vacancy").serialize());
             console.log($("#description").val());
             console.log("beforeSend");
             console.log($(".id_vacancy_delete").serialize());
           },
           success:function(result){
             console.log(result);
             if(result.error == '00'){
                setTimeout(function(){
                  $(".alert-process").hide();
                  $(".alert-error").hide();
                  $(".alert-sukses").show();
                }, 1000);
                setTimeout(function(){
                  window.location = result.redirect;
                }, 2500);
                console.log(result.post_data);
             } else if (result.error == '01') {
                setTimeout(function(){
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                }, 2000);
                console.log(result.post_data);
             } else {
                setTimeout(function(){
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                }, 2000);
                console.log('else');
             }

           },
           error: function(xhr, Status, err) {
              setTimeout(function(){
              $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              }, 2000);
              console.log('error');
           }

         });

       return false;
     })

    // Start View user
    $(".edit-vacancy").click(function(){
      $('.id_vacancy_edit').val($(this).data('id'));
      $('.idfakultas').val($(this).data('fakultas'));
      $('.idprodi').val($(this).data('prodi'));
      $('.job').val($(this).data('title'));
      $('.vacancy-description').val($(this).data('desc-vacancy'));
      $('.deadline').val($(this).data('deadline'));
      $('.interview_schedule_1').val($(this).data('inter-schedule'));
      $('.interview_description_1').val($(this).data('inter-desc'));
      $('.psikotes_schedule').val($(this).data('psiko-schedule'));
      $('.psikotes_description').val($(this).data('psiko-desc'));
      $('.toefl_schedule').val($(this).data('toefl-schedule'));
      $('.toefl_description').val($(this).data('toefl-desc'));
      $('.interview_schedule_2').val($(this).data('inter-schedulep'));
      $('.interview_description_2').val($(this).data('inter-descp'));

      // console.log($(this).data('inter-scheduleII'));

      loadFakultasEdit('.fakultas-edit');
      loadProdiEdit('.prodi-edit');
      $('.fakultas-edit').change(function(){
        $('.prodi-edit').show();
        var idfakultas = $('.fakultas-edit').val();
        loadProdi(idfakultas,'.prodi-edit');
      });

      function loadFakultasEdit(id){
      //var idfakultas = document.getElementById('idfakultas');
      $(id).html('loading...');
      $.ajax({
          url:'<?php echo base_url("admin/getFakultas")?>',
          dataType:'json',
          //type: 'post',
          //data: {idfakultas:val($(this).data('idfakultas'))},
          beforeSend:function(){
              $(id).append('<option value="">Loading . . .</option>');
          },
          success:function(response){
            $(id).html('');
            fakultas = '';
              $.each(response['mess'], function(i,n){
                if (n['IDFakultas'] == $('.idfakultas').val()) {
                  fakultas = '<option value="'+n['IDFakultas']+'" selected="selected">'+n['NamaFakultas']+'</option>';
                } else {
                  fakultas = '<option value="'+n['IDFakultas']+'">'+n['NamaFakultas']+'</option>';
                };
                fakultas = fakultas + '';
                $(id).append(fakultas);
              });
              //$(id).prepend('<option value="" selected="selected">Silahkan Pilih Fakultas</option>');

          },
          error:function(){
            $(id).html('ERROR');
          }
        });
      }

      function loadProdiEdit(id){
      $.ajax({
          url:'<?php echo base_url("admin/getProdi")?>',
          dataType:'json',
          data:{idfakultas:$('.idfakultas').val()},
          success:function(response){
            $(id).html('');
              prodi = '';
                $.each(response['mess'], function(i,n){
                  if (n['IDProgdi'] == $('.idprodi').val()) {
                    prodi = '<option value="'+n['IDProgdi']+'" selected="selected">'+n['NamaProgdi']+'</option>';
                  } else {
                    prodi = '<option value="'+n['IDProgdi']+'">'+n['NamaProgdi']+'</option>';
                  };
                  prodi = prodi + '';
                  $(id).append(prodi);
                });
          },
          error:function(){
            $(id).html('ERROR');
          }
        });
      }

      function loadProdi(idfakultas,id){
        $.ajax({
          url:'<?php echo base_url("admin/getProdi")?>',
          dataType:'json',
          data:{idfakultas:idfakultas},
          beforeSend:function(){
            setTimeout(function(){
              $(id).html('<option value="">Loading . . .</option>');
            }, 100);
          },
          success:function(response){
            setTimeout(function(){
              $(id).html('');
              prodi = '';
                $.each(response['mess'], function(i,n){
                  prodi = '<option value="'+n['IDProgdi']+'">'+n['NamaProgdi']+'</option>';
                  prodi = prodi + '';
                  $(id).append(prodi);
                });
                $(id).prepend('<option value="" selected="selected">Silahkan Pilih Program Studi</option>');
            }, 2500);
          },
          error:function(){
            $(id).html('ERROR');
          }
        });
      }

      $('#edit-vacancy-modal').modal('show');
    })
    // End View user



    $("#user").click(function(){

         $.ajax({
           url : "<?php echo base_url();?>admin/user/save_user",
           type: "post",
           data: $(".user-form").serialize(),
           dataType:"json",
           beforeSend:function(){
             $(".alert-process").show();
             $(".alert-error").hide();
             $(".alert-sukses").hide();
             $(".alert-infor").hide();
             console.log($(".user-form").serialize());
             console.log("beforeSend");
           },
           success:function(result){
             console.log(result);
             if(result.error == '00'){
                setTimeout(function(){
                  $(".alert-process").hide();
                  $(".alert-error").hide();
                  $(".alert-sukses").show();
                  $(".alert-infor").hide();
                }, 2000);
                setTimeout(function(){
                  window.location = result.redirect;
                }, 1000);
                console.log(result.post_data);
             } else if (result.error == '01') {
                setTimeout(function(){
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
                console.log(result.post_data);
             } else if (result.error == '02') {
                setTimeout(function(){
                  $(".alert-process").hide();
                  $(".alert-error").hide();
                  $(".alert-sukses").hide();
                  document.getElementById('mess').innerHTML = result.mess;
                  $(".alert-infor").show();
                }, 2000);
             } else {
                setTimeout(function(){
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
                console.log('else');
             }

           },
           error: function(xhr, Status, err) {
              setTimeout(function(){
              $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
              }, 2000);
              console.log('error');
           }

         });

       return false;
     })


      $(".pass_interview_1_btn").click(function(){
         $.ajax({
           url : "<?php echo base_url();?>admin/vacancy/pass_test",
           type: "post",
           data: $(".pass-interview-1").serialize(),
           dataType:"json",
           beforeSend:function(){
              $(".alert-process").show();
              $(".alert-error").hide();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
             console.log('beforeSend');
             console.log($(".pass-interview-id").serialize());
           },
           success:function(result){
             console.log(result.post);
             if(result.error == '00'){
                setTimeout(function(){
                  $(".alert-process").hide();
                  $(".alert-error").hide();
                  $(".alert-sukses").show();
                  $(".alert-infor").hide();
                }, 1000);
                setTimeout(function(){
                  window.location.reload('false');
                }, 2500);
                console.log(result.user.status);
                console.log('in');
                console.log(result.post_data);
             } else if (result.error == '01') {
                setTimeout(function(){
                document.getElementById('mess').innerHTML = result.mess;
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
                console.log(result.post_data);
             } else {
                setTimeout(function(){
                  document.getElementById('mess').innerHTML = result.mess;
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
                console.log('else');
             }

           },
           error: function(xhr, Status, err) {
              setTimeout(function(){
                document.getElementById('mess').innerHTML = result.mess;
              $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
              }, 2000);
              console.log('error');
           }

         });

       return false;
     })
    // End approve

    $(".pass_psikotest_btn").click(function(){
         $.ajax({
           url : "<?php echo base_url();?>admin/vacancy/pass_test",
           type: "post",
           data: $(".pass-psikotest").serialize(),
           dataType:"json",
           beforeSend:function(){
              $(".alert-process").show();
              $(".alert-error").hide();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
             console.log('beforeSend');
             console.log($(".pass-psikotest").serialize());
           },
           success:function(result){
             console.log(result.post);
             if(result.error == '00'){
                setTimeout(function(){
                  $(".alert-process").hide();
                  $(".alert-error").hide();
                  $(".alert-sukses").show();
                  $(".alert-infor").hide();
                }, 1000);
                setTimeout(function(){
                  window.location.reload('false');
                }, 2500);
                console.log(result.user.status);
                console.log('in');
                console.log(result.post_data);
             } else if (result.error == '01') {
                setTimeout(function(){
                document.getElementById('mess').innerHTML = result.mess;
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
                console.log(result.post_data);
             } else {
                setTimeout(function(){
                  document.getElementById('mess').innerHTML = result.mess;
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
                console.log('else');
             }

           },
           error: function(xhr, Status, err) {
              setTimeout(function(){
                document.getElementById('mess').innerHTML = result.mess;
              $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
              }, 2000);
              console.log('error');
           }

         });

       return false;
     })
    // End approve

    $(".pass_toefl_btn").click(function(){
         $.ajax({
           url : "<?php echo base_url();?>admin/vacancy/pass_test",
           type: "post",
           data: $(".pass-toefl").serialize(),
           dataType:"json",
           beforeSend:function(){
              $(".alert-process").show();
              $(".alert-error").hide();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
             console.log('beforeSend');
             console.log($(".pass-toefl").serialize());
           },
           success:function(result){
             console.log(result);
             if(result.error == '00'){
                setTimeout(function(){
                  $(".alert-process").hide();
                  $(".alert-error").hide();
                  $(".alert-sukses").show();
                  $(".alert-infor").hide();
                }, 1000);
                setTimeout(function(){
                  window.location.reload('false');
                }, 2500);
                console.log(result.user.status);
                console.log('in');
                console.log(result.post_data);
             } else if (result.error == '01') {
                setTimeout(function(){
                  document.getElementById('mess').innerHTML = result.mess;
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
                console.log(result.post_data);
             } else {
                setTimeout(function(){
                  document.getElementById('mess').innerHTML = result.mess;
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
                console.log('else');
             }

           },
           error: function(xhr, Status, err) {
              setTimeout(function(){
                document.getElementById('mess').innerHTML = result.mess;
              $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
              }, 2000);
              console.log('error');
           }

         });

       return false;
     })
    // End approve

    $(".pass_interview_2_btn").click(function(){
         $.ajax({
           url : "<?php echo base_url();?>admin/vacancy/pass_test",
           type: "post",
           data: $(".pass-interview-2").serialize(),
           dataType:"json",
           beforeSend:function(){
              $(".alert-process").show();
              $(".alert-error").hide();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
             console.log('beforeSend');
             console.log($(".pass-interview-2").serialize());
           },
           success:function(result){
             console.log(result);
             if(result.error == '00'){
                setTimeout(function(){
                  $(".alert-process").hide();
                  $(".alert-error").hide();
                  $(".alert-sukses").show();
                  $(".alert-infor").hide();
                }, 1000);
                setTimeout(function(){
                  window.location.reload('false');
                }, 2500);
                console.log(result.user.status);
                console.log('in');
                console.log(result.post_data);
             } else if (result.error == '01') {
                setTimeout(function(){
                  document.getElementById('mess').innerHTML = result.mess;
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
                console.log(result.post_data);
             } else {
                setTimeout(function(){
                  document.getElementById('mess').innerHTML = result.mess;
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
                console.log('else');
             }

           },
           error: function(xhr, Status, err) {
              setTimeout(function(){
                document.getElementById('mess').innerHTML = result.mess;
              $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
              }, 2000);
              console.log('error');
           }

         });

       return false;
     })
    // End approve

    // $(".delete-user2").click(function(){
    //      $.ajax({
    //        url : "<?php echo base_url();?>admin/user/delete_user",
    //        type: "post",
    //        data: $(".iduser").serialize(),
    //        dataType:"json",
    //        beforeSend:function(){
    //           $(".alert-process").show();
    //           $(".alert-error").hide();
    //           $(".alert-sukses").hide();
    //           $(".alert-infor").hide();
    //          console.log('beforeSend');
    //        },
    //        success:function(result){
    //          console.log(result);
    //          if(result.error == '00'){
    //             setTimeout(function(){
    //               $(".alert-process").hide();
    //               $(".alert-error").hide();
    //               $(".alert-sukses").show();
    //               $(".alert-infor").hide();
    //             }, 1000);
    //             setTimeout(function(){
    //               window.location.reload('false');
    //             }, 2500);
    //             console.log('in');
    //             console.log(result);
    //          } else if (result.error == '01') {
    //             console.log(result);
    //          } else {
    //             console.log('else');
    //          }
    //
    //        },
    //        error: function(xhr, Status, err) {
    //           console.log('error');
    //        }
    //
    //      });
    //
    //    return false;
    //  })

    $(".create-id-btn").click(function(){
         $.ajax({
           url : "<?php echo base_url();?>admin/vacancy/pass_test",
           type: "post",
           data: $(".create-id").serialize(),
           dataType:"json",
           beforeSend:function(){
              $(".alert-process").show();
              $(".alert-error").hide();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
           },
           success:function(result){
             if(result.respon == '00'){
                setTimeout(function(){
                  $(".alert-process").hide();
                  $(".alert-error").hide();
                  $(".alert-sukses").show();
                  $(".alert-infor").hide();
                }, 1000);
                setTimeout(function(){
                  location.reload();
                }, 2500);
             } else if (result.respon == '01') {
                setTimeout(function(){
                  document.getElementById('mess').innerHTML = result.mess;
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
             } else {
                setTimeout(function(){
                  document.getElementById('mess').innerHTML = result.mess;
                $(".alert-process").hide();
                $(".alert-error").show();
                $(".alert-sukses").hide();
                $(".alert-infor").hide();
                }, 2000);
             }

           },
           error: function(xhr, Status, err) {
              setTimeout(function(){
                document.getElementById('mess').innerHTML = result.mess;
              $(".alert-process").hide();
              $(".alert-error").show();
              $(".alert-sukses").hide();
              $(".alert-infor").hide();
              }, 2000);
           }

         });

       return false;
     })

    $("#forgot-password-btn").click(function(){
         $.ajax({
           url : "<?php echo base_url();?>index/forgot_password",
           type: "post",
           data: $(".forgot-password-form").serialize(),
           dataType:"json",
           success:function(result){
             console.log(result);
             if(result.error == '00'){
                alert('Sukses mengirim email, silahkan cek email Anda');
             } else {
                alert('Gagal Mengirim Password');
             }

           },
           error: function(xhr, Status, err) {

           }

         });

       return false;
     })
    // End approve
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
      loadFakultas('.fakultas');
      $('.fakultas').change(function(){
        $('.prodi').show();
        var idfakultas = $('.fakultas').val();
        loadProdi(idfakultas,'.prodi')
      });
    });

    function loadFakultas(id){
      $(id).html('loading...');
      $.ajax({
        url:'<?php echo base_url("admin/getFakultas")?>',
        dataType:'json',
        beforeSend:function(){
            $(id).append('<option value="">Loading . . .</option>');
        },
        success:function(response){

          $(id).html('');
          fakultas = '';
            $.each(response['mess'], function(i,n){
              fakultas = '<option value="'+n['IDFakultas']+'">'+n['NamaFakultas']+'</option>';
              fakultas = fakultas + '';
              $(id).append(fakultas);
            });
            $(id).prepend('<option value="" selected="selected">Silahkan Pilih Fakultas</option>');

        },
        error:function(){
          $(id).html('ERROR');
        }
      });
    }

    function loadProdi(idfakultas,id){
      $.ajax({
        url:'<?php echo base_url("admin/getProdi")?>',
        dataType:'json',
        data:{idfakultas:idfakultas},
        beforeSend:function(){
          setTimeout(function(){
            $(id).html('<option value="">Loading . . .</option>');
          }, 100);
        },
        success:function(response){
          setTimeout(function(){
            $(id).html('');
            prodi = '';
              $.each(response['mess'], function(i,n){
                prodi = '<option value="'+n['IDProgdi']+'">'+n['NamaProgdi']+'</option>';
                prodi = prodi + '';
                $(id).append(prodi);
              });
              $(id).prepend('<option value="" selected="selected">Silahkan Pilih Program Studi</option>');
          }, 2500);
        },
        error:function(){
          $(id).html('ERROR');
        }
      });
    }
</script>

</body>
</html>
