<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Our Website Content Goes Here -->
<header class="simple-normal">
     <div class="top-bar">
          <div class="logo">
               <a href="<?php echo base_url();?>admin" title=""><i class="fa fa-bullseye"></i> Administrator</a>
          </div>
          <div class="menu-options"><span class="menu-action"><i></i></span></div>

          <div class="top-bar-quick-sec">
               <span id="toolFullScreen" class="full-screen-btn"><i class="fa fa-arrows-alt"></i></span>
               <div class="name-area">
                    <ul class="quick-notify-section custom-dropdowns">
                    <li class="activity-list dropdown">
                         <span><i class="fa fa-user"></i><strong class="blue-bg text-capitalize"><i class="fa fa-cog"></i></strong></span>
                          <div class="activity drop-list">
                              <span>Profile Settings</span>
                              <ul>
                                   <li>
                                       <a title="" data-iduser="{id}" data-username="{username}" data-email="{email}" data-rouser="{role}" data-status="{status}" data-toggle="modal" class="edit-user" ><span><img src="http://placehold.it/40x40" alt="" /></span><i>My Account</i>{username}<h6>Configure your profile account</h6></a>
                                   </li>
                                   <li>
                                        <a href="{list_user}" title=""><span><img src="http://placehold.it/40x40" alt="" /></span><i>Administrator</i>{username}<h6>View list of administrator</h6></a>
                                   </li>
                              </ul>
                              <a href="{logout}" title="">Logout</a>
                         </div><!-- Activity Drop list -->
                    </li>
               </ul>
               </div>
          </div>
     </div><!-- Top Bar -->
     <div class="side-menu-sec" id="header-scroll">
          <div class="side-menus">
               <span></span>
               <span>MAIN LINKS</span>
               <nav>
                    <ul class="parent-menu">
                         <li>
                              <a href="<?php echo base_url();?>admin"><i class="ti-desktop"></i><span>Dashboard</span></a>
                         </li>
                         <li >
                              <a href="{list_applicant}"><i class="ti-user"></i><span>Applicant</span></a>
                         </li>
                         <li >
                              <a href="{list_vacancy}"><i class="ti-thought"></i><span>Vacancy</span></a>
                         </li>
                         <li >
                              <a href="{list_user}"><i class="ti-panel"></i><span>Administrator</span></a>
                         </li>
                    </ul>
               </nav>
                <span class="footer-line">Developed by <a href="https://skyesprojects.net" target="_blank">Skyes Projects</a></span>
          </div>
     </div>
</header>
<!-- Edit modal -->
<div class="modal fade" id="edit-user-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">My Account</h4>
      </div>
      <div class="modal-body">
        <center>
          <span class="alert-sukses" style="display:none;"><strong>Done!</strong> You successfully save update user.</span>
          <span class="alert-process" style="display:none;"><strong>Please wait...</strong> Processing data..</span>
          <span class="alert-error" style="display:none;"><strong>Sorry!</strong> update user is unsuccessful, please check your data and submit again.</span>
          <span class="alert-infor" style="display:none;"><strong>Sorry!</strong> <span class="mess"></span></span>
         </center>
        <form class="user-form">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                       <label for="username">Username</label>
                       <input type="text" placeholder="Username" id="username" name="username" class="form-control username form-edit-user" value="" required="required">
                       <input type="hidden" id="id_user_update" name="id_user_update" class="form-control form-edit-user" value="">
                       <input type="hidden" id="action" name="action" class="form-control form-edit-user" value="update">
                    </div>
                    <div class="form-group">
                       <label for="password">Password</label>
                       <input type="password" placeholder="Password" id="password" name="password" class="form-control password form-edit-user" value="" required="required">
                    </div>
                    <div class="form-group">
                       <label for="email">Email</label>
                       <input type="email" placeholder="Email" id="email" name="email" class="form-control email form-edit-user" value="" >
                    </div>
                    <div class="form-group">
                       <label for="role">Role</label>
                       <select name="role" class="form-control role form-edit-user"></select>
                    </div>
                    <div class="form-group">
                       <label for="status">Status</label>
                       <select name="status" class="form-control status form-edit-user"></select>
                    </div>
              </div>

            </div>
            <br />
            <button class="btn green-bg btn-block btn-user-update" type="submit">Save</button>
            </form>
      </div>
    </div>
  </div>
</div>
