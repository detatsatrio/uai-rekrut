

<!-- Data Penelitian -->
<div class="modal-header">
  <h4 class="modal-title">Research Data</h4>
</div>
<div class="modal-body">
  <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <h5 class="modal-title">#Research-<?php echo $i;?></h5>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
           <label for="title" readonly>Judul Penelitian</label>
           <input type="text" placeholder="Judul Penelitian" class="form-control title-<?php echo $i?>" value="<?php echo $info['title'];?>" readonly>
        </div>
        <div class="form-group">
           <label for="category" readonly>Kategori Penelitian</label>
           <input type="text" placeholder="Kategori Penelitian" class="form-control category-<?php echo $i?>" value="<?php echo $info['category'];?>" readonly>
        </div>
        <div class="form-group">
           <label for="publikasi" readonly>Nama Publikasi</label>
           <input type="text" placeholder="Nama Publikasi" class="form-control publikasi-<?php echo $i?>" value="<?php echo $info['publikasi'];?>" readonly>
        </div>
        <div class="form-group">
           <label for="category" readonly>Nama Peneliti</label>
           <input type="text" placeholder="Nama Peneliti" class="form-control name-<?php echo $i?>" value="<?php echo $info['name'];?>" readonly>
        </div>
        <div class="form-group">
           <label for="year" readonly>Tahun Penelitian</label>
           <input type="text" placeholder="Tahun Penelitian" class="form-control year-<?php echo $i?>" value="<?php echo $info['tahun'];?>" readonly>
        </div>
      </div>

      <div class="form-group">
        <hr />
      </div>
  </div>
</div>
<!-- End Data Penelitian -->

<!-- Data Mengajar -->
<div class="modal-header">
  <h4 class="modal-title">Lecture Data</h4>
</div>
<div class="modal-body">
  <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <h5 class="modal-title">#Lecturer-<?php echo $i;?></h5>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
           <label for="lecture" readonly>Mata Kuliah</label>
           <input type="text" placeholder="Mata Kuliah" class="form-control lecture-<?php echo $i?>" value="<?php echo $info['lecture'];?>" readonly>
        </div>
        <div class="form-group">
           <label for="jenjang" readonly>Jenjang</label>
           <input type="text" placeholder="Jenjang" class="form-control jenjang-<?php echo $i?>" value="<?php echo $info['jenjang'];?>" readonly>
        </div>
        <div class="col-md-6">
          <div class="form-group">
             <label for="semester" readonly>Semester</label>
             <input type="text" placeholder="Semester" class="form-control semester-<?php echo $i?>" value="<?php echo $info['semester'];?>" readonly>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
             <label for="year" readonly>Tahun</label>
             <input type="text" placeholder="Tahun" class="form-control year-<?php echo $i?>" value="<?php echo $info['tahun'];?>" readonly>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
           <label for="jurusan" readonly>Jurusan/Prodi</label>
           <input type="text" placeholder="Jurusan/Prodi" class="form-control jurusan-<?php echo $i?>" value="<?php echo $info['prodi'];?>" readonly>
        </div>
        <div class="form-group">
           <label for="fakultas" readonly>Fakultas</label>
           <input type="text" placeholder="Fakultas" class="form-control fakultas-<?php echo $i?>" value="<?php echo $info['fakultas'];?>" readonly>
        </div>
        <div class="form-group">
           <label for="perguruan_tinggi" readonly>Perguruan Tinggi</label>
           <input type="text" placeholder="Perguruan Tinggi" class="form-control perguruan-tinggi-<?php echo $i?>" value="<?php echo $info['perguruan_tinggi'];?>" readonly>
        </div>
      </div>

      <div class="form-group">
        <hr />
      </div>
  </div>
</div>
<!-- End Data Mengajar -->
