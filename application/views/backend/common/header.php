<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>{title}</title>
    <meta charset="utf-8">
    <meta name="description" content="Glade is a clean and powerful ready to use responsive AngularJs Admin Template based on Latest Bootstrap version and powered by jQuery, Glade comes with 3 amazing Dashboard layouts. Glade is completely flexible and user friendly admin template as it supports all the browsers and looks awesome on any device.">
    <meta name="keywords" content="admin, admin dashboard, angular admin, bootstrap admin, dashboard, modern admin, responsive admin, web admin, web app, bitlers">
    <meta name="author" content="bitlers">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backend/res/css/bootstrap.min.css">

    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backend/res/css/icons.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backend/res/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backend/res/css/responsive.css">

    <!-- datetimepicker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/res/datetimepicker/css/bootstrap-datetimepicker.min.css">
    <!-- data tables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
    
    <!-- Tinymce -->
    <script src="<?php echo base_url();?>assets/res/tinymce/tinymce.min.js"></script>
    <script>
        tinymce.init({selector:'.textarea'});
    </script>
</head>
