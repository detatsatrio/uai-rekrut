<div class="main-content">
    <div class="panel-content">
        <div class="main-title-sec">
            <div class="row">
                <div class="col-md-3 column">
                    <div class="heading-profile">
                        <h2>List Applicant</h2>
                        <span></span>
                    </div>
                </div>
            </div>
        </div><!-- Heading Sec -->
        <ul class="breadcrumbs">
            <li><a href="<?php echo base_url();?>admin" title="">Dashboard</a></li>
            <li><a href="" title="">List Applicant</a></li>
        </ul>
        <div class="main-content-area">
            <div class="row">
                <div class="col-md-12">
                         <div class="streaming-table">
                                   <span id="found" class="label label-info"></span>
                                   <table id="applicant-table" class='table table-striped table-bordered'>
                                     <thead>
                                        <tr>
                                          <th>ID Applicant</th>
                                          <th>Name</th>
                                          <th>Vacancy</th>
                                          <th>CV</th>
                                          <th>Identity (Docs)</th>
                                          <th>Ijazah Asli (Docs)</th>
                                          <th>Optional File (Docs)</th>
                                          <th>Completed Form (Docs)</th>
                                          <th>Status</th>
                                          <th width="15px">Action</th>
                                        </tr>
                                     </thead>
                                     <tbody>
                                        <?php if(isset($applicant)=='') { ?>
                                          <h4>Applicant is empty</h4>
                                        <?php } else { ?>
                                          <?php foreach ($applicant as $app) { ?>
                                            <tr>
                                                  <td><?php echo 'CALDOS - '.str_pad($app['id_applicant'], 4, '0', STR_PAD_LEFT)?></td>
                                                <td>
                                                  <?php foreach ($user as $u) {
                                                    if ($app['id_user']==$u['id_user']) {
                                                      $fullname = $u['full_name'];
                                                      echo $fullname;
                                                    }
                                                  } ?>
                                                </td>
                                                <td>
                                                <?php foreach ($vacancy as $va) {
                                                  if ($va['id_vacancy'] == $app['vacancy']) {
                                                    $job = $va['title_vacancy'];
                                                    echo $job;
                                                  }
                                                }
                                                ?></td>
                                                <td>
                                                  <a href="#">Download CV</a>
                                                </td>
                                                <td>
                                                  <?php foreach ($docs as $photo) {
                                                    if ($photo['applicant'] == $app['id_user']) {
                                                      if ($photo['id_pict'] !== '') { ?>
                                                        <a href="<?php echo site_url('admin/download_file/id/'.$photo['id_pict']) ?>">Download Photo</a><br /><br />
                                                      <?php }

                                                    }

                                                  }?>
                                                  <?php foreach ($docs as $id_card) {
                                                    if ($id_card['applicant'] == $app['id_user']) {
                                                      if ($id_card['id_card_pict']) { ?>
                                                        <a href="<?php echo site_url('admin/download_file/id_card/'.$id_card['id_card_pict']) ?>">Download ID Card</a><br /><br />
                                                      <?php }
                                                    }
                                                  }?>
                                                </td>
                                                <td>
                                                <?php foreach ($docs as $ijazah_s1) {
                                                    if ($ijazah_s1['applicant'] == $app['id_user']) {
                                                      if ($ijazah_s1['cert_bachelor']) { ?>
                                                        <a href="<?php echo site_url('admin/download_file/bachelor/'.$ijazah_s1['cert_bachelor']) ?>">Download Ijazah S1</a><br />
                                                      <?php }
                                                    }
                                                  }?>

                                                <?php foreach ($docs as $ijazah_s2) {
                                                    if ($ijazah_s2['applicant'] == $app['id_user']) {
                                                      if (isset($ijazah_s2['cert_master'])) { ?>
                                                       <a href="<?php echo site_url('admin/download_file/master/'.$ijazah_s2['cert_master']) ?>">Download Ijazah S2</a><br />
                                                      <?php }
                                                    }
                                                  }?>

                                                <?php foreach ($docs as $ijazah_s3) {
                                                    if ($ijazah_s3['applicant'] == $app['id_user']) {
                                                      if (isset($ijazah_s3['cert_doctor'])) { ?>
                                                        <a href="<?php echo site_url('admin/download_file/doctor/'.$ijazah_s3['cert_doctor']) ?>">Download Ijazah S3</a><br />
                                                      <?php }
                                                    }
                                                  }?>
                                                </td>
                                                <td>
                                                <?php foreach ($docs as $dikti) {
                                                    if ($dikti['applicant'] == $app['id_user']) {
                                                      if ($dikti['cert_inter']) { ?>
                                                        <a href="<?php echo site_url('admin/download_file/inter/'.$dikti['cert_inter']) ?>">Download Penyetaraan Dikti</a><br />
                                                      <?php }
                                                    }
                                                  }?>
                                                  <?php foreach ($docs as $sk_mendik) {
                                                    if ($sk_mendik['applicant'] == $app['id_user']) {
                                                      if ($sk_mendik['sk_mendiknas']) { ?>
                                                        <a href="<?php echo site_url('admin/download_file/mendiknas/'.$sk_mendik['sk_mendiknas']) ?>">Download Mendiknas</a><br />
                                                      <?php }
                                                    }
                                                  }?>
                                                  <?php foreach ($docs as $lolos_butuh) {
                                                    if ($lolos_butuh['applicant'] == $app['id_user']) {
                                                      if ($lolos_butuh['paper_lolos_butuh']) { ?>
                                                        <a href="<?php echo site_url('admin/download_file/lolos_butuh/'.$lolos_butuh['paper_lolos_butuh']) ?>">Download Lolos Butuh</a><br />
                                                      <?php }
                                                    }
                                                  }?>
                                                  <?php foreach ($docs as $sk_henti) {
                                                    if ($sk_henti['applicant'] == $app['id_user']) {
                                                      if ($sk_henti['sk_berhenti']) { ?>
                                                        <a href="<?php echo site_url('admin/download_file/sk_berhenti/'.$sk_henti['sk_berhenti']) ?>">Download SK Berhenti</a><br />
                                                      <?php }
                                                    }
                                                  }?>
                                                </td>
                                                <td>
                                                <?php foreach ($docs as $rohani) {
                                                    if ($rohani['applicant'] == $app['id_user']) {
                                                      if ($rohani['sehat_rohani']) { ?>
                                                        <a href="<?php echo site_url('admin/download_file/sehat_rohani/'.$rohani['sehat_rohani']) ?>">Download Sehat Rohani</a><br />
                                                      <?php }
                                                    }
                                                  }?>
                                                  <?php foreach ($docs as $jasmani) {
                                                    if ($jasmani['applicant'] == $app['id_user']) {
                                                      if ($jasmani['sehat_jasmani']) { ?>
                                                        <a href="<?php echo site_url('admin/download_file/sehat_jasmani/'.$jasmani['sehat_jasmani']) ?>">Download Sehat Jasmani</a><br />
                                                      <?php }
                                                    }
                                                  }?>
                                                  <?php foreach ($docs as $narkotika) {
                                                    if ($narkotika['applicant'] == $app['id_user']) {
                                                      if ($narkotika['bebas_narkotika']) {?>
                                                        <a href="<?php echo site_url('admin/download_file/bebas_narkotika/'.$narkotika['bebas_narkotika']) ?>">Download Bebas Narkotika</a><br />
                                                      <?php }
                                                    }
                                                  }?>
                                                </td>
                                                <td>
                                                <?php foreach ($status as $stat) {
                                                  if ($app['status'] == $stat['id_status']) {
                                                    $status_name = $stat['status'];
                                                    echo $status_name;
                                                  } elseif ($app['status'] == "") {
                                                    $status_name = "Status is empty";
                                                  }

                                                };?>

                                                </td>
                                                <td>
                                                   <?php if ($app['status']=='2'||$app['status']=='8') { ?>
                                                    <input type="hidden" name="id_user" class="view-applicant" value="<?php echo $app['id_applicant'];?>" />
                                                   <a id="view-applicant" class="c-btn small green-bg btn-block view-applicant" data-toggle="modal" data-toggle="modal" data-idapplicant="<?php echo $app['id_applicant'];?>">View</a>
                                                    <a title="" class="c-btn small green-bg btn-block approve-applicant" data-toggle="modal" data-id="<?php echo $app['id_applicant']?>" data-name="<?php echo $fullname?>">Approve</a>
                                                    <a title="" class="c-btn small red-bg btn-block reject-applicant" data-toggle="modal" data-id="<?php echo $app['id_applicant']?>" data-name="<?php echo $fullname?>">Reject</a>
                                                    <a class="c-btn small green-bg btn-block " href="#">Download RAR</a>
                                                   <?php } else { ?>
                                                    <a title="" class="c-btn small green-bg btn-block view-applicant" data-toggle="modal" data-toggle="modal" data-idapplicant="<?php echo $app['id_applicant'];?>">View</a>
                                                   <?php } ?>
                                                </td>
                                            </tr>
                                            <!-- Modal view -->
                                            <div class="modal fade" id="view-applicant-modal-<?php echo $app['id_applicant'];?>" tabindex="-1" role="dialog">
                                              <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Personal Data</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                        <div class="row">
                                                          <div class="col-md-6">
                                                                <div class="form-group">
                                                                   <label for="name">Name</label>
                                                                   <?php foreach ($user as $u) {
                                                                     if ($app['id_user']==$u['id_user']) {
                                                                       $userdata = $u;
                                                                       echo '<input type="text" placeholder="Name" id="name" name="name" class="form-control name" value="'.$userdata['full_name'].'" required="required" readonly>';
                                                                     }
                                                                   } ?>
                                                                </div>
                                                                <div class="form-group">
                                                                   <label for="vacancy">Vacancy</label>
                                                                   <?php foreach ($vacancy as $va) {
                                                                     if ($va['id_vacancy'] == $app['vacancy']) {
                                                                       $job = $va['title_vacancy'];
                                                                       echo '<input type="text" placeholder="Vacancy" id="vacancy_name" name="vacancy" class="form-control vacancy_name" value="'.$job.'" required="required" readonly>';
                                                                     }
                                                                   }
                                                                   ?>

                                                                </div>
                                                                <div class="form-group">
                                                                   <label for="birthdate">Birth Date</label>
                                                                   <input type="text" placeholder="Birthdate" id="birthdate" name="birthdate" class="form-control birthdate" value="<?php echo $userdata['birth_date']?>" readonly>
                                                                </div>
                                                                <div class="form-group">
                                                                   <label for="gender">Gender</label>
                                                                   <input type="text" placeholder="Gender" class="form-control gender" value="<?php foreach($jenkel as $j) { if($j['id_gender']==$userdata['gender']) { echo $j['gender'];} } ?>" readonly>
                                                                </div>
                                                                <div class="form-group">
                                                                   <label for="marital">Marital</label>
                                                                   <input type="text" placeholder="Marital" class="form-control marital" value="<?php foreach($marital as $m) { if($m['id_marital'] == $userdata['marital']) {echo $m['marital'];} }?>" readonly>
                                                                </div>
                                                                <div class="form-group">
                                                                   <label for="mother_name" readonly>Mother Name</label>
                                                                   <input type="text" placeholder="Mother Name" id="mother_name" name="mother_name" class="form-control" value="<?php echo $userdata['mother_name']?>" readonly>
                                                                </div>
                                                          </div>
                                                          <div class="col-md-6">
                                                            <div class="form-group">
                                                              <label for="address">Address</label>
                                                              <textarea class="form-control" rows="5" readonly><?php echo $userdata['address']?></textarea>
                                                           </div>
                                                            <div class="form-group">
                                                              <label for="id_card">ID Card</label>
                                                              <input type="text" placeholder="ID Card" id="id_card" name="id_card" class="form-control" value="<?php echo $userdata['id_card_no']?>" readonly>
                                                           </div>
                                                            <div class="form-group">
                                                              <label for="homephone">Homephone</label>
                                                              <input type="text" placeholder="Homephone" id="homephone" name="homephone" class="form-control" value="<?php echo $userdata['homephone_no']?>" readonly>
                                                           </div>
                                                            <div class="form-group">
                                                              <label for="handphone">Handphone</label>
                                                              <input type="text" placeholder="Handphone" id="handphone" name="handphone" class="form-control" value="<?php echo $userdata['handphone_no']?>" readonly>
                                                           </div>
                                                            <div class="form-group">
                                                              <label for="email">Email</label>
                                                              <input type="text" placeholder="Email" id="email" name="email" class="form-control" value="<?php echo $userdata['email']?>" readonly>
                                                           </div>
                                                          </div>
                                                        </div>
                                                  </div>

                                                  <!-- Data Pendidikan Form -->
                                                  <?php $i = 1;
                                                  foreach ($education as $edu) { ?>
                                                    <?php if ($edu['id_user'] == $userdata['id_user']): ?>
                                                      <div class="modal-header">
                                                        <h4 class="modal-title">Education Data</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                              <div class="form-group">
                                                                <h5 class="modal-title">#Education-<?php echo $i;?></h5>
                                                              </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                              <div class="form-group">
                                                                 <label for="jenjang" readonly>Jenjang</label>
                                                                 <input type="text" placeholder="Jenjang" class="form-control jenjang-<?php echo $i?>" value="<?php foreach($jenjang as $je){if($je['id_jenjang']==$edu['jenjang']){echo $je['jenjang'];}};?>" readonly>
                                                              </div>
                                                              <div class="form-group">
                                                                 <label for="role" readonly>NIM</label>
                                                                 <input type="text" placeholder="NIM" class="form-control nim-<?php echo $i?>" value="<?php echo $edu['nim']?>" readonly>
                                                              </div>
                                                              <div class="form-group">
                                                                 <label for="Jurusan" readonly>Jurusan</label>
                                                                 <input type="text" placeholder="Jurusan" class="form-control jurusan-<?php echo $i?>" value="<?php echo $edu['prodi']?>" readonly>
                                                              </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                              <div class="col-md-6">
                                                                <div class="form-group">
                                                                   <label for="in_year" readonly>Tahun Masuk</label>
                                                                   <input style="width:100%;" type="text" placeholder="Tahun Masuk" class="form-control in-year-<?php echo $i?>" value="<?php echo $edu['in_year']?>" readonly>
                                                                </div>
                                                              </div>
                                                              <div class="col-md-6">
                                                                <div class="form-group">
                                                                   <label for="end_year" readonly>Tahun Keluar</label>
                                                                   <input type="text" placeholder="Tahun Lulus" class="form-control" value="<?php echo $edu['end_year']?>" readonly>
                                                                  </div>
                                                              </div>
                                                              <div class="form-group">
                                                                 <label for="minat" readonly>Bidang Minat</label>
                                                                 <input type="text" placeholder="Bidang Minat" class="form-control " value="<?php echo $edu['minat']?>" readonly>
                                                                </div>
                                                                <div class="form-group">
                                                                   <label for="karya" readonly>Judul Karya Ilmiah</label>
                                                                   <input type="text" placeholder="Judul Karya Ilmiah" class="form-control" value="<?php echo $edu['karya']?>" readonly>
                                                                  </div>
                                                            </div>
                                                            <div class="form-group">
                                                              <hr />
                                                            </div>
                                                        </div>
                                                      </div>
                                                    <?php endif; ?>
                                                  <?php $i++;} ?>
                                                  <!-- End Data Pendidikan -->
                                                </div>
                                              </div>
                                            </div>

                                        <?php  } ?>
                                        <?php } ?>
                                     </tbody>
                                   </table>
                                   <div id="summary">
                                   <div>
                                   <h5>Reports : </h5>
                                   <a href="<?php echo base_url()?>admin/report_file" title="" class="icon-btn"><i class="fa fa-file-excel-o skyblue-bg"></i> ALL APPLICANT</a>
                                </div>
                              </div>
                         </div>
                    </div>
            </div>
        </div>
    </div>
</div><!-- Panel Content -->

<!-- Modal Reject -->
<div class="modal fade small-modal" id="small" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <input type="hidden" value="" name="id_applicant" id="id_applicant" class="id_applicant"/>
      <div class="form-content">
      </div>
      <div class="modal-footer">
      <button type="button" class="c-btn large red-bg btn-block" id="btn-reject-applicant">Reject</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Approve -->
<div class="modal fade small-modal" id="approve-applicant-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Notification</h4>
      </div>
      <div class="modal-body">
        <center>
          <span class="alert-sukses" style="display:none;"><strong>Done!</strong> You successfully approve applicant.</span>
          <span class="alert-process" style="display:none;"><strong>Please wait...</strong> Processing data..</span>
          <span class="alert-error" style="display:none;"><strong>Sorry!</strong> approve applicant unsuccessful</span>
          <span class="alert-infor" style="display:none;"><strong>Sorry!</strong> <span id="mess"></span></span>
         </center>
         <input type="hidden" value="" name="id_applicant" id="id_applicant" class="id_applicant"/>
        <center>Are you sure for approve applicant?</center>
      </div>
      <div class="modal-footer">
        <button type="button" class="c-btn large green-bg btn-block" id="btn-approve-applicant">Approve</button>
      </div>
    </div>
  </div>
</div>



<!-- Modal edit -->
<div class="modal fade" id="edit-applicant-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Data Applicant</h4>
      </div>
      <div class="modal-body">
        <form class="user-form">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                       <label for="name">Name</label>
                       <input type="text" placeholder="Name" id="name" name="name" class="form-control name" value="{name}" required="required">
                       <input type="hidden" id="action" name="action" class="form-control" value="update">
                    </div>
                    <div class="form-group">
                       <label for="vacancy">Vacancy</label>
                       <input type="text" placeholder="Vacancy" id="vacancy_name" name="vacancy" class="form-control" value="{vacancy}" required="required">
                    </div>
                    <div class="form-group">
                       <label for="birthdate">Birth Date</label>
                       <input type="text" placeholder="Birthdate" id="birthdate" name="birthdate" class="form-control" value="{birthdate}" >
                    </div>
                    <div class="form-group">
                       <label for="gender">Gender</label>
                       <select name="gender" class="form-control">
                         <?php foreach ($gender as $g) { ?>
                           <?php if ($g['id_gender'] == $gender_me) { ?>
                            <option value="<?php echo $g['id_gender']?>" selected><?php echo $g['gender']?></option>
                           <?php } else { ?>
                            <option value="<?php echo $g['id_gender']?>"><?php echo $g['gender']?></option>
                           <?php } ?>
                         <?php }?>
                       </select>
                    </div>
                    <div class="form-group">
                       <label for="marital">Marital</label>
                       <select name="marital" class="form-control">
                         <?php foreach ($marital as $m) { ?>
                           <?php if ($m['id_marital'] == $marital_me) { ?>
                            <option value="<?php echo $m['id_marital']?>" selected><?php echo $m['marital']?></option>
                           <?php } else { ?>
                            <option value="<?php echo $m['id_marital']?>"><?php echo $m['marital']?></option>
                           <?php } ?>
                         <?php }?>
                       </select>
                    </div>
                    <div class="form-group">
                       <label for="mother_name">Mother Name</label>
                       <input type="text" placeholder="Mother Name" id="mother_name" name="mother_name" class="form-control" value="{mother_name}" >
                    </div>
                     <div class="form-group">
                       <label for="address">Address</label>
                       <input type="text" placeholder="Address" id="address" name="address" class="form-control" value="{address}" >
                    </div>
                     <div class="form-group">
                       <label for="id_card">ID Card</label>
                       <input type="text" placeholder="ID Card" id="id_card" name="id_card" class="form-control" value="{id_card}" >
                    </div>
                     <div class="form-group">
                       <label for="homephone">Homephone</label>
                       <input type="text" placeholder="Homephone" id="homephone" name="homephone" class="form-control" value="{homephone}" >
                    </div>
                     <div class="form-group">
                       <label for="handphone">Handphone</label>
                       <input type="text" placeholder="Handphone" id="handphone" name="handphone" class="form-control" value="{handphone}" >
                    </div>
                     <div class="form-group">
                       <label for="email">Email</label>
                       <input type="text" placeholder="Email" id="email" name="email" class="form-control" value="{email}" >
                    </div>
                    <div class="form-group">
                       <label for="role">Role</label>
                       <select name="role" class="form-control">
                         <?php if($role == '1') { ?>
                          <option value="1" selected> Admin</option>
                          <option value="2"> Participant</option>
                         <?php } else { ?>
                          <option value="1"> Admin</option>
                          <option value="2" selected> Participant</option>
                         <?php } ?>
                       </select>
                    </div>
                    <div class="form-group">
                       <label for="status">Status</label>
                       <select name="status" class="form-control">
                         <?php if($status == '1') { ?>
                          <option value="1" selected> Enable</option>
                          <option value="2"> Disable</option>
                         <?php } else { ?>
                          <option value="1" > Enable</option>
                          <option value="2" selected> Disable</option>
                         <?php } ?>
                       </select>
                    </div>
              </div>

            </div>
            <br />
            <button class="btn green-bg btn-block" type="submit" id="user">Save</button>
            </form>
      </div>
    </div>
  </div>
</div>
