<div class="container">
<div class="main-content-area">
               <div class="mail-area">
                    <div class="row">
                         <div class="col-md-12">
                              <div class="inbox-nav">
                                   <div class="user-mail">
                                        <span><img src="http://placehold.it/100x100" alt="" /></span>
                                        <h3>Labrina Scholer</h3>
                                        <i>Senior Designer</i>
                                        <ul class="social-btns">
                                             <li><a title=""  ><i class="ti-facebook"></i></a></li>
                                             <li><a title=""  ><i class="ti-google"></i></a></li>
                                             <li><a title=""  ><i class="ti-twitter"></i></a></li>
                                        </ul>
                                        <a href="#/mail/compose" title="" class="compose-btn">Compose</a>
                                   </div>
                                   <div class="mail-menu">
                                        <ul>
                                             <li><a   title="">Inbox <span class="green-bg">23</span></a></li>
                                             <li><a   title="">Important</a></li>
                                             <li><a   title="">Sent</a></li>
                                             <li><a   title="">Drafts</a></li>
                                             <li><a   title="">Trash<span class="red-bg">67</span></a></li>
                                        </ul>
                                   </div>
                                   <div class="mail-labels">
                                        <h3>Labels</h3>
                                        <ul>
                                             <li><a   title=""><i class="ti-tag"></i>Friends</a></li>
                                             <li><a   title=""><i class="ti-tag"></i>Groups</a></li>
                                             <li><a   title=""><i class="ti-tag"></i>Private</a></li>
                                        </ul>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <!-- Modal -->
               </div><!-- Mail Area -->
          </div>
</div>