<style>
th, td{
  vertical-align: middle !important;
}
</style>

<!-- Script of Reason form -->
<script>
function saveInterview1Reason(result,applicant,vacancy){
    $.ajax({
      url : "<?php echo base_url();?>admin/vacancy/result_test",
      type: "post",
      data: "action=interview_1_reason&id_applicant="+applicant+"&id_vacancy="+vacancy+"&value="+result+"",
      dataType:"json",
      beforeSend:function(){
        $(".alert-process").show();
        $(".alert-error").hide();
        $(".alert-sukses").hide();
        console.log($(".form-result-vacancy-"+$(this).data('idapplicant')).serialize());
        console.log(result);
      },
      success:function(result){
        if(result.error == '00'){
           setTimeout(function(){
             $(".alert-process").hide();
             $(".alert-error").hide();
             $(".alert-sukses").show();
           }, 1000);
           setTimeout(function(){
             location.reload();
           }, 2500);
           console.log('in');
        } else if (result.error == '01') {
           setTimeout(function(){
           $(".alert-process").hide();
           $(".alert-error").show();
           $(".alert-sukses").hide();
           }, 2000);
           console.log(result);
           console.log('01');
        } else {
           setTimeout(function(){
           $(".alert-process").hide();
           $(".alert-error").show();
           $(".alert-sukses").hide();
           }, 2000);
           console.log('else');
        }

      },
      error: function(xhr, Status, err) {
         setTimeout(function(){
         $(".alert-process").hide();
         $(".alert-error").show();
         $(".alert-sukses").hide();
         }, 2000);
         console.log('error');
      }

    });

  return false;
}

function savePsikotestReason(result,applicant,vacancy){
    $.ajax({
      url : "<?php echo base_url();?>admin/vacancy/result_test",
      type: "post",
      data: "action=psikotest_reason&id_applicant="+applicant+"&id_vacancy="+vacancy+"&value="+result+"",
      dataType:"json",
      beforeSend:function(){
        $(".alert-process").show();
        $(".alert-error").hide();
        $(".alert-sukses").hide();
        console.log($(".form-result-vacancy-"+$(this).data('idapplicant')).serialize());
        console.log(result);
      },
      success:function(result){
        if(result.error == '00'){
           setTimeout(function(){
             $(".alert-process").hide();
             $(".alert-error").hide();
             $(".alert-sukses").show();
           }, 1000);
           setTimeout(function(){
             location.reload();
           }, 2500);
           console.log('in');
        } else if (result.error == '01') {
           setTimeout(function(){
           $(".alert-process").hide();
           $(".alert-error").show();
           $(".alert-sukses").hide();
           }, 2000);
           console.log(result);
           console.log('01');
        } else {
           setTimeout(function(){
           $(".alert-process").hide();
           $(".alert-error").show();
           $(".alert-sukses").hide();
           }, 2000);
           console.log('else');
        }

      },
      error: function(xhr, Status, err) {
         setTimeout(function(){
         $(".alert-process").hide();
         $(".alert-error").show();
         $(".alert-sukses").hide();
         }, 2000);
         console.log('error');
      }

    });

  return false;
}

function saveToeflReason(result,applicant,vacancy){
    $.ajax({
      url : "<?php echo base_url();?>admin/vacancy/result_test",
      type: "post",
      data: "action=toefl_reason&id_applicant="+applicant+"&id_vacancy="+vacancy+"&value="+result+"",
      dataType:"json",
      beforeSend:function(){
        $(".alert-process").show();
        $(".alert-error").hide();
        $(".alert-sukses").hide();
        console.log($(".form-result-vacancy-"+$(this).data('idapplicant')).serialize());
        console.log(result);
      },
      success:function(result){
        if(result.error == '00'){
           setTimeout(function(){
             $(".alert-process").hide();
             $(".alert-error").hide();
             $(".alert-sukses").show();
           }, 1000);
           setTimeout(function(){
             location.reload();
           }, 2500);
           console.log('in');
        } else if (result.error == '01') {
           setTimeout(function(){
           $(".alert-process").hide();
           $(".alert-error").show();
           $(".alert-sukses").hide();
           }, 2000);
           console.log(result);
           console.log('01');
        } else {
           setTimeout(function(){
           $(".alert-process").hide();
           $(".alert-error").show();
           $(".alert-sukses").hide();
           }, 2000);
           console.log('else');
        }

      },
      error: function(xhr, Status, err) {
         setTimeout(function(){
         $(".alert-process").hide();
         $(".alert-error").show();
         $(".alert-sukses").hide();
         }, 2000);
         console.log('error');
      }

    });

  return false;
}

function saveInterview2Reason(result,applicant,vacancy){
    $.ajax({
      url : "<?php echo base_url();?>admin/vacancy/result_test",
      type: "post",
      data: "action=interview_2_reason&id_applicant="+applicant+"&id_vacancy="+vacancy+"&value="+result+"",
      dataType:"json",
      beforeSend:function(){
        $(".alert-process").show();
        $(".alert-error").hide();
        $(".alert-sukses").hide();
        console.log($(".form-result-vacancy-"+$(this).data('idapplicant')).serialize());
        console.log(result);
      },
      success:function(result){
        if(result.error == '00'){
           setTimeout(function(){
             $(".alert-process").hide();
             $(".alert-error").hide();
             $(".alert-sukses").show();
           }, 1000);
           setTimeout(function(){
             location.reload();
           }, 2500);
           console.log('in');
        } else if (result.error == '01') {
           setTimeout(function(){
           $(".alert-process").hide();
           $(".alert-error").show();
           $(".alert-sukses").hide();
           }, 2000);
           console.log(result);
           console.log('01');
        } else {
           setTimeout(function(){
           $(".alert-process").hide();
           $(".alert-error").show();
           $(".alert-sukses").hide();
           }, 2000);
           console.log('else');
        }

      },
      error: function(xhr, Status, err) {
         setTimeout(function(){
         $(".alert-process").hide();
         $(".alert-error").show();
         $(".alert-sukses").hide();
         }, 2000);
         console.log('error');
      }

    });

  return false;
}
</script>
<!-- End of Reason form -->

<script>
  function saveInterview1(result,applicant,vacancy){
      $.ajax({
        url : "<?php echo base_url();?>admin/vacancy/result_test",
        type: "post",
        data: "action=interview_1&id_applicant="+applicant+"&id_vacancy="+vacancy+"&value="+result+"",
        dataType:"json",
        beforeSend:function(){
          $(".alert-process").show();
          $(".alert-error").hide();
          $(".alert-sukses").hide();
          console.log($(".form-result-vacancy-"+$(this).data('idapplicant')).serialize());
          console.log(result);
        },
        success:function(result){
          if(result.error == '00'){
             setTimeout(function(){
               $(".alert-process").hide();
               $(".alert-error").hide();
               $(".alert-sukses").show();
             }, 1000);
             setTimeout(function(){
               location.reload();
             }, 2500);
             console.log('in');
          } else if (result.error == '01') {
             setTimeout(function(){
             $(".alert-process").hide();
             $(".alert-error").show();
             $(".alert-sukses").hide();
             }, 2000);
             console.log(result);
             console.log('01');
          } else {
             setTimeout(function(){
             $(".alert-process").hide();
             $(".alert-error").show();
             $(".alert-sukses").hide();
             }, 2000);
             console.log('else');
          }

        },
        error: function(xhr, Status, err) {
           setTimeout(function(){
           $(".alert-process").hide();
           $(".alert-error").show();
           $(".alert-sukses").hide();
           }, 2000);
           console.log('error');
        }

      });

    return false;
  }

  function savePsikotest(result,applicant,vacancy){
      $.ajax({
        url : "<?php echo base_url();?>admin/vacancy/result_test",
        type: "post",
        data: "action=psikotest&id_applicant="+applicant+"&id_vacancy="+vacancy+"&value="+result+"",
        dataType:"json",
        beforeSend:function(){
          $(".alert-process").show();
          $(".alert-error").hide();
          $(".alert-sukses").hide();
        },
        success:function(result){
          if(result.error == '00'){
             setTimeout(function(){
               $(".alert-process").hide();
               $(".alert-error").hide();
               $(".alert-sukses").show();
             }, 1000);
             setTimeout(function(){
               location.reload();
             }, 2500);
          } else if (result.error == '01') {
             setTimeout(function(){
             $(".alert-process").hide();
             $(".alert-error").show();
             $(".alert-sukses").hide();
             }, 2000);
          } else {
             setTimeout(function(){
             $(".alert-process").hide();
             $(".alert-error").show();
             $(".alert-sukses").hide();
             }, 2000);
          }

        },
        error: function(xhr, Status, err) {
           setTimeout(function(){
           $(".alert-process").hide();
           $(".alert-error").show();
           $(".alert-sukses").hide();
           }, 2000);
           console.log('error');
        }

      });

    return false;
    }

    function saveToefl(result,applicant,vacancy){
        $.ajax({
          url : "<?php echo base_url();?>admin/vacancy/result_test",
          type: "post",
          data: "action=toefl&id_applicant="+applicant+"&id_vacancy="+vacancy+"&value="+result+"",
          dataType:"json",
          beforeSend:function(){
            $(".alert-process").show();
            $(".alert-error").hide();
            $(".alert-sukses").hide();
          },
          success:function(result){
            if(result.error == '00'){
               setTimeout(function(){
                 $(".alert-process").hide();
                 $(".alert-error").hide();
                 $(".alert-sukses").show();
               }, 1000);
               setTimeout(function(){
                 location.reload();
               }, 2500);
            } else if (result.error == '01') {
               setTimeout(function(){
               $(".alert-process").hide();
               $(".alert-error").show();
               $(".alert-sukses").hide();
               }, 2000);
            } else {
               setTimeout(function(){
               $(".alert-process").hide();
               $(".alert-error").show();
               $(".alert-sukses").hide();
               }, 2000);
            }

          },
          error: function(xhr, Status, err) {
             setTimeout(function(){
             $(".alert-process").hide();
             $(".alert-error").show();
             $(".alert-sukses").hide();
             }, 2000);
             console.log('error');
          }

        });

      return false;
    }

      function saveInterview2(result,applicant,vacancy){
          $.ajax({
            url : "<?php echo base_url();?>admin/vacancy/result_test",
            type: "post",
            data: "action=interview_2&id_applicant="+applicant+"&id_vacancy="+vacancy+"&value="+result+"",
            dataType:"json",
            beforeSend:function(){
              $(".alert-process").show();
              $(".alert-error").hide();
              $(".alert-sukses").hide();
            },
            success:function(result){
              if(result.error == '00'){
                 setTimeout(function(){
                   $(".alert-process").hide();
                   $(".alert-error").hide();
                   $(".alert-sukses").show();
                 }, 1000);
                 setTimeout(function(){
                   location.reload();
                 }, 2500);
              } else if (result.error == '01') {
                 setTimeout(function(){
                 $(".alert-process").hide();
                 $(".alert-error").show();
                 $(".alert-sukses").hide();
                 }, 2000);
              } else {
                 setTimeout(function(){
                 $(".alert-process").hide();
                 $(".alert-error").show();
                 $(".alert-sukses").hide();
                 }, 2000);
              }

            },
            error: function(xhr, Status, err) {
               setTimeout(function(){
               $(".alert-process").hide();
               $(".alert-error").show();
               $(".alert-sukses").hide();
               }, 2000);
               console.log('error');
            }

          });

        return false;
  }
</script>
<div class="main-content">
    <div class="panel-content">
        <div class="main-title-sec">
            <div class="row">
                <div class="col-md-3 column">
                    <div class="heading-profile">
                        <h2>Result Vacancy</h2>
                        <span></span>
                    </div>
                </div>
            </div>
        </div><!-- Heading Sec -->
        <ul class="breadcrumbs">
            <li><a href="<?php echo base_url();?>admin" title="">Dashboard</a></li>
            <li><a href="" title="">Result Vacancy</a></li>
        </ul>
        <div class="main-content-area">
               <div class="row">

                    <div class="col-md-12">

                         <div class="streaming-table">
                                  <center>
          <span class="alert-sukses" style="display:none;"><strong>Done!</strong> You successfully update applicant.</span>
          <span class="alert-process" style="display:none;"><strong>Please wait...</strong> Processing data..</span>
          <span class="alert-error" style="display:none;"><strong>Sorry!</strong> <span id="mess"></span></span>
          <span class="alert-infor" style="display:none;"><strong>Sorry!</strong> <span id="mess"></span></span>
         </center>
                                   <span id="found" class="label label-info"></span>
                                   <table id="stream_table" class='table table-striped table-bordered'>
                                     <thead>
                                        <tr>
                                          <!-- <th>ID Dosen</th> -->
                                          <th rowspan="2">ID Applicant</th>
                                          <th rowspan="2">Applicant</th>
                                          <th width="10%" colspan="2">Interview I</th>

                                          <th width="10%" colspan="2">Psikotest</th>

                                          <th width="10%" colspan="2">TOEFL</th>

                                          <th width="10%" colspan="2">Interview II</th>

                                          <th width="12%" rowspan="2">Action</th>
                                        </tr>
                                        <tr>
                                          <th >Status</th>
                                          <th width="5%">Result</th>
                                          <th >Status</th>
                                          <th width="5%">Result</th>
                                          <th >Status</th>
                                          <th width="5%">Result</th>
                                          <th >Status</th>
                                          <th width="5%">Result</th>
                                        </tr>

                                     </thead>
                                     <tbody>
                                        <?php if(isset($announcement)=='') { ?>
                                          <h4>Annoucement is empty</h4>
                                        <?php } else { ?>
                                          <?php foreach ($announcement as $vac) { ?>

                                            <tr>
                                                <!-- <td><?php echo $vac['id_dosen'];?></td> -->
                                                <td rowspan="2"><?php echo 'CALDOS - '.str_pad($vac['id_applicant'], 4, '0', STR_PAD_LEFT);?></td>
                                                <td rowspan="2" class="text-capitalize">
                                                <?php
                                                  foreach ($user as $app) {
                                                    if ($app['id_user'] == $vac['id_user']) {
                                                      echo $app['full_name'];
                                                    }
                                                  }
                                                ?>
                                                </td>
                                                <td style="vertical-align:middle;">
                                                  <?php echo ($vac['status'] > '3' && $vac['status'] <= '12') ? "<a title='' class='c-btn mini skyblue-bg btn-block'>Passed</a>" :  "<a title='' class='c-btn mini red-bg btn-block'>Not Passed</a>"?>
                                                </td>
                                                <td>
                                                  <input type="text" onchange="saveInterview1($(this).val(),<?php echo $vac['id_applicant'];?>,<?php echo $vac['vacancy'];?>);" class="form-control" value="<?php echo (isset($vac['interview_1'])) ? $vac['interview_1'] : '';?>">
                                                </td>
                                                <td>
                                                  <?php echo ($vac['status'] > '4' && $vac['status'] <= '12') ? "<a title='' class='c-btn mini skyblue-bg btn-block'>Passed</a>" :  "<a title='' class='c-btn mini red-bg btn-block'>Not Passed</a>"?>
                                                </td>
                                                <td>
                                                  <input type="text" onchange="savePsikotest($(this).val(),<?php echo $vac['id_applicant'];?>,<?php echo $vac['vacancy'];?>);" class="form-control" value="<?php echo (isset($vac['psikotest'])) ? $vac['psikotest'] : '';?>">
                                                </td>
                                                <td>
                                                  <?php echo ($vac['status'] > '5' && $vac['status'] <= '12') ? "<a title='' class='c-btn mini skyblue-bg btn-block'>Passed</a>" :  "<a title='' class='c-btn mini red-bg btn-block'>Not Passed</a>"?>
                                                </td>
                                                <td>
                                                  <input type="text" onchange="saveToefl($(this).val(),<?php echo $vac['id_applicant'];?>,<?php echo $vac['vacancy'];?>);" class="form-control" value="<?php echo (isset($vac['toefl'])) ? $vac['toefl'] : '';?>">
                                                </td>
                                                <td>
                                                  <?php echo ($vac['status'] > '6' && $vac['status'] <= '12') ? "<a title='' class='c-btn mini skyblue-bg btn-block'>Passed</a>" :  "<a title='' class='c-btn mini red-bg btn-block'>Not Passed</a>"?>
                                                </td>
                                                <td>
                                                  <input type="text" onchange="saveInterview2($(this).val(),<?php echo $vac['id_applicant'];?>,<?php echo $vac['vacancy'];?>);" class="form-control" value="<?php echo (isset($vac['interview_2'])) ? $vac['interview_2'] : '';?>">
                                                </td>
                                                <td rowspan="2">
                                                      <?php if ($vac['status'] =='3') { ?>
                                                        <input type="hidden" name="id_applicant" class="pass-interview-1" value="<?php echo $vac['id_applicant']?>">
                                                        <input type="hidden" name="id_vacancy" class="pass-interview-1" value="<?php echo $vac['vacancy']?>">
                                                        <input type="hidden" name="action" class="pass-interview-1" value="interview_1">
                                                        <a title="" class="c-btn small green-bg btn-block pass-interview-id pass_interview_1_btn" data-id="<?php echo $vac['id_applicant']?>">Pass Interview I</a>
                                                      <?php } elseif ($vac['status'] == '4') { ?>
                                                        <input type="hidden" name="id_applicant" class="pass-psikotest" value="<?php echo $vac['id_applicant']?>">
                                                        <input type="hidden" name="id_vacancy" class="pass-psikotest" value="<?php echo $vac['vacancy']?>">
                                                        <input type="hidden" name="action" class="pass-psikotest" value="psikotest">
                                                        <a href="javascript:void(0)" title="" class="c-btn small green-bg btn-block pass_psikotest_btn">Pass Psikotest</a>
                                                      <?php } elseif ($vac['status'] =='5') { ?>
                                                        <input type="hidden" name="id_applicant" class="pass-toefl" value="<?php echo $vac['id_applicant']?>">
                                                        <input type="hidden" name="id_vacancy" class="pass-toefl" value="<?php echo $vac['vacancy']?>">
                                                        <input type="hidden" name="action" class="pass-toefl" value="toefl">
                                                        <a href="javascript:void(0)" title="" class="c-btn small green-bg btn-block pass_toefl_btn">Pass TOEFL</a>
                                                      <?php } elseif ($vac['status'] =='6') { ?>
                                                        <input type="hidden" name="id_applicant" class="pass-interview-2" value="<?php echo $vac['id_applicant']?>">
                                                        <input type="hidden" name="id_user" class="pass-interview-2" value="<?php echo $vac['id_user']?>">
                                                        <input type="hidden" name="id_vacancy" class="pass-interview-2" value="<?php echo $vac['vacancy']?>">
                                                        <input type="hidden" name="action" class="pass-interview-2" value="interview_2">
                                                        <a href="javascript:void(0)" title="" class="c-btn small green-bg btn-block pass_interview_2_btn">Pass Interview II</a>
                                                      <?php } elseif ($vac['status'] =='10') { ?>
                                                        <a href="javascript:void(0)" title="" class="c-btn small red-bg btn-block">Not Qualified</a>
                                                      <?php } elseif ($vac['status'] =='1') { ?>
                                                        <a href="javascript:void(0)" title="" class="c-btn small green-bg btn-block">Waiting Filled CV</a>
                                                      <?php } elseif ($vac['status'] =='2') { ?>
                                                        <a href="javascript:void(0)" title="" class="c-btn small green-bg btn-block">Validasi Form I</a>
                                                      <?php } elseif ($vac['status'] =='7') { ?>
                                                        <a href="javascript:void(0)" title="" class="c-btn small green-bg btn-block">Waiting upload form II</a>
                                                      <?php } elseif ($vac['status'] =='8') { ?>
                                                        <a href="javascript:void(0)" title="" class="c-btn small green-bg btn-block">Validasi Form II</a>
                                                      <?php } elseif ($vac['status'] =='9') { ?>
                                                        <input type="hidden" name="id_applicant" class="create-id" value="<?php echo $vac['id_applicant']?>">
                                                        <input type="hidden" name="id_user" class="create-id" value="<?php echo $vac['id_user']?>">
                                                        <input type="hidden" name="id_vacancy" class="create-id" value="<?php echo $vac['vacancy']?>">
                                                        <input type="hidden" name="action" class="create-id" value="create_id">
                                                        <a href="javascript:void(0)" title="" class="c-btn small green-bg btn-block create-id-btn">Create ID</a>
                                                      <?php } else { ?>
                                                        <a href="javascript:void(0)" title="" class="c-btn small green-bg btn-block">New Dosen</a>
                                                      <?php } ?>
                                                </td>
                                            </tr>
                                            <tr>
                                              <td colspan="2">
                                                <input type='text' onchange="saveInterview1Reason($(this).val(),<?php echo $vac['id_applicant'];?>,<?php echo $vac['vacancy'];?>);" class='form-control input-sm' placeholder='Reason' value="<?php echo (isset($vac['interview_1_reason'])) ? $vac['interview_1_reason'] : '';?>">
                                              </td>
                                              <td colspan="2">
                                                <input type='text' onchange="savePsikotestReason($(this).val(),<?php echo $vac['id_applicant'];?>,<?php echo $vac['vacancy'];?>);" class='form-control input-sm' placeholder='Reason' value="<?php echo (isset($vac['psikotest_reason'])) ? $vac['psikotest_reason'] : '';?>">
                                              </td>
                                              <td colspan="2">
                                                <input type='text' onchange="saveToeflReason($(this).val(),<?php echo $vac['id_applicant'];?>,<?php echo $vac['vacancy'];?>);" class='form-control input-sm' placeholder='Reason' value="<?php echo (isset($vac['toefl_reason'])) ? $vac['toefl_reason'] : '';?>">
                                              </td>
                                              <td colspan="2">
                                                <input type='text' onchange="saveInterview2Reason($(this).val(),<?php echo $vac['id_applicant'];?>,<?php echo $vac['vacancy'];?>);" class='form-control input-sm' placeholder='Reason' value="<?php echo (isset($vac['interview_2_reason'])) ? $vac['interview_2_reason'] : '';?>">
                                              </td>
                                            </tr>
                                        <?php  } ?>
                                        <?php } ?>
                                     </tbody>
                                     <tfoot>

                                     </tfoot>
                                   </table>
                                   <div id="summary">
                                   <div>
                                   <h5>Reports : </h5>
                                   <a href="<?php echo base_url('admin/report_file/vacancy/'.$this->input->post("id_vacancy").'');?>" title="" class="icon-btn"><i class="fa fa-file-excel-o skyblue-bg"></i> ALL APPLICANT</a>
                                </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
    </div>
</div><!-- Panel Content -->

<div class="modal fade small-modal" id="delete-vacancy-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Notification</h4>
      </div>
      <div class="modal-body">
        <center>
          <span class="alert-sukses" style="display:none;"><strong>Done!</strong> You successfully deleted vacancy.</span>
          <span class="alert-process" style="display:none;"><strong>Please wait...</strong> Processing data..</span>
          <span class="alert-error" style="display:none;"><strong>Sorry!</strong> delete vacancy is unsuccessful</span>
          <span class="alert-infor" style="display:none;"><strong>Sorry!</strong> <span id="mess"></span></span>
         </center>
         <input type="hidden" id="id_vacancy_delete" name="id_vacancy" class="id_vacancy_delete" value="">
        <center>Are you sure ?</center>
      </div>
      <div class="modal-footer">
        <button type="button" class="c-btn large red-bg btn-block" id="btn-delete-vacancy">Delete</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="edit-vacancy-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Data Vacancy</h4>
      </div>
      <div class="modal-body">
        <br />
        <center>
          <span class="alert-sukses" style="display:none;"><strong>Done!</strong> You successfully save vacancy.</span>
          <span class="alert-process" style="display:none;"><strong>Please wait...</strong> Processing data..</span>
          <span class="alert-error" style="display:none;"><strong>Sorry!</strong> save vacancy is unsuccessful, please check your data and submit again.</span>
          <span class="alert-infor" style="display:none;"><strong>Sorry!</strong> <span class="mess"></span></span>
         </center>
         <br/>
        <div class="widget-tabs">
           <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" data-target="#information">Vacancy Information</a></li>
                <li><a data-toggle="tab" data-target="#interview_1">Interview I</a></li>
                <li><a data-toggle="tab" data-target="#psikotest">Psikotest</a></li>
                <li><a data-toggle="tab" data-target="#toefl">TOEFL</a></li>
                <li><a data-toggle="tab" data-target="#interview_2">Interview II</a></li>
           </ul>
           <div class="tab-content">

                <div id="information" class="tab-pane fade in active">
                  <div class="form-group">
                     <label for="job">Job</label>
                     <input type="hidden" id="id_vacancy" name="id_vacancy" class="id_vacancy_edit edit-form-vacancy" value="">
                     <input type="hidden" id="action" name="action" class="edit-form-vacancy" value="update">
                     <input type="text" placeholder="Job" id="job" name="job" class="form-control edit-form-vacancy job" value="">
                  </div>
                  <div class="form-group">
                     <label for="Description">Description</label>
                     <textarea name="description" id="description" class="form-control vacancy-description edit-form-vacancy" placeholder="Description" rows="5"></textarea>
                  </div>
                  <div class="form-group">
                     <label for="deadline">Deadline</label>
                     <input type="text" placeholder="Deadline" id="datetimepicker" name="deadline" class="form-control datetimepicker deadline edit-form-vacancy" value="" >
                  </div>
                </div>
                <div id="interview_1" class="tab-pane fade">
                    <div class="form-group">
                       <label for="job">Interview I Schedule</label>
                       <!-- <input type="text" placeholder="Schedule" id="datetimepicker" name="interview_schedule_1" class="form-control" value="2012-05-15 21:05" required>-->
                        <input type="text" value="" class="form-control datetimepicker interview_schedule_1 edit-form-vacancy" name="interview_schedule_1" id="datetimepicker" >
                    </div>
                    <div class="form-group">
                       <label for="interview_description_1">Interview I Description</label>
                       <textarea name="interview_description_1" id="interview_description_1" class="form-control interview_description_1 edit-form-vacancy" placeholder="Interview I Description" rows="5"></textarea>
                    </div>

                </div>
                <div id="psikotest" class="tab-pane fade">
                     <div class="form-group">
                       <label for="psikotest">Psikotest Schedule</label>
                       <!-- <input type="text" placeholder="Schedule" id="datetimepicker" name="interview_schedule_1" class="form-control" value="2012-05-15 21:05" >-->
                        <input type="text" value="" name="psikotest_schedule" class="form-control datetimepicker psikotes_schedule edit-form-vacancy"  id="datetimepicker" >
                    </div>
                    <div class="form-group">
                       <label for="psikotest">Psikotest Description</label>
                       <textarea name="psikotest_description" id="description" class="form-control psikotes_description edit-form-vacancy" placeholder="Psikotest Description" rows="5" ></textarea>
                    </div>

                </div>
                <div id="toefl" class="tab-pane fade">
                     <div class="form-group">
                       <label for="toefl_schedule">TOEFL Schedule</label>
                       <!-- <input type="text" placeholder="Schedule" id="datetimepicker" name="interview_schedule_1" class="form-control" value="2012-05-15 21:05" >-->
                        <input type="text" value="" name="toefl_schedule" class="form-control datetimepicker toefl_schedule edit-form-vacancy"  id="datetimepicker" >
                    </div>
                    <div class="form-group">
                       <label for="toefl_description">TOEFL Description</label>
                       <textarea name="toefl_description" id="description" class="form-control toefl_description edit-form-vacancy" placeholder="Description" rows="5" ></textarea>
                    </div>

                </div>
                <div id="interview_2" class="tab-pane fade">
                     <div class="form-group">
                       <label for="job">Interview II Schedule</label>
                       <!-- <input type="text" placeholder="Schedule" id="datetimepicker" name="interview_schedule_1" class="form-control" value="2012-05-15 21:05" >-->
                        <input type="text" value="" name="interview_schedule_2" class="form-control datetimepicker interview_schedule_2 edit-form-vacancy"  id="datetimepicker" >
                    </div>
                    <div class="form-group">
                       <label for="Description">Description</label>
                       <textarea name="interview_description_2" id="description" class="form-control interview_description_2 edit-form-vacancy" placeholder="Description" rows="5" ></textarea>
                    </div>

                </div>
           </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="c-btn large green-bg btn-block" id="update_vacancy">Update</button>
      </div>
      </div>
    </div>
  </div>
</div>
