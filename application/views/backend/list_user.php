<div class="main-content">
    <div class="panel-content">
        <div class="main-title-sec">
            <div class="row">
                <div class="col-md-3 column">
                    <div class="heading-profile">
                        <h2>List User</h2>
                        <span></span>
                    </div>
                </div>
                <div class="col-md-9 column">
                    <div class="quick-btn-title">
                        <a href="{add_user}" title=""><i class="fa fa-plus"></i> New User</a>
                    </div>
                </div>
            </div>
        </div><!-- Heading Sec -->
        <ul class="breadcrumbs ">
            <li><a href="<?php echo base_url();?>admin" title="">Dashboard</a></li>
            <li><a href="" title="">List User</a></li>
        </ul>
        <div class="main-content-area">
            <div class="row">
                <div class="col-md-12">
                  <center>
                    <span class="alert-sukses" style="display:none;"><strong>Done!</strong> You successfully deleted user.</span>
                    <span class="alert-process" style="display:none;"><strong>Please wait...</strong> Processing data..</span>
                    <span class="alert-error" style="display:none;"><strong>Sorry!</strong> delete user is unsuccessful</span>
                    <span class="alert-infor" style="display:none;"><strong>Sorry!</strong> <span id="mess"></span></span>
                   </center>
                         <div class="streaming-table">
                                   <table id="stream_table" class='table table-striped table-bordered'>
                                     <thead>
                                        <tr>
                                          <th>ID User</th>
                                          <th>Username</th>
                                          <th>Email</th>
                                          <th>Role</th>
                                          <th>Status</th>
                                          <th width="15px">Action</th>
                                        </tr>
                                     </thead>
                                     <tbody>
                                        <?php if(isset($user)=='') { ?>
                                          <h4>User is empty</h4>
                                        <?php } else { ?>
                                          <?php foreach ($user as $u) { ?>
                                            <tr>
                                                <td><?php echo 'User - '.str_pad($u['id_user'], 4, '0', STR_PAD_LEFT)?></td>
                                                <td><?php echo $u['username'];?></td>
                                                <td><?php echo $u['email'];?></td>
                                                <td class="text-capitalize">
                                                  <?php foreach ($peran as $key) {
                                                    if ($u['role'] == $key['id_role']) {
                                                      $rouser = $key['role'];
                                                    } else {
                                                      $rouser = '';
                                                    }
                                                    echo $rouser;
                                                  }?>
                                                </td>
                                                <td>
                                                <?php if ($u['status'] == '1') {
                                                  $status = 'Enable';
                                                  } else {
                                                  $status = "Disable";
                                                 } echo $status;?>
                                                </td>
                                                <td>
                                                   <?php if ($this->session->userdata('role') == '3') { ?>
                                                    <?php if ($this->session->userdata('id') == $u['id_user']) { ?>
                                                      <a title="" class="c-btn small green-bg btn-block edit-user" data-toggle="modal" data-iduser="<?php echo $u['id_user']?>" data-username="<?php echo $u['username']?>" data-email="<?php echo $u['email']?>" data-rouser="<?php foreach($peran as $key){if($u['role']==$key['id_role']){$rouser=$key['role'];}else{$rouser='';}echo$rouser;}?>" data-status="<?php echo $status?>">My Account</a>
                                                    <?php } else { ?>
                                                      <a title="" class="c-btn small green-bg btn-block edit-user2" data-toggle="modal" data-iduser="<?php echo $u['id_user']?>" data-username="<?php echo $u['username']?>" data-email="<?php echo $u['email']?>" data-rouser2="<?php foreach($peran as $key){if($u['role']==$key['id_role']){$rouser=$key['role'];}else{$rouser='';}echo$rouser;}?>" data-status2="<?php echo $status?>">Edit</a>
                                                      <a href="<?php echo base_url('admin/delete_user/').'/'.$u['id_user']?>" class="c-btn small red-bg btn-block" data-toggle="modal" data-iduserdelete="<?php echo $u['id_user']?>">Delete</a>
                                                    <?php } ?>
                                                   <?php } else { ?>
                                                      <?php if ($u['role']=='1' && ($u['id_user'] == $this->session->userdata('id')) || $u['role']=='2') { ?>
                                                        <?php if ($this->session->userdata('id') == $u['id_user']) { ?>
                                                          <a title="" class="c-btn small green-bg btn-block edit-user" data-toggle="modal" data-iduser="<?php echo $u['id_user']?>" data-username="<?php echo $u['username']?>" data-email="<?php echo $u['email']?>" data-rouser="<?php foreach($peran as $key){if($u['role']==$key['id_role']){$rouser=$key['role'];}else{$rouser='';}echo$rouser;}?>" data-status="<?php echo $status?>">My Account</a>
                                                        <?php } else { ?>
                                                          <a title="" class="c-btn small green-bg btn-block edit-user2" data-toggle="modal" data-iduser="<?php echo $u['id_user']?>" data-username="<?php echo $u['username']?>" data-email="<?php echo $u['email']?>" data-rouser2="<?php foreach($peran as $key){if($u['role']==$key['id_role']){$rouser=$key['role'];}else{$rouser='';}echo$rouser;}?>" data-status2="<?php echo $status?>">Edit</a>
                                                          <input type="hidden" value="<?php echo $u['id_user']?>" name="id_user" class="iduser" id="id_user_<?php echo $u['id_user']?>">
                                                        <?php } ?>
                                                      <?php } else { ?>
                                                        <a title="" class="c-btn small green-bg btn-block view-user" data-toggle="modal" data-username="<?php echo $u['username']?>" data-email="<?php echo $u['email']?>" data-rouser="<?php foreach($peran as $key){if($u['role']==$key['id_role']){$rouser=$key['role'];}else{$rouser='';}echo$rouser;}?>" data-status="<?php echo $status?>">View</a>
                                                      <?php } ?>
                                                   <?php } ?>

                                                </td>
                                            </tr>
                                        <?php  } ?>
                                        <?php } ?>
                                     </tbody>
                                   </table>
                                   <div id="summary">
                                   <div>
                                </div>
                              </div>
                         </div>
                    </div>
            </div>
        </div>
    </div>
</div><!-- Panel Content -->

<div class="modal fade small-modal delete-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Notification</h4>
      </div>
      <div class="modal-body">
        <center>
          <span class="alert-sukses" style="display:none;"><strong>Done!</strong> You successfully deleted vacancy.</span>
          <span class="alert-process" style="display:none;"><strong>Please wait...</strong> Processing data..</span>
          <span class="alert-error" style="display:none;"><strong>Sorry!</strong> delete user is unsuccessful</span>
          <span class="alert-infor" style="display:none;"><strong>Sorry!</strong> <span id="mess"></span></span>
         </center>
         <input type="hidden" id="id_vacancy_edit" name="id_vacancy" class="delete-form-vacancy" value="">
        <center>Are you sure ?</center>
      </div>
      <div class="modal-footer">
        <button type="button" class="c-btn large red-bg btn-block" id="delete-vacancy-btn">Delete</button>
      </div>
    </div>
  </div>
</div>

<!-- Edit modal -->
<div class="modal fade" id="edit-user-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Data User</h4>
      </div>
      <div class="modal-body">
        <br />
        <center>
          <span class="alert-sukses" style="display:none;"><strong>Done!</strong> You successfully save update user.</span>
          <span class="alert-process" style="display:none;"><strong>Please wait...</strong> Processing data..</span>
          <span class="alert-error" style="display:none;"><strong>Sorry!</strong> update user is unsuccessful, please check your data and submit again.</span>
          <span class="alert-infor" style="display:none;"><strong>Sorry!</strong> <span class="mess"></span></span>
         </center>
         <br/>
        <form class="user-form">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                       <label for="username">Username</label>
                       <input type="text" placeholder="Username" id="username" name="username" class="form-control username form-edit-user" value="" required="required">
                       <input type="hidden" id="id_user_update" name="id_user_update" class="form-control form-edit-user" value="">
                       <input type="hidden" id="action" name="action" class="form-control form-edit-user" value="update">
                    </div>
                    <div class="form-group">
                       <label for="password">Password</label>
                       <input type="password" placeholder="Password" id="password" name="password" class="form-control password form-edit-user" value="" required="required">
                    </div>
                    <div class="form-group">
                       <label for="email">Email</label>
                       <input type="email" placeholder="Email" id="email" name="email" class="form-control email form-edit-user" value="" >
                    </div>
                    <div class="form-group">
                       <label for="role">Role</label>
                       <select name="role" class="form-control role form-edit-user"></select>
                    </div>
                    <div class="form-group">
                       <label for="status">Status</label>
                       <select name="status" class="form-control status form-edit-user"></select>
                    </div>
              </div>

            </div>
            <br />
            <button class="btn green-bg btn-block btn-user-update" type="submit">Save</button>
            </form>
      </div>
    </div>
  </div>
</div>

<!-- Edit modal -->
<div class="modal fade" id="edit-user2-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Data User</h4>
      </div>
      <div class="modal-body">
        <br />
        <center>
          <span class="alert-sukses" style="display:none;"><strong>Done!</strong> You successfully save update user.</span>
          <span class="alert-process" style="display:none;"><strong>Please wait...</strong> Processing data..</span>
          <span class="alert-error" style="display:none;"><strong>Sorry!</strong> update user is unsuccessful, please check your data and submit again.</span>
          <span class="alert-infor" style="display:none;"><strong>Sorry!</strong> <span class="mess"></span></span>
         </center>
         <br/>
        <form class="user-form">
            <div class="row">
              <div class="col-md-12">
                    <div class="form-group">
                       <label for="role">Role</label>
                       <select name="role" class="form-control role form-edit-user"></select>
                    </div>
                    <div class="form-group">
                       <label for="status">Status</label>
                       <select name="status" class="form-control status form-edit-user"></select>
                    </div>
              </div>
            </div>
            <br />
            <button class="btn green-bg btn-block btn-user-update" type="submit">Save</button>
            </form>
      </div>
    </div>
  </div>
</div>

<!-- View modal -->
<div class="modal fade" id="view-user-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">View Data User</h4>
      </div>
      <div class="modal-body">
        <form class="user-form">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                       <label for="username">Username</label>
                       <input type="text" placeholder="Username" id="username" name="username" class="form-control username" value="" readonly>
                    </div>
                    <div class="form-group">
                       <label for="email">Email</label>
                       <input type="email" placeholder="Email" id="email" name="email" class="form-control email" value="" readonly>
                    </div>
                    <div class="form-group">
                       <label for="role">Role</label>
                       <input type="text" placeholder="Role" id="roleuser" name="roleuser" class="form-control roleuser" value="" readonly>
                    </div>
                    <div class="form-group">
                       <label for="status" readonly>Status</label>
                       <input type="text" placeholder="Status" name="status" class="form-control status" value="" readonly>
                    </div>
              </div>

            </div>
            </form>
      </div>
    </div>
  </div>
</div>
