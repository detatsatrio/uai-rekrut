/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 10.1.21-MariaDB : Database - uia_rekrut
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`uia_rekrut` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `uia_rekrut`;

/*Table structure for table `applicant` */

DROP TABLE IF EXISTS `applicant`;

CREATE TABLE `applicant` (
  `id_applicant` int(10) NOT NULL AUTO_INCREMENT,
  `id_user` int(10) NOT NULL,
  `id_dosen` int(10) DEFAULT NULL,
  `vacancy` int(10) DEFAULT NULL,
  `interview_1` varchar(30) DEFAULT NULL,
  `interview_1_reason` text,
  `psikotest` varchar(30) DEFAULT NULL,
  `psikotest_reason` text,
  `toefl` varchar(30) DEFAULT NULL,
  `toefl_reason` text,
  `interview_2` varchar(30) DEFAULT NULL,
  `interview_2_reason` text,
  `status` int(2) DEFAULT NULL,
  `create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update` time NOT NULL,
  PRIMARY KEY (`id_applicant`),
  KEY `status_ticket` (`status`),
  KEY `lowongan` (`vacancy`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lowongan` FOREIGN KEY (`vacancy`) REFERENCES `vacancy` (`id_vacancy`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `status_ticket` FOREIGN KEY (`status`) REFERENCES `status` (`id_status`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;

/*Data for the table `applicant` */

insert  into `applicant`(`id_applicant`,`id_user`,`id_dosen`,`vacancy`,`interview_1`,`interview_1_reason`,`psikotest`,`psikotest_reason`,`toefl`,`toefl_reason`,`interview_2`,`interview_2_reason`,`status`,`create`,`update`) values 
(79,40,NULL,259,'80','Gak lulus nilainya kurang                                    ',NULL,'belum testing',NULL,'belum beruntung',NULL,'gak sesuai                                            ',2,'2017-08-01 18:41:40','00:00:00'),
(82,40,NULL,258,'80','Gak lulus\n                                                ',NULL,NULL,NULL,NULL,NULL,NULL,2,'2017-08-01 18:43:37','00:00:00'),
(83,40,NULL,259,'80','Gak lulus\n                                                ',NULL,NULL,NULL,NULL,NULL,NULL,2,'2017-08-01 18:34:11','00:00:00'),
(84,40,NULL,258,'80','Gak lulus\n                                                ',NULL,NULL,NULL,NULL,NULL,NULL,2,'2017-08-01 18:43:41','00:00:00'),
(85,40,NULL,259,'80','Gak lulus\n                                                ',NULL,NULL,NULL,NULL,NULL,NULL,3,'2017-08-01 18:47:39','00:00:00'),
(86,40,NULL,258,'80','Gak lulus\n                                                ',NULL,NULL,NULL,NULL,NULL,NULL,2,'2017-08-01 18:43:50','00:00:00'),
(87,40,NULL,257,'80','Gak lulus\n                                                ',NULL,NULL,NULL,NULL,NULL,NULL,2,'2017-08-01 18:43:52','00:00:00'),
(88,40,NULL,259,'80','Gak lulus\n                                                ',NULL,NULL,NULL,NULL,NULL,NULL,2,'2017-08-01 18:34:11','00:00:00'),
(89,40,NULL,257,'80','Gak lulus\n                                                ',NULL,NULL,NULL,NULL,NULL,NULL,2,'2017-08-01 18:43:54','00:00:00'),
(90,40,NULL,257,'80','Gak lulus\n                                                ',NULL,NULL,NULL,NULL,NULL,NULL,2,'2017-08-01 18:43:58','00:00:00'),
(91,40,NULL,259,'80','Gak lulus\n                                                ',NULL,NULL,NULL,NULL,NULL,NULL,2,'2017-08-01 18:34:11','00:00:00');

/*Table structure for table `docs` */

DROP TABLE IF EXISTS `docs`;

CREATE TABLE `docs` (
  `id_docs` int(10) NOT NULL AUTO_INCREMENT,
  `applicant` int(10) DEFAULT NULL,
  `id_pict` varchar(255) DEFAULT NULL,
  `id_card_pict` varchar(255) DEFAULT NULL,
  `cert_bachelor` varchar(255) DEFAULT NULL,
  `cert_master` varchar(255) DEFAULT NULL,
  `cert_doctor` varchar(255) DEFAULT NULL,
  `cert_inter` varchar(255) DEFAULT NULL,
  `sk_mendiknas` varchar(255) DEFAULT NULL,
  `paper_lolos_butuh` varchar(255) DEFAULT NULL,
  `sk_berhenti` varchar(255) DEFAULT NULL,
  `sehat_jasmani` varchar(255) DEFAULT NULL,
  `sehat_rohani` varchar(255) DEFAULT NULL,
  `bebas_narkotika` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_docs`),
  KEY `id_pelamar` (`applicant`),
  CONSTRAINT `id_pelamar` FOREIGN KEY (`applicant`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `docs` */

insert  into `docs`(`id_docs`,`applicant`,`id_pict`,`id_card_pict`,`cert_bachelor`,`cert_master`,`cert_doctor`,`cert_inter`,`sk_mendiknas`,`paper_lolos_butuh`,`sk_berhenti`,`sehat_jasmani`,`sehat_rohani`,`bebas_narkotika`) values 
(1,40,NULL,'id-card-40.jpg','ijazah-s1-40.jpg','ijazah-s2-40.jpg',NULL,'cert-inter-40.jpg','sk-mendiknas-40.jpg','lolos-butuh-40.jpg',NULL,NULL,NULL,NULL);

/*Table structure for table `education` */

DROP TABLE IF EXISTS `education`;

CREATE TABLE `education` (
  `id_education` int(2) NOT NULL AUTO_INCREMENT,
  `id_user` int(10) DEFAULT NULL,
  `jenjang` int(10) DEFAULT NULL,
  `in_year` varchar(20) DEFAULT NULL,
  `end_year` varchar(20) DEFAULT NULL,
  `nim` varchar(20) DEFAULT NULL,
  `prodi` varchar(60) DEFAULT NULL,
  `minat` varchar(60) DEFAULT NULL,
  `karya` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_education`),
  KEY `id_user_education` (`id_user`),
  KEY `jenjang_education` (`jenjang`),
  CONSTRAINT `id_user_education` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jenjang_education` FOREIGN KEY (`jenjang`) REFERENCES `jenjang` (`id_jenjang`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `education` */

insert  into `education`(`id_education`,`id_user`,`jenjang`,`in_year`,`end_year`,`nim`,`prodi`,`minat`,`karya`) values 
(1,40,1,'2015','2017','','','1230','karya'),
(2,40,3,'30-08-2016','28-08-2015','240000','apa saja','apasaja','karya ilmiah'),
(3,41,3,'213','213','213','123','123','13');

/*Table structure for table `fakultas` */

DROP TABLE IF EXISTS `fakultas`;

CREATE TABLE `fakultas` (
  `IDFakultas` tinyint(3) unsigned NOT NULL,
  `NamaFakultas` varchar(50) DEFAULT NULL,
  `FakultasInggris` varchar(50) DEFAULT NULL,
  `Alias` varchar(10) DEFAULT NULL,
  `warna` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`IDFakultas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fakultas` */

insert  into `fakultas`(`IDFakultas`,`NamaFakultas`,`FakultasInggris`,`Alias`,`warna`) values 
(0,'Universitas','University','UAI','#ffffff'),
(1,'Sains dan Teknologi','Science and Technology','ST','#d9f8b1'),
(3,'Ekonomi','Economics','FE','#d7e5f9'),
(4,'Sastra','Literature','FS','#eeeeee'),
(6,'Psikologi dan Pendidikan','Psychology and Education','FP','#f8d7f9'),
(7,'Hukum','Faculty of Law','FH','#f9d7dc'),
(8,'Ilmu Sosial dan Ilmu Politik','Social and Political Sciences','FI','#f9f9d7'),
(9,'Pasca Sarjana','Pasca Sarjana','PS',NULL);

/*Table structure for table `gender` */

DROP TABLE IF EXISTS `gender`;

CREATE TABLE `gender` (
  `id_gender` int(2) NOT NULL AUTO_INCREMENT,
  `gender` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_gender`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `gender` */

insert  into `gender`(`id_gender`,`gender`) values 
(1,'Laki-laki'),
(2,'Perempuan');

/*Table structure for table `jenjang` */

DROP TABLE IF EXISTS `jenjang`;

CREATE TABLE `jenjang` (
  `id_jenjang` int(10) NOT NULL AUTO_INCREMENT,
  `jenjang` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_jenjang`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `jenjang` */

insert  into `jenjang`(`id_jenjang`,`jenjang`) values 
(1,'DI'),
(2,'DII'),
(3,'DIII'),
(4,'DIV'),
(5,'S1'),
(6,'S2'),
(7,'S3');

/*Table structure for table `marital` */

DROP TABLE IF EXISTS `marital`;

CREATE TABLE `marital` (
  `id_marital` int(2) NOT NULL AUTO_INCREMENT,
  `marital` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id_marital`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `marital` */

insert  into `marital`(`id_marital`,`marital`) values 
(1,'Lajang'),
(2,'Menikah'),
(3,'Bercerai');

/*Table structure for table `prodi` */

DROP TABLE IF EXISTS `prodi`;

CREATE TABLE `prodi` (
  `IDProgdi` int(4) unsigned NOT NULL,
  `IDFakultas` int(4) unsigned NOT NULL,
  `IDJenjang` int(4) unsigned NOT NULL,
  `kode_dikti` varchar(10) NOT NULL,
  `id_sms` varchar(50) NOT NULL COMMENT 'Kode PD-DIKTI',
  `nidn_kaprodi` varchar(20) NOT NULL,
  `Kode` varchar(3) DEFAULT NULL,
  `NamaProgdi` varchar(50) DEFAULT NULL,
  `NamaTerdaftar` varchar(50) NOT NULL,
  `ProgdiInggris` varchar(50) DEFAULT NULL,
  `Simbol` varchar(10) DEFAULT NULL,
  `Gelar` varchar(10) NOT NULL,
  `kode_jenjang_dikti` varchar(10) NOT NULL,
  PRIMARY KEY (`IDProgdi`),
  KEY `IDFakultas` (`IDFakultas`),
  KEY `IDJenjang` (`IDJenjang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prodi` */

insert  into `prodi`(`IDProgdi`,`IDFakultas`,`IDJenjang`,`kode_dikti`,`id_sms`,`nidn_kaprodi`,`Kode`,`NamaProgdi`,`NamaTerdaftar`,`ProgdiInggris`,`Simbol`,`Gelar`,`kode_jenjang_dikti`) values 
(0,0,0,'','','','00','Universitas','',NULL,'UAI','',''),
(1,1,5,'26201','b230f968-cc8b-42dd-b893-eaaa82e250af','0324087401','01','Teknik Industri','Teknik Industri','Industrial Engineering','TI','S.T','C'),
(2,1,5,'55201','6f103fd1-b503-44ae-bd14-acdb805c7d49','0417026702','02','Teknik Informatika','Teknik Informatika','Informatics Engineering','IF','S.T','C'),
(3,1,5,'20201','6e6ee0db-11d2-4b88-b53e-7a125a1b6d61','0308067501','03','Teknik Elektro','Teknik Elektro','Electrical Engineering','EL','S.T','C'),
(6,1,5,'46201','9d78886b-f89a-42bf-8bcc-e31333fa39bf','0317076401','04','Biologi (Biotek)','Biologi (Biotek)','Biology','BI','S.Si','C'),
(7,3,5,'61201','0fecb290-2c8b-4d4c-b312-e5ca0740c0a1','0326087605','01','Manajemen','Manajemen','Management','MJ','SE','C'),
(8,3,5,'62201','ff21668e-6ece-4dde-9bdd-a5fc01307564','0030097702','02','Akuntansi','Akuntansi','Accounting','AK','SE','C'),
(9,4,5,'79203','bc0dad7b-c642-4a4f-8685-ff74ec262583','0325087701','01','Sastra Arab','Sastra Arab','Arabic Literature','AR','S.S','C'),
(10,4,5,'79209','15e68f4c-6e92-4239-bf3f-a6679645f387','0324117201','02','Sastra Cina','Sastra Cina','Chinese Literature','CN','','C'),
(11,4,5,'79202','002a6c6c-578d-4ba0-83e7-3e8eee5afb1d','0305098202','03','Sastra Inggris','Sastra Inggris','English Literature','IG','S.S','C'),
(12,4,5,'79204','e8b6a7a1-60af-40f9-8f1a-ab5ff5b8d3d0','0330047701','04','Sastra Jepang','Sastra Jepang','Japanese Literature','JP','S.S','C'),
(16,6,5,'70232','589f8c62-1804-40b2-a2bd-7750fd22ab3e','0002057712','01','Bimbingan Penyuluhan Islam (Konseling)','Bimbingan Penyuluhan Islam','Department of Islamic Guidance and Counseling','DK','S.Sos.I','C'),
(17,6,5,'86208','66b8f40b-bd4a-4f9a-a77f-98a440b093fc','0010046801','02','Tarbiyah PAUD','Pendidikan Agama Islam','Department of Islamic Education','TA','S.Pd','C'),
(18,7,5,'74201','657d8299-c5b5-4a0a-b1d2-5f7b021288d6','0304067002','01','Ilmu Hukum','Hukum Ekonomi dan Teknologi','Department of Law','HE','SH','C'),
(19,8,5,'64201','c4f67bac-803f-42ab-bbd5-eda02f7ff0f4','0327056901','01','Ilmu Hubungan Internasional','Ilmu Hubungan Internasional','International Relations','HI','SIP','C'),
(20,8,5,'70201','c1063bf3-289e-433d-b10b-9d260427e687','0302048202','02','Ilmu Komunikasi','Ilmu Komunikasi','Communication','KM','S.Sos','C'),
(33,6,5,'73201','067a959c-b301-482e-bdc0-75aa7fe47b27','0329118202','03','Psikologi','Psikologi','Psychology','PI','','C'),
(35,6,5,'86207','8b0c58ac-fe3d-461d-8120-f1b21d86ab09','0010046801','04','PG PAUD','PG PAUD','Early Childhood Teacher Education','PG','','C'),
(36,9,6,'74171','3362ca93-873a-4d13-affe-960914e98dcc','0320027202','02','Magister Ilmu Hukum','Magister Ilmu Hukum',NULL,'PH','','B'),
(37,8,5,'70201','c1063bf3-289e-433d-b10b-9d260427e687','0302048202','02','Ilmu Komunikasi (Kelas Sore)','Ilmu Komunikasi','Communication','KM','S.Sos','C');

/*Table structure for table `research` */

DROP TABLE IF EXISTS `research`;

CREATE TABLE `research` (
  `id_research` int(2) NOT NULL AUTO_INCREMENT,
  `applicant` int(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `publikasi` varchar(255) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_research`),
  KEY `id_user_research` (`applicant`),
  CONSTRAINT `id_user_research` FOREIGN KEY (`applicant`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `research` */

insert  into `research`(`id_research`,`applicant`,`title`,`category`,`name`,`publikasi`,`tahun`) values 
(1,40,'               penelitian               ','penelitian','penelitian','penelitian','mata kuliah'),
(2,41,'   Judul   ','kategro','kategro','kategro','213');

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id_role` int(2) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  `create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update` time NOT NULL DEFAULT '00:00:00',
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `role` */

insert  into `role`(`id_role`,`role`,`create`,`update`) values 
(1,'Admin','2017-04-12 23:50:15','12:56:44'),
(2,'Applicant','2017-04-12 23:49:49','12:56:49'),
(3,'Super Admin','2017-04-23 21:05:56','12:56:55');

/*Table structure for table `status` */

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id_status` int(2) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) DEFAULT NULL,
  `create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update` time NOT NULL DEFAULT '00:00:00',
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `status` */

insert  into `status`(`id_status`,`status`,`create`,`update`) values 
(1,'Register','2017-04-12 23:58:13','19:33:08'),
(2,'Validasi Form I','2017-04-12 23:57:11','19:33:16'),
(3,'Interview I','2017-04-12 23:57:18','19:33:23'),
(4,'Psikotest','2017-04-12 23:57:25','19:33:26'),
(5,'TOEFL','2017-04-12 23:57:28','19:33:30'),
(6,'Interview II','2017-04-12 23:57:36','19:33:37'),
(7,'Final Result','2017-04-12 23:57:42','19:33:47'),
(8,'Validasi Form II','2017-04-12 23:57:48','19:33:54'),
(9,'Created ID','2017-04-12 23:57:56','19:33:59'),
(10,'Not Qualified','2017-04-22 13:15:31','19:34:07'),
(11,'Dosen','2017-06-15 20:48:32','00:00:00'),
(12,'Passed','2017-07-16 02:20:05','00:00:00');

/*Table structure for table `status_result` */

DROP TABLE IF EXISTS `status_result`;

CREATE TABLE `status_result` (
  `id_status_result` int(10) NOT NULL AUTO_INCREMENT,
  `status_result` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_status_result`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `status_result` */

insert  into `status_result`(`id_status_result`,`status_result`) values 
(1,'Passed'),
(2,'Not Passed');

/*Table structure for table `status_user` */

DROP TABLE IF EXISTS `status_user`;

CREATE TABLE `status_user` (
  `id_status_user` int(2) NOT NULL AUTO_INCREMENT,
  `status_user` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_status_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `status_user` */

insert  into `status_user`(`id_status_user`,`status_user`) values 
(1,'Enable'),
(2,'Disable');

/*Table structure for table `teach` */

DROP TABLE IF EXISTS `teach`;

CREATE TABLE `teach` (
  `id_teach` int(10) NOT NULL AUTO_INCREMENT,
  `applicant` int(10) DEFAULT NULL,
  `lecture` varchar(255) DEFAULT NULL,
  `semester` varchar(60) DEFAULT NULL,
  `tahun` varchar(60) DEFAULT NULL,
  `jenjang` varchar(12) DEFAULT NULL,
  `prodi` varchar(255) DEFAULT NULL,
  `fakultas` varchar(255) DEFAULT NULL,
  `perguruan_tinggi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_teach`),
  KEY `pelamar_teach` (`applicant`),
  CONSTRAINT `pelamar_teach` FOREIGN KEY (`applicant`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `teach` */

insert  into `teach`(`id_teach`,`applicant`,`lecture`,`semester`,`tahun`,`jenjang`,`prodi`,`fakultas`,`perguruan_tinggi`) values 
(1,40,'mata kuliah','mata kuliah','mata kuliah','1','mata kuliah','mata kuliah','mata kuliah'),
(2,41,'Mata kuliah','21','213','3','sad','as','sda');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `birth_place` varchar(255) DEFAULT NULL,
  `birth_date` varchar(255) DEFAULT NULL,
  `gender` int(10) DEFAULT NULL,
  `marital` int(10) DEFAULT NULL,
  `mother_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `id_card_no` varchar(255) DEFAULT NULL,
  `homephone_no` varchar(255) DEFAULT NULL,
  `handphone_no` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` time NOT NULL DEFAULT '00:00:00',
  `status` int(2) DEFAULT NULL,
  `role` int(2) NOT NULL DEFAULT '2',
  `oauth_provider` varchar(60) DEFAULT NULL,
  `oauth_uid` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  KEY `role_user` (`role`),
  KEY `status_user` (`status`),
  KEY `gender_user` (`gender`),
  KEY `marital` (`marital`),
  CONSTRAINT `gender_user` FOREIGN KEY (`gender`) REFERENCES `gender` (`id_gender`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `marital` FOREIGN KEY (`marital`) REFERENCES `marital` (`id_marital`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user` FOREIGN KEY (`role`) REFERENCES `role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `status_user` FOREIGN KEY (`status`) REFERENCES `status_user` (`id_status_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`username`,`password`,`email`,`full_name`,`birth_place`,`birth_date`,`gender`,`marital`,`mother_name`,`address`,`id_card_no`,`homephone_no`,`handphone_no`,`created`,`updated`,`status`,`role`,`oauth_provider`,`oauth_uid`) values 
(1,'detatsatrio','V1VUh/62tQFMXkmhQES9YLcFw0zBk+2bQ7ilUg3gh1CFN2mJ1Xa+D4kgTTSOzoxJBJukVAFkoVlz9D7LUIAqYA==','admin@uia.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-04-12 23:48:06','22:12:08',1,1,NULL,NULL),
(2,'admin','s0RXojIiAS+SKQd0MjJ7Sk+3aJ9ai8OIl1dV7DVqHIlE9IGZYNawxmnNUET10gP9UBnQbJZAlImr7kdJ2sVjFA==','admin@uia.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-04-21 20:34:53','23:11:54',1,3,NULL,NULL),
(16,'supersu','Tog9d8pf5sbaYoAvoCnZUBXzwdJuT+uNqqfhJ74JXh0iHQw+BcFa9DunLxC3Whdrl59Ovb6UsNc4f4OFxeLJ5g==',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-04-23 21:11:30','21:11:55',1,3,NULL,NULL),
(40,'pelamar','NYapDwvXuvuJb/0KrUUiOVP22JWOdLJUQIkaezuiq+kTxdDaPDrU2MZb4ifnu+iuI7A3i4c+6I0GYhI5292j8g==','detatsatr89io@gmail.com','John Doe','semarnag','30-08-1993',1,1,'Jessica ','Jl. Waringin 9 No.47 Griya Pangkah Indah','123123','085642240012','087730015246','2017-06-11 00:50:05','01:25:34',1,2,NULL,NULL),
(41,'fara','NYapDwvXuvuJb/0KrUUiOVP22JWOdLJUQIkaezuiq+kTxdDaPDrU2MZb4ifnu+iuI7A3i4c+6I0GYhI5292j8g==','detatsatriwqo@outlook.com','Nama','Tegla','40-23-120',1,1,'Rabbinai','asdjkl','123','a213','213','2017-06-11 12:28:30','12:29:13',1,2,NULL,NULL),
(42,'baru','5xo3fGkRp3ayuBttpGgjv/scTtenJw9MbbSwhP2OrLxGhyTWGOhuklYfHhc3UOz/NkhSZQeg1xg/ci17tRR9VQ==','baru@gmail.com','Baru','Baru','Baru',1,1,'Baru','Baru','Baru','Baru','Baru','2017-06-11 14:06:45','14:07:26',1,2,NULL,NULL);

/*Table structure for table `vacancy` */

DROP TABLE IF EXISTS `vacancy`;

CREATE TABLE `vacancy` (
  `id_vacancy` int(10) NOT NULL AUTO_INCREMENT,
  `fakultas` int(5) DEFAULT NULL,
  `prodi` int(5) DEFAULT NULL,
  `title_vacancy` varchar(255) NOT NULL,
  `desc_vacancy` varchar(255) NOT NULL,
  `inter_schedule` varchar(255) NOT NULL,
  `inter_desc` varchar(255) NOT NULL,
  `psiko_schedule` varchar(255) NOT NULL,
  `psiko_desc` varchar(255) NOT NULL,
  `toefl_schedule` varchar(255) NOT NULL,
  `toefl_desc` varchar(255) NOT NULL,
  `inter2_schedule` varchar(255) NOT NULL,
  `inter2_desc` varchar(255) NOT NULL,
  `deadline` varchar(255) NOT NULL,
  `create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update` time NOT NULL DEFAULT '00:00:00',
  PRIMARY KEY (`id_vacancy`)
) ENGINE=InnoDB AUTO_INCREMENT=260 DEFAULT CHARSET=latin1;

/*Data for the table `vacancy` */

insert  into `vacancy`(`id_vacancy`,`fakultas`,`prodi`,`title_vacancy`,`desc_vacancy`,`inter_schedule`,`inter_desc`,`psiko_schedule`,`psiko_desc`,`toefl_schedule`,`toefl_desc`,`inter2_schedule`,`inter2_desc`,`deadline`,`create`,`update`) values 
(257,3,7,'Teknik Mesin','Warga Negara Indonesia (WNI) dan beragama Islam.\r\nProgram Studi Sarjana: kualifikasi minimal bergelar Magister dan berusia tidak lebih dari 40 tahun.\r\nProgram Studi Pascasarjana: kualifikasi minimal bergelar PhD dan berusia tidak lebih dari 47 tahun.\r\nKhu','2017-06-16 02:10','Selamat Anda telah lolos ke tahap Interview I.\r\n\r\nSilahkan datang ke Gedung Al Azhar','2017-06-17 02:15','Selamat Anda telah lolos ke tahap Psikotest.\r\n\r\nSilahkan datang ke Gedung Al Azhar','2017-06-18 02:10','Selamat Anda telah lolos ke tahap Toefl.\r\n\r\nSilahkan datang ke Gedung Al Azhar','2017-06-19 02:10','Selamat Anda telah lolos ke tahap Interview II.\r\n\r\nSilahkan datang ke Gedung Al Azhar','2017-07-29 23:15','2017-06-11 01:20:40','21:29:51'),
(258,3,8,'CPNS Dosen Matematika','Warga Negara Indonesia (WNI)\r\nBerusia antara 18 (delapan belas) tahun dan 35 (tiga puluh lima) tahun pada tanggal 1 Desember 2014. Bagi pelamar yang berusia 35 tahun dan kurang dari 40 tahun per tanggal yang ditetapkan oleh Panselnas, harus memiliki masa ','2017-06-18 05:25','coba','2017-06-19 09:45','coba','2017-06-20 10:50','coba','2017-06-21 10:50','coba','2017-07-29 00:05','2017-06-11 03:52:34','22:29:38'),
(259,1,2,'Dosen Informatika','Deskripsi','2017-06-17 15:55','Deskripsi','2017-06-18 21:30','Deskripsi','2017-06-19 21:30','Deskripsi','2017-06-20 21:30','Deskripsi','2017-07-29 01:05','2017-06-13 21:34:22','00:00:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
